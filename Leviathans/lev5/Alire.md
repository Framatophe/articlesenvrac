# Compiler avec Pandoc



Pour obtenir un PDF / LaTeX

        pandoc --filter pandoc-citeproc --bibliography=lev5-projet-biblio.bib --csl=iso-french-bricole.csl -s --latex-engine=xelatex --toc --number-section -colorlinks leviathan5-projet.md -o lev.pdf


Si on veut utiliser le template (solution obsolète : le template par défaut + les commandes inclue dans le descriptif Yaml du document sont suffisants)

        pandoc --filter pandoc-citeproc --bibliography=lev5-projet-biblio.bib --csl=annales-modelebricole.csl -s --toc --latex-engine=xelatex --number-section --template=lanterne.latex -colorlinks -V fontsize=12pt leviathan5-projet.md -o lev.pdf

Si on veut sortir un .tex en vue d'avoir une bibliographie compilée aevc biber, il faut utiliser --biblatex au lieu du file citeproc, ainsi


Pour du HTML :

        pandoc --filter pandoc-citeproc --bibliography=lev5-projet-biblio.bib --csl=iso-french-bricole.csl -s --toc --number-section --template template.html --css template.css leviathan5-projet.md -o lev.html




---

Le modèle de bibliographie est fourni avec le fichier CSL.
Il s'inspire du modèle Annales. Histoire sciences sociales.
Pour le modifier, on peut utiliser l'éditeur en ligne : http://editor.citationstyles.org/about/

Dans la bibliographie, même avec le moteur xelatex, latex a du mal à digérer le o exposant des numéros de revue. Sais pas pourquoi.... J'ai donc  modifié le modèle pour indiquer num. (utilisation d'une variable).



