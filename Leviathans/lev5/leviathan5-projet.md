---
title: "Vie privée, informatique et marketing dans le monde d'avant Google"
subtitle: "(Leviathans V)"
date: "05 mars 2018 -- v.0.9.9 (document de travail)"
author: "Christophe Masutti"
link-citations: true
header-includes:
        \usepackage[utf8]{inputenc}
        \usepackage[french]{babel}
        \usepackage{csquotes}
        \usepackage{tocloft} 
        \cftsetindents{section}{0em}{1em}
        \cftsetindents{subsection}{0em}{1em}
        \renewcommand\cfttoctitlefont{\hfill\Large\bfseries}
        \renewcommand\cftaftertoctitle{\hfill\mbox{}}
        \setlength{\cftbeforesecskip}{2pt}
        \setcounter{tocdepth}{2}
        \renewcommand\cftsecfont{\small}
        \renewcommand\cftsecpagefont{\small}
        \usepackage{relsize,etoolbox}
        \AtBeginEnvironment{quote}{\smaller}

fontsize: 11pt
papersize: a4
toc-depth: 3
linkcolor: magenta
citecolor: magenta
urlcolor: blue
toccolor: blue
links-as-notes: true
abstract:   |
    Il est notable que les monopoles de l'économie numérique
    d'aujourd'hui exploitent à grande échelle nos données personnelles
    et explorent nos vies privées. Cet article propose une mise au
    point historique sur la manière dont se sont constitués ces
    modèles : dès les années 1960, par la convergence entre
    l'industrie informatique, les méthodes de marketing (en particulier
    le marketing direct) et les applications en bases de données. Les
    pratiques de captation et d'exploitation des données personnelles
    ont en réalité toujours été sources de débats, de limitation et de
    mises en garde. Malgré cela, le contrôle social exercé par la
    segmentation sociale, elle-même imposée par le marketing, semble
    être une condition de l'avènement d'une forme d'économie de la
    consommation que de nombreux auteurs ont dénoncé. Peut-on penser
    autrement ce capitalisme de surveillance ?
keywords: informatique, histoire, surveillance, marketing, vie privée, consommation, base de données, économie, capitalisme
thanks: Cet article est placé sous les termes de la Licence Art Libre (``http://artlibre.org/``). Merci à Goofy pour sa relecture.
---

# Introduction

Nous considérons aujourd'hui que les géants du Web sont 
les principaux pourvoyeurs de pratiques qui mettent en 
danger nos vies privées. Pour leurs détracteurs, ils sont 
la tête et les bras d'un «&nbsp;capitalisme de
surveillance&nbsp;»[@Zuboff2015], c'est-à-dire une manière
de concevoir les mécanismes économiques de la société de consommation
gouvernée par des monopoles (avec la complicité des États) exerçant
le contrôle et la surveillance des individus, en particulier
les utilisateurs de services numériques.

On se pose souvent ces questions, sans pouvoir vraiment y 
répondre&nbsp;: comment en sommes-nous arrivés là&nbsp;? Comment 
est né ce capitalisme de surveillance, capable de 
conformer nos comportements individuels et collectifs à un 
marché modelé par les monopoles d'Internet&nbsp;? Comment 
a-t-on basculé dans cette économie de la surveillance 
sur laquelle beaucoup s'interrogent&nbsp;? Quels en sont les
dangers et quelles sont les possibilités d'en sortir&nbsp;?

L'objectif de cet article est d'apporter quelques pistes 
historiques. Mais avant d'entamer ce parcours, il faut en 
envisager les conséquences&nbsp;: pourquoi s'interroger 
aujourd'hui à ce sujet&nbsp;? D'aucuns pourraient se contenter 
du constat et déployer davantage d'efforts à sortir de 
cette crise de la vie privée en offrant autant 
d'alternatives aux pratiques économiques déloyales des 
firmes. Cette œuvre salutaire doit non seulement être 
encouragée, mais aussi considérée comme vitale pour 
l'équilibre social.

Nous pouvons aussi considérer que c'est en élaborant une 
histoire du capitalisme de surveillance que nous pouvons au 
mieux en déconstruire les principes. Un élément 
frappant, par exemple, est de constater à quel point 
l'histoire de l'informatique en réseau, depuis la fin des 
années 1960, est considérée comme une histoire qui 
relève exclusivement des techniques informatiques, alors 
qu'elle est indissociable de celle des techniques 
commerciales. Cela fausse tout particulièrement l'idée 
que l'on se fait du rôle que jouent les grands monopoles 
du Web d'aujourd'hui. En effet, comme nous le verrons, la 
captation et l'extraction de données personnelles n'est 
pas une activité nouvelle, y compris même lorsqu'elle se 
produit à très grande échelle. De même, les algorithmes 
d'analyse de bases de données ne sont 
pas des nouveautés que les firmes de moteurs de recherche 
ont réussi à implémenter pour optimiser le marketing 
direct, même si ces processus ont bien évidemment connu 
une sophistication inédite ces dernières années. 

Ce qu'on appelle le marketing numérique, dont 
relèvent les techniques de marketing qui font aujourd'hui 
l'objet des plus acerbes critiques ou d'une admiration 
béate (comme l'optimisation pour les moteurs de recherche 
&ndash;&nbsp;SEO&nbsp;&ndash;, le marketing «&nbsp;digital&nbsp;» des réseaux sociaux, 
l'e-mailing, etc.), sont en réalité des pratiques 
anciennes utilisant les médias existants 
et qui permettent la récolte d'informations à une vitesse 
inégalée. Croire que ces pratiques ont façonné le Web 
est une idée à la fois fausse, dans la mesure où elles 
ne font qu'exploiter des canaux de communication, et à la 
fois vraie, dans la mesure où ce sont des stratégies 
marketing qui ont permis l'émergence de services plus ou 
moins utiles ou ont poussé les utilisateurs à utiliser 
des protocoles existants. Cette conception laisse une 
grande place à une histoire du marketing et de l'économie
du numérique conçus comme autant de *story telling*, sublimant 
l'histoire soi-disant aride de l'informatique depuis les 
années 1960 en une histoire plus colorée de l'aventure
des *start-ups* à l'image de celles de la Silicon-Valley.

Cette histoire revisitée est communiquée à bon escient 
par les grandes firmes, et il est facile de concevoir 
que derrière la répétition *ad nauseam* du terme 
«&nbsp;innovations&nbsp;», les intentions n'ont jamais été 
d'optimiser le confort social. En réalité, les plus 
importantes innovations liées à l'histoire d'Internet ont 
surtout consisté à mettre au point des protocoles et des 
technologies de manière à élaborer un espace ouvert 
d'initiatives, aussi bien mercantiles que 
désintéressées. Internet n'est ni ce qu'en font ni ce 
qu'en disent les firmes géantes du numérique 
d'aujourd'hui. Nous devons donc aussi 
«&nbsp;dégoogliser&nbsp;» l'histoire, celle de 
l'informatique en réseau et plus précisément celle de 
l'économie numérique, car à travers l'histoire nous 
pouvons déconstruire ce qui a façonné l'économie de la 
surveillance des utilisateurs et de leurs choix.

À l'époque où se mettait en place, à une échelle industrielle, le traitement informatique des informations personnelles dans le secteur des services et des biens de consommation, le statut de la vie privée a été d’emblée questionné. L'approche collective de la vie privée, à tous les niveaux professionnels concernés (juristes, ingénieurs, managers), avait atteint un assez haut niveau de maturité. 

Notre partons donc de l’hypothèse que les atteintes potentielles à la vie privée ont été envisagées de longue date. Comment en est-on arrivé à un tel déséquilibre entre l’exploitation de nos informations personnelles et la liberté de choix dans laquelle s’inscrit notre vie privée ?



Nous partons aussi d'un principe, dont nous 
tâcherons de voir s'il se vérifie ou non&nbsp;: la 
société de surveillance est une composante nécessaire au 
capitalisme de consommation et dans ce cadre, nous devons 
étudier l'histoire des instruments de cette surveillance 
que sont le traitement automatisé des informations 
personnelles et les techniques marketing de segmentation 
des marchés qui catégorisent les individus et donc, 
façonnent la société et ses représentations.

Comme cette approche est essentiellement exploratoire, nous 
choisirons un découpage par décennies, depuis le début 
des années 1960 et la structuration du marché de 
l'informatique industrielle jusqu'au début des années 
1990 où le débat sur le régime de la société de 
surveillance atteint un point de formulation explicite.

Dans l'idéal, pour se faire une idée correcte des 
connaissances et des usages, il faudrait effectuer une 
lecture exhaustive des publications, rapports et journaux, 
depuis l'époque de la production des ordinateurs à grande 
échelle et leur utilisation dans les secteurs 
industriels et des services. Les sources sont foisonnantes 
et sont même accompagnées par de nombreux témoignages 
oraux (audios et vidéos) retranscrits que nous 
n'aborderons pas ici, à regret. L'approche que nous avons 
choisie consiste à établir une description des mécanismes 
à l'œuvre dans une rencontre entre l'informatique 
industrielle, les techniques de traitement des bases de 
données, la transformation et l'essor économique du 
secteur du marketing et les réactions politiques et 
juridiques relatives à la protection de la vie privée.

Apportons aussi une précision. Hormis dans une section 
où nous adopterons un point de vue plus international, 
nous allons nous concentrer sur l'histoire 
nord-américaine. Il y a deux raisons à cela. La première 
est que les mécanismes et les objets que nous étudions 
depuis les années 1960 sont les plus visibles dans cette 
partie du monde, en particulier parce qu'ils y prirent 
naissance[^constatatten]. La seconde, plus pratique, est que les sources 
sur lesquelles ont peut travailler sont très accessibles 
et que les nombreuses revues américaines spécialisées 
(en informatique, en marketing et en droit) permettaient la 
rédaction et la diffusion de contenus à haute valeur 
documentaire à une échelle inégalée dans les autres 
parties du monde. Parmi les nombreuses limites de cet 
article, il faut donc considérer qu'une approche par pays, 
et si possible de manière comparée, pourrait apporter 
davantage d'éclaircissements, ce qui suppose un travail 
archivistique de taille, mais éminemment intéressant. 

[^constatatten]: C'est aussi ce qu'on peut constater en lisant la brillante étude, sur le même sujet, par Michel Atten[@atten2013].

Enfin, puisque nous en avons parlé, nous devons définir 
ce qu'on entend par vie privée. Bien que nous ne cherchions 
pas à en faire l'histoire, un détour est 
nécessaire étant donné l'extrême malléabilité de 
cette notion selon les pays et les époques.

Aux États-Unis, le développement de la *privacy*, tant du 
point de vue social que du point de vue juridique de la 
*common law*, est la plupart du temps le fruit d'une 
dialectique. On se réfère généralement à un document 
historique, celui de la création même du concept en 1890 
par Samuel Warren and Louis Brandeis, dans un article 
intitulé «&nbsp;The Right to Privacy&nbsp;»[@warrenbrand1890].  En 
fait, la *privacy* «&nbsp;à l'américaine&nbsp;» relève 
essentiellement du droit privé et de la définition des 
*torts* que révèle la jurisprudence&nbsp;: on entend par 
 *the right to be let alone*, tel que l'ont formulé Warren 
et Brandeis, le moyen légal de s'opposer à l'intrusion 
dans les affaires privées, à la diffusion non souhaitée 
d'information privées ou à l'attribution indue d'une 
opinion ou d'un fait et enfin à l'utilisation non 
autorisée d'information à des fins commerciales. Le champ 
reste donc très large et a tendance à s'enrichir 
régulièrement dans l'histoire.

La *privacy* concerne donc des faits et des actions qui 
font l'objet de procédures légales dans la *common law*. C'est justement ce qui la distingue de l'acception de la vie 
privé dans d'autres pays occidentaux disposant d'un droit 
civil et où la notion de vie privée n'apparaît que très 
tardivement. En Allemagne (République Fédérale) c'est la 
Loi Fondamentale de en 1949 qui affirme que «&nbsp;chacun a 
droit au libre épanouissement de sa personnalité&nbsp;». Pour 
ce qui concerne la France, bien que le droit depuis la 
Révolution permette de condamner la 
diffamation[@Halperin2013], la vie privée n'est définie 
qu'à partir de la déclaration Universelle des Droits de 
l'Homme de 1948 et n'entre dans le Code Civil qu'en 1970 
comme «&nbsp;droit au respect de la vie privée&nbsp;». Dans les 
pays européens[^whitman2004],  la vie privée au regard du 
droit civil ou constitutionnel est conçue surtout en 
opposition à la notion de vie publique ou en opposant vie 
privée (qui ne regarde pas l'État) et la vie civile.

Si l'acception de la vie privée sous l'angle de 
la *privacy* a pris une grande ampleur ces dernières 
années[@Halperin2005] dans beaucoup de pays européens, 
c'est parce que nous tendons vers une uniformisation 
technologique des usages de l'information et que ces usages 
posent la question de la vie privée sous l'angle de la 
liberté de choix de divulguer ou non des informations 
réputées nous appartenir (comme un bien), c'est-à-dire 
une acception de la notion de confidentialité. Et c'est 
justement une partie de ce débat qui se développe
 dès les années 1960.


[^whitman2004]: Pour une étude comparée France &ndash; Allemagne &ndash; États-Unis, on peut se reporter à [@Whitman2004].






# Années 1960&nbsp;: Bank Bang&nbsp;!

«&nbsp;Il importe de savoir ce que nous entendons par vie 
privée&nbsp;», c'est en substance le genre de phrase que l'on 
croise de manière quasiment incessante dans les années 
1960, lorsqu'on consulte les rapports officiels, articles 
de droit ou essais, aux États-Unis, en France et un peu 
partout dans le monde occidental. Tout semble confirmer 
qu'à cette période le concept de vie privée est 
sérieusement questionné, jusqu'à faire l'objet de grands 
projets de lois. Pour qu'il en soit ainsi, c'est que 
des changements ont eu lieu. Quels sont-ils&nbsp;? 

Les années 1960 marquent la jonction entre un essor de 
l'industrie informatique et le traitement des informations 
personnelles pour les besoins des autorités 
gouvernementales et pour le secteur du marketing. Cette révolution
informatique a en réalité touché bien d'autres secteurs d'activité,
comme l'industrie et la recherche. Elle a changé radicalement
les pratiques et c'est en cela que tient ce caractère
révolutionnaire.  Il serait cependant trop simpliste de ne
parler que d'une «&nbsp;révolution informatique&nbsp;» sans
en détailler les deux aspects fondamentaux&nbsp;:
1) des productions industrielles 
innovantes en matériels informatiques, qui en moins de 10 
ans, changèrent radicalement les télécommunications et 
le traitement de l'information et 2) des productions en 
ingénierie logicielles dont le plus important secteur de 
développement est le traitement des bases de données (qui 
va de pair avec les systèmes d'exploitation à temps 
partagé).

Pour résumer, il s'agit du matériel (hardware) et du 
logiciel (software). Mais pour comprendre pour quelles 
raisons cet essor fut aussi une réussite économique, il 
faut prendre en compte le besoin auquel il répondait. La 
production informatique aurait très bien pu se contenter 
des applications nombreuses dans les secteurs de la 
recherche, de l'ingénierie (en aéronautique, en 
automobile, etc.). On se doute rarement du grand chamboulement juridique qu'apportèrent les technologies informatiques dans le 
traitement automatisé des banques de données. Cependant, on ne peut distinguer aussi artificiellement les secteurs. Par exemple, les chercheurs (pour ceux qui n'ont pas participé directement à la 
recherche informatique depuis les années 1940) ont très 
vite éprouvé des  besoins en matière de logiciels de 
traitement de bases de données scientifiques (comme en 
biologie) ou en traitement de bases de données 
bibliographiques. Mais le plus important négoce, le plus 
rentable pour l'industrie informatique,  résidait dans les 
applications utilisées par les secteurs d'activité dont 
les bases de données (et le traitement automatisé de listes) était le 
cœur de métier. 

À ce point, nous devons faire preuve de prudence. Ce qui 
va suivre n'est pas une histoire de l'Intelligence 
Artificielle (IA) et de ses applications dans le *data 
mining* ou les systèmes experts. En effet, nous verrons 
que la convergence entre l'IA, l'automatisation de l'analyse 
de données en statistique et les applications en marketing 
viendra beaucoup plus tard, malgré l'essor de l'IA dans la 
recherche durant toute la seconde moitié du
<span style="font-variant:small-caps;">xx</span>^e^ siècle. 
 Nous allons parler de banques de données aussi bien que de 
bases de données&nbsp;: les deux termes sont pratiquement 
équivalents, à ceci près que, dans les années 1960 et 
1970, les *banques* de données relèvent d'un stockage 
d'informations où l'on puise ce qui paraît pertinent pour 
un travail statistique, alors qu'une base de données 
représente le contenu structuré (la manière dont la 
banque de données est organisée, notamment par des solutions logicielles). Aussi, le sujet qui 
fait débat ici concerne les outils informatiques de 
l'analyse statistique, elle-même définie comme une 
activité consistant à manipuler, combiner, transformer 
des données de manière à en tirer des conclusions utiles à 
la décision. Et comme ces décisions concernent les 
individus, elles mettent en jeu l'utilisation des données 
personnelles.

Se remémorant ces années où prenait naissance le réseau 
Internet, le juriste de l'Electronic Frontier Fondation 
Eben Moglen, rappelait dans une conférence tenue à Berlin 
en 2012&nbsp;: «&nbsp;nous n'avons pas inventé l'anonymat lorsque 
nous avons inventé Internet&nbsp;»[@Moglen2012]. Comment 
aurait-il pu en être autrement&nbsp;? En effet, le secteur le 
plus exigeant en matière de bases de données était celui 
du crédit bancaire. Avec la montée des monopoles 
bancaires, la mise en réseau des ordinateurs et les bases 
de données nominatives ont fini par constituer un moteur de 
croissance inédit.

Pour en donner la mesure, on peut se pencher sur l'histoire 
de l'informatisation du renseignement bancaire aux 
États-Unis. Dès l'apparition sur le marché des 
ordinateurs *mainframe* à temps partagé, la course à 
l'informatisation bancaire commença. Par exemple, en 1964, 
sortit l'IBM&nbsp;System/360, une machine à prix abordable et surtout 
dont le design comprenait des applications à visées 
professionnelles dans d'autres secteurs que la recherche ou 
moins spécialisés que dans des secteurs industriels comme 
l'aéronautique. Si bien que cette série de machines fut 
énormément employée pour des besoins d'automatisation, de 
tâches procédurales et, de manière générale pour toutes les entreprises bancaires, la mise en place de systèmes informatisés de gestion, un secteur qui deviendra très vite une science. On peut noter aussi qu'avec cette gamme venait tout un ensemble de dispositifs 
d'enregistrement tels la reconnaissance magnétique des 
caractères (MICR &ndash;&nbsp;Magnetic Ink Character Recognition) 
qui servait notamment à la gestion des chèques, ainsi que la possibilité de créer des systèmes d'information beaucoup plus rapides et en réseau[^etenfrance].

[^etenfrance]: Cette petite révolution n'a pas eu lieu qu'aux États-Unis. Les filiales d'IBM ont permit d'essaimer les produits sur plusieurs continents. Ainsi, en France, on peut aussi prendre l'exemple des organismes de crédit bancaire. Cetelem (Compagnie pour le financement des équipements électro-ménagers), créé en 1953, a toujours été une utilisatrice des innovations d'IBM. Cetelem s'est équipée dès 1962 de machines IBM 1401, puis en 1973 de machines IBM 370, et en 1984, des machines IBM 3081. Elle fut parmi les premières sociétés à utiliser le système de communication TRANSPAC, le premier réseau commercial de transmission par paquets en France[@Trumbull2014, p. 116].

Rassemblant alors de manière exponentielle, années après 
années, des sommes de données sur leurs clients, 
entreprises et particuliers, les organismes de crédit 
bancaire commencèrent à voir la valeur de ces données 
augmenter tout autant. Le profilage de la clientèle se 
faisait de plus en plus finement grâce à des ordinateurs 
en réseau. Une concurrence s'établit dans une course à 
l'automatisation et au traitement du *credit reporting*, le 
secteur le plus stratégique des institutions bancaires. 
Les petits bureaux de crédit locaux, dans lesquels se 
rendaient traditionnellement les Américains furent petit 
à petit absorbés par des monopoles bancaires dont la 
puissance résidait dans la centralisation de l'information
bancaire, le partage rapide de celle-ci et le profilage de la 
clientèle (les degrés de solvabilité, la mesure de 
risque dans les emprunts, etc.). 

Ramenée à ce succès de l'informatique bancaire, on peut 
 prétendre que la course à la base de données 
inaugurée par les banques américaines (et plus 
exactement avec leurs prestataires en gestion de données) 
durant la seconde moitié des années 1960 marqua une 
seconde révolution informatique après l'industrialisation 
des ordinateurs&nbsp;:  l'industrialisation de la base de 
données. 

Dans un article visionnaire du Chicago Tribune en 
1961[@Knoch1961], Robert W. Galvin, président de Motorola 
plaidait en faveur d'un dépôt central de données de 
crédits à la consommation. Sous le nom de FACTS (Fully 
Automated Credit Transaction System), il imaginait une 
carte que le client pourrait insérer dans un terminal, 
donnant ainsi au commerçant un accès à toutes ses 
informations bancaires. Selon Galvin, le «&nbsp;rêve&nbsp;» du 
renseignement de solvabilité parfait, l'évaluation 
permanente des clients, était sur le point de naître. 

Comme le signale Arthur Miller au sous-comité antitrust du 
Sénat dans un rapport en 1968[@Miller1968], c'est la 
Credit Data Corporation (CDC)[@Rule1973] qui commença en 
1965 à créer à une échelle fédérale un système 
interconnecté d'information bancaire. Son président, 
Henry C. Jordan, en vantait les mérites après un fichage 
de plus de 20&nbsp;millions d'Américains en un temps record. Un 
peu plus tard, en septembre 1968, c'est l'Associated Credit 
Bureau of America (ACB) qui noua une convention avec 
International Telephone and Telegraphs Corporation pour 
fournir aux banques membres de l'ACB ses services 
informatisés de *credit reporting*. Cela fit prendre 
une dimension inédite aux pratiques de fichage en les 
étendant à la grande majorité des institutions 
bancaires. Dans la foulée, des sociétés comme First Data 
et Telecredit Inc., spécialisées dans le traitement et 
l'analyse bancaires, prirent leur envol. 

Néanmoins, la saga débridée de ces pratiques ne dura 
qu'un temps&nbsp;: il fallait un jour ou l'autre que les 
institutions publiques et les citoyens finissent par 
remarquer les dangers potentiels d'un tel déséquilibre 
informationnel entre un secteur privé aussi puissant et 
les consommateurs. Les controverses virent le jour.

On peut recenser au moins deux courants dans ces 
controverses.

Le premier, ce qu'un lecteur actuel peut 
légitimement s'attendre à voir et sans pour autant faire 
d'anachronisme, c'est une interprétation catastrophiste 
sur le mode de la défense des libertés individuelles et 
surtout de la vie privée. Deux facteurs expliquent cette 
tendance&nbsp;:

- Dans le monde intellectuel, la représentation 
orwellienne de la société était une manière d'envisager 
le contexte politique durant la Guerre Froide et encore 
davantage pendant la Guerre du Vietnam. Les concepts 
présents dans la dystopie *1984* se prêtaient 
parfaitement à ce contexte fait de peur, de risque 
totalitaire, de risque nucléaire, etc. Un élément 
central du *Big Brother is watching you* se reflétait 
bien dans l'informatisation de la société et 
 devenait pour beaucoup une prophétie en cours de réalisation. Si 
bien que, à la lecture des essais et articles de cette 
seconde moitié des années 1960, on ne compte plus les 
référence à Georges Orwell, quasi-systématiques pour 
qui se penchait précisément sur la captation des 
informations sur les citoyens.
- Ramenée à l'échelle de la politique intérieure des 
États-Unis, une autre composante fut nécessaire&nbsp;: la 
méfiance systématique envers des mesures fédérales. 
Dès lors qu'une institution bancaire exerce un monopole au 
niveau fédéral, elle atteint un point de rupture avec 
l'attachement et l'identification territoriale à l'État, 
ce qui explique la virulence de l'opinion américaine à 
propos de la mise en réseau des informations bancaires par 
des organismes d'évaluation de crédit.

Un second courant, plus réaliste et pragmatique, cherchait 
cependant à se différencier de la vindicte libertaire 
tout en dénonçant les abus du fichage informatique. Il 
est illustré par des personnalités politiques, qui 
plaidaient à la fois pour un débat équilibré et une régulation fédérale. On trouve aussi les auteurs de rapports, ceux-là même qui préfigurèrent la Fair Credit Reporting Act de 1970 qui statue en faveur de la confidentialité des informations sur les consommateurs 
et les mesures visant à les prémunir contre les 
risques liés aux fausses informations sur leurs profils 
bancaires. 

Parmi la pléthore d'études préalables à cette loi, 
l'une d'entre elles mérite d'être signalée car non 
seulement elle fut publiée mais connut aussi un écho dans 
le monde journalistique en apportant 
un point de vue plus nuancé. Il s'agit d'une étude 
co-financée par la Fondation Russell Sage et la National 
Academy of Science sur l'impact social des banques de 
données, dirigée par Alan F. Westin and Michael A. Baker&nbsp;:
 *Databanks in a Free Society: Computers, Record-keeping, and Privacy*[@WestinBaker1972]. L'étude fut menée grâce 
des questionnaires ciblés selon les professions&nbsp;:
chefs de services administratifs, 
responsables de banques et leurs services informatiques, 
ingénieurs, chercheurs, dans tous les secteurs concernés. 
La principale conclusion fut que malgré les craintes du 
public au sujet de cette intrusion dans les libertés 
civiles et la vie privée, l'impact ne fut pas si grand&nbsp;: 
la puissance croissante des ordinateurs et leurs capacités 
de stockage n'ont pas joué le rôle qu'on aurait pu leur 
prêter, à savoir une augmentation du degré d'extraction 
des données personnelles. Le principal changement dans les 
innovations technologiques de 1965 à 1970 fut surtout en 
matière de qualité de traitement.

Néanmoins, le rapport 
ne fait aucune concession au sujet du manque de politique 
publique en la matière. Très libéral sur le fond (et 
Westin ne renie pas en cela son précédent ouvrage sur le 
sujet[@Westin1967]), il ne pardonne pas les manquements en 
matière juridique dont sont fautives les autorités 
laissant les citoyens dans un quasi-total dénuement&nbsp;: 
pouvoir consulter ses propres informations, exercer un 
droit au retrait, garantir la confidentialité, limiter la 
revente de données, etc.

Cependant, le procès à charge contre l'État et le 
crédit bancaire était déjà bien avancé avec la 
publication, dès 1964, de *best seller* tirant 
littéralement à vue sur le mode de la défense des 
libertés civiles face à la centralisation des 
informations sur les citoyens-consommateurs. Cette 
centralisation était vécue comme une menace, sublimée par les possibilités informatiques du traitement des données permettant de 
croiser plusieurs sources statistiques pour pouvoir en 
inférer des profils de plus en plus réalistes. On peut 
citer au moins deux ouvrages célèbres&nbsp;: Myron Brenton dans *The Privacy Invaders* (1964)[@Brenton1964], et Vance Packard, avec *The Naked Society* (1965)[@Packard1964; Traduction française : @Packard1965].

Dans un essai récent sur la surveillance des consommateurs 
de produits financiers aux États-Unis, Josh Lauer consacre 
une grande partie à l'informatisation des bases de 
données, qui marque selon lui une seconde révolution 
informatique. Il résume ainsi la situation[@Lauer2017]&nbsp;:

> Les professionnels du crédit se réjouirent pendant 
longtemps de l'ignorance du public américain en matière 
de surveillance du crédit à la consommation. En 1954, le 
directeur de la National Education Association  confessa&nbsp;: 
«&nbsp;Je suis toujours étonné de constater que l'individu 
moyen ne comprend pas du tout les fonctions d'un bureau 
d'évaluation de crédit. Il n' y a, à mon avis, aucune 
organisation qui affecte la vie quotidienne d'un si grand 
nombre de personnes et qui soit aussi peu comprise&nbsp;». 
Cette situation a brusquement changé au milieu des années 
1960, mais pas en faveur de l'industrie. Le public se 
réveilla et les Américains furent indignés.
> 
> En 1966, le Congrès forma un comité pour enquêter sur 
les risques d'atteinte à la vie privée d'une 
hypothétique base de données fédérale. Ce faisant, ce 
comité découvrit une réalité beaucoup plus troublante. 
Des bases de données centralisées du secteur privé, y 
compris celles concernant les rapports de solvabilité de 
l'industrie, étaient déjà en ligne et traçaient des 
millions d'Américains. La portée et la sophistication de 
l'industrie du reporting choqua les législateurs.  Le 
*credit reporting* informatisé, opposa l'un des membres du 
Congrès, était «&nbsp;vraiment un centre de données national 
entre des mains privées&nbsp;», mais sans aucune garanties 
réglementaires pour protéger le citoyen-consommateur 
contre les abus. Big Brother n'était pas arrivé sous 
l'égide des technocrates orwelliens, mais plutôt comme un 
système commercial pour contrôler les consommateurs.

L'approche choisie par les législateurs américains 
consista à se concentrer sur le secteur qui posait le plus 
de problèmes, celui du crédit bancaire. En octobre 1970, 
le Fair Credit Reporting Act fut la première loi au monde 
en faveur de la protection de données personnelles. Il fut 
voté sous la pression des groupes de consommateurs et en 
particulier la Federal Trade Commission. Les principales 
attentes étaient comblées&nbsp;:

- les types d'informations sur les clients sont rendus 
exclusifs et leur utilisation autorisée uniquement à des 
fins définies par la loi,
- obligation d'informer le client en cas de mesure prise à 
son encontre sur la base des informations qui le concernent,
- l'exactitude des informations sur le client peut être 
vérifiée et contestée par lui sur demande.

En se concentrant ainsi sur un secteur finalement assez 
spécialisé, cette loi faisait néanmoins figure de 
bulle juridique dans une tourmente qui dépassait de loin 
la questions des informations du crédit bancaire.  Même la 
jurisprudence (qui devait encore prendre du temps à se 
mettre en place) ne pouvait excéder ces limites. 
Clairement, le Fair Credit Reporting Act ne satisfaisait 
pas. 

La charge la plus critique fut sans conteste celle d'un 
célèbre juriste d'Harvard, Arthur R. Miller, dans un 
autre best-seller paru moins d'un an après le vote du Congrès&nbsp;:
 *The assault on privacy. Computers, Data Banks and Dossier*.
Dans sa conception, à la limite parfois 
d'une forme de diabolisation du traitement informatique de 
l'information personnelle, Miller cherche à montrer selon 
quelles innovations à venir l'informatique est capable de 
créer une société de contrôle. Par exemple, dès le 
début de son livre, il prétend qu'il sera possible à 
l'avenir de surveiller un être humain, ses activités et 
ses émotions, avec des capteurs sensoriels ou encore que 
la communication de «&nbsp;dossiers&nbsp;» exhaustifs sur chaque 
citoyen sera possible grâce aux technologies 
informatiques.

Le livre d'Arthur Miller n'est qu'une illustration de 
l'état d'esprit du moment, partagé par beaucoup de 
citoyens américains. Quelles que soient les conceptions 
plus ou moins orwelliennes de l'avenir de l'informatique, 
il montrait cependant que, du point de vue gouvernemental 
comme du point de vue des organismes privées, les 
technologies informatiques mettaient en jeu *globalement* le 
droit à la vie privée. Les citoyens américains, et en 
particulier les consommateurs, démontraient leur souhait 
d'une plus grande transparence dans la construction et 
l'exploitation de toute base de données, commerciale ou 
institutionnelle.

# Début des années 1970&nbsp;: la vie privée en Act

Consulter les groupes d'intérêts et les questionner sur 
la vie privée, telle fut la tâche dévolue à la [Federal 
Communications 
Commission](https://fr.wikipedia.org/wiki/Federal_Communicat
ions_Commission) (FCC) qui lança en novembre 1966 une 
grande enquête afin d'obtenir les avis de l'industrie de 
l'informatique, de l'industrie des communications et de 
diverses organisations gouvernementales. Le résultat fut 
publié en 1969, avec l'aide de l'Université de Stanford 
qui traita plus de 3&nbsp;000 pages issues de plus d'une 
soixantaine d'organisations[@Dunn1969; voir aussi, plus 
généralement sur les enquêtes du FCC Computer I, 
Computer II et Computer III&nbsp;: @Cannon2003].

Concernant la protection de la vie privée, la question 
posée était très directe et formulée ainsi&nbsp;: «&nbsp;Quelles 
mesures l'industrie informatique et les entreprises de 
télécommunications doivent-elles prendre pour protéger 
la vie privée et la nature exclusive des données 
stockées dans les ordinateurs et transmises au moyen 
d'installations de communication (&hellip;)&nbsp;?&nbsp;»

Le traitement de la question fut pour le moins lapidaire. 
En effet, il était notamment biaisé par la conception 
d'emblée imposée par la FCC qui distinguait entre les 
opérateurs de communication (les transporteurs) et les 
utilisateurs des réseaux (l'industrie informatique et ses 
clients, comme les entreprise de marketing 
direct et les agences d'évaluation de crédit). 

La conclusion de l'enquête sur la question de 
la vie privée fut éloquente&nbsp;: 

- les clients des entreprises informatiques spécialisées 
dans l'élaboration de banques de données n'ont pas 
d'intérêt à la protection de la vie privée et la 
demande n'existe pas du point de vue des prestataires,
- ce type de protection représenterait un surcoût que la 
clientèle aurait à payer (et elle n'est pas disposée à 
le faire),
- il n'existe quasiment aucune possibilité d'espace 
confidentiel et sécurisé disponible sur le marché pour 
un usage à l'échelle de la clientèle concernée.

Compte tenu de ces résultats, il était difficile de 
plaider, au début des années 1970, en faveur d'un code 
éthique relatif aux usages des informations privées 
véhiculées et manipulées par les prestataires en analyse 
et leurs clients. La solution ne pouvait venir que des 
instances publiques.

En 1973, Grant Morris alors jeune chercheur à la Colombus 
School of Law, publie un article en collaboration avec le 
Sous-Comité Sénatorial sur le Droit Constitutionnel. Dans 
cet article intitulé «&nbsp;Computer Data Bank &ndash; Privacy 
Controversy Revisited: An Analysis and an Administrative 
Proposal&nbsp;», il plaide en faveur d'une harmonisation de la 
*common law* au niveau fédéral à propos du respect de la 
vie privée dans le contexte du stockage et de 
l'exploitation des informations personnelles dans les 
banques de données. Il part d'un constat[@Morris1973]&nbsp;: 

> Malgré les enquêtes gouvernementales qui se sont 
étalées sur une période de six ans, il n'y a 
actuellement aucune protection juridique adéquate 
concernant la collecte et la diffusion des informations 
stockées dans les banques de données (&hellip;) Quelles que 
soient les raisons de l'hésitation du Congrès dans ce 
domaine, le retard semble impardonnable parce que les 
intrusions gouvernementales dans les affaires privées de 
ses citoyens par le biais d'un système aveugle et non 
structuré de collecte d'informations sont des faits qui 
exigent la promulgation de garanties législatives. Compte tenu
 de la demande croissante d'information de la part du 
gouvernement et du secteur privé, ainsi que de l'essor 
fulgurant des banques de données informatiques, l'avenir 
pourrait commencer à refléter le mode de vie décrit dans 
*1984*.

En réalité, selon Morris, la question de 
l'informatisation des banques de données n'est pas le 
principal sujet en termes de droit. Le phénomène et son 
ampleur n'ont fait qu'accroître la sensibilité des 
citoyens à la question du déséquilibre entre le respect 
de la vie privée et le contrôle des usages des 
informations collectées (ainsi que les normes qui 
devraient être imposées). Cela crée alors une situation 
d'urgence qui fait apparaître une certaine contradiction 
entre le fait que l'atteinte à la vie privée 
est reconnue depuis la fin du
 <span style="font-variant:small-caps;">xix</span>^e^ siècle
 aux États-Unis comme un 
délit (depuis l'appel de Waren et Brandeis) et le fait que, 
relevant de la *common law*, tous les États et tribunaux ne reconnaissent pas cette atteinte de la même manière. 

Cette situation d'urgence est de surcroît doublée par un 
ensemble de risques inhérents à l'arrivée du traitement 
informatique des données personnelles&nbsp;:

1. l'informatisation ne permet pas seulement le stockage 
mais aussi une puissance d'analyse jusque là inédite&nbsp;; 
dès lors il faut déterminer quelle est l'exacte portée 
du droit à la vie privée, dans la mesure où il s'agit 
d'une protection de l'individu contre la publication 
d'informations et potentiellement contre l'extraction de 
données inférées de sources diverses non-directement 
reliées à sa personne ou confiées de plein gré&nbsp;;
2. il y a un manque de possibilité de recours à l'encontre 
d'une erreur de classification, dont les répercussions pour 
un citoyen pourraient être désastreuses, ainsi que contre 
l'erreur qui pourrait donner lieu à des fuites de données 
(et donc une publication non-intentionnelle)&nbsp;;
3. le problème du temps de latence entre le rythme de la 
justice en cas de plainte et le rythme du traitement 
informatique qui accroît de manière exponentielle, jour 
après jour, l'accumulation des données des citoyens 
américains dans des banques informatisées&nbsp;;
4. le fait que la *common law* accorde des réparations 
compensatoires en cas de préjudice, et donc n'agit 
qu'*a posteriori*, alors qu'il faudrait réguler *a priori* les 
conditions d'exploitation des données privées.

Reprenant les mots d'Arthur Miller[@Miller1971] qui voyait 
arriver à grands pas une «&nbsp;société du dossier&nbsp;», 
Morris synthétise les craintes qu'il qualifie de risque 
d'«&nbsp;atteinte grave&nbsp;» à la liberté sur deux fronts&nbsp;:

- du point de vue des institutions publiques&nbsp;: la tendance 
suivie par les institutions gouvernementales est 
d'instaurer, grâces aux technologies informatiques, de 
nouveaux moyens de gouvernance, mais sans filet et risquant 
par conséquent de glisser vers un fichage systématisé au 
niveau fédéral,
- du point de vue des organismes privés&nbsp;: le croisement 
des informations entre les institutions gouvernementales et les 
sociétés privées, en particulier les organismes 
bancaires, constitue autant de cas d'atteinte à 
la *privacy* par la publication non souhaitée 
d'informations personnelles.

En ce début des années 1970, le constat de Morris est que 
les remèdes à cette situation sont finalement peu 
nombreux. Il préconise a)&nbsp;de créer des contraintes 
techniques et juridiques et faire reposer la garantie sur 
une agence indépendante telle la FCC, b)&nbsp;créer une loi 
fédérale en droit civil, en se reposant sur une 
consultation des groupes d'intérêt, c)&nbsp;créer des normes 
et procédures d'auto-réglementation sur la protection des 
renseignements personnels et la sécurité des données.

Dans cette orientation, en 1972, un Comité consultatif sur 
les systèmes de traitements automatisés des données fut 
créé au sein de l'U.S. Department of Health, Education 
and Welfare (HEW), dans le but rendre un rapport sur les 
pratiques équitables en matière de traitement de 
l'information et de protection des données personnelles. 
Ce comité publia en 1973 le Code of Fair Information 
Practice[@Sacapds1973, p. xxiii], un ensemble de cinq
principes sur lesquels se fonde le Privacy Act, voté l'année 
suivante[@Bellanova2009]&nbsp;:

> Interdiction des systèmes secrets d'enregistrement des 
données&nbsp;; possibilité d'accès pour l'individu à ces 
informations&nbsp;; principe de limitation de la finalité (sauf 
accord préalable)&nbsp;; possibilité de correction des 
informations&nbsp;; principe de sécurité des informations.

Bien que le Privacy Act ait imposé un cadre juridique strict, 
l'usage montra qu'au moins deux exceptions 
pouvaient lui faire face. La première est que les 
institutions gouvernementales en sont exemptées. En effet, 
la loi concerne les données individuelles détenues par 
des agences, c'est-à-dire des organismes privés. Les 
bases de données des cours de justice, des organes 
exécutifs et des institutions gouvernementales restent 
donc toujours sans cadre légal strict. La seconde 
exception concerne le partage de l'information dans un 
contexte routinier, «&nbsp;dans une finalité qui est 
compatible avec la finalité pour laquelle il a été 
collecté&nbsp;». Ce fut là une faille dans laquelle 
s'engouffrèrent presque immédiatement les agences. 

# Fin des années 1970&nbsp;: une affaire de politique

Si le Privacy Act permettait des exceptions qui 
contrecarraient quelque peu les objectifs de départ, il 
faut reconnaître que ce fut, en ce début des années 
1970, la seule loi qui intégrait la régulation de l'usage 
des données personnelles dans un cadre d'activité 
commerciale et privée. Il est intéressant de lire les 
analyses comparées des mutations juridiques dans l'histoire 
entre différents pays[par exemple @Bennett1992]. Cependant à titre 
intermédiaire, nous pouvons nous pencher brièvement sur 
les pays qui s'illustrèrent en la matière durant cette 
période. 

Il y eu au moins un dénominateur commun&nbsp;: dans les pays 
pionniers en matière de loi «&nbsp;informatique et vie privée&nbsp;»,
 à la différence des États-Unis et le Privacy Act, 
tous se prémunissent plus précisément d'un usage 
déloyal des informations personnelles de la part des 
institutions gouvernementales. Il s'agit de chercher à 
éviter les risques liés aux croisements des données ou 
à un déséquilibre inter-institutionnel qui permettrait 
d'exercer un pouvoir de type anticonstitutionnel. En fait, 
presque partout dans l'Europe du début des années 1970, 
on se pose la question du rapport entre l'usage du 
traitement informatisé des données personnelles par le 
pouvoir institutionnel et la protection de la vie 
privée[@Weinraub1971]. 

La dimension internationale de ce questionnement ne fait 
pas l'ombre d'un doute. Par exemple, le 3^e^ Colloque 
international sur la convention européenne des droits de 
l'homme, entre le 30 septembre et le 3 octobre 1970 avait 
pour thème «&nbsp;La protection de la vie privée 
»[@CIDH1970], et le 24^e^ congrès de l'association 
internationale des avocats tenu à Paris du 26 au 30 
juillet 1971 a consacré une partie de ses travaux aux 
rapports entre techniques et droit, en particulier la 
question de la vie privée[@Nerson1971, p. 740].

Ces rassemblements ne sont en réalité qu'une partie 
émergente d'un processus qui concerne tous les pays qui 
connurent un essor industriel en matière d'informatique et 
de bases de données. Plus exactement, s'agissant la 
plupart du temps d'une industrie dont l'économie est 
transnationale (IBM vend partout dans le monde), nous nous 
situons dans un cas de figure typique d'adaptation des 
droits nationaux au changement technologique international. 
En matière juridique, les processus sont presque toujours 
les mêmes, entre commissions mandatées, interrogations de 
groupes d'intérêts, rapports et promulgation de la loi. 
Pour prendre quelques exemples nous pouvons observer les 
quatre suivants.

En France, en 1974, en particulier suite à l'affaire 
SAFARI[^safari] une commission «&nbsp;informatique et libertés&nbsp;»
 est créé (décret 74.938 du 8 novembre 1974) et rend 
son rapport en un temps record[@CIL1975] (il sera publié 
en 1975), non sans rappeler à raison que le Conseil 
d'État avait déjà planché sur le rapport entre le 
développement de l'informatique et les libertés publiques 
dès 1970[@CE1970]. Tout cela abouti à la loi 
«&nbsp;Informatique et Libertés&nbsp;» du 6 janvier 1978 relative à 
la protection des données à caractère personnel dans les 
traitements informatiques, et à la création de la CNIL 
(Commission nationale de l'informatique et des libertés).

[^safari]: SAFARI est le Système Automatisé pour les Fichiers Administratifs et le Répertoire des Individus. Il s'agissait d'utiliser les numéros Insee et permettre d'interconnecter tous les fichiers nominatifs de l'administration française sous l'égide du Ministère de l'Intérieur. L'affaire est rendue publique le 21 mars 1974 par le quotidien *Le Monde*[@Boucher1974].

Aux Pays-Bas, la Commission Koopmans entama ses travaux en 
1971 et publia son rapport en 1976 «&nbsp;Enregistrement des 
données personnelles&nbsp;»[@Koopmans1976], base sur laquelle, 
dix ans plus tard en 1981, les États Généraux promulguèrent la 
loi sur les données personnelles.


En Allemagne, c'est le *Land* de Hesse qui est souvent 
cité comme un exemple anticipatif en Europe. La Hesse 
faisait suite à un ensemble réglementaire adopté dans le 
*Land* du Schleswig-Holstein, imposant des règles de 
confidentialité dans l'intégration de l'informatique dans 
l'administration publique. Pour la Hesse, il s'agissait 
d'intégrer des règles de confidentialité de ce type dans 
la loi elle-même, ce qui fut effectué en 1970, avec la 
loi sur la protection des données personnelles 
(*[Hessisches 
Datenschutzgeset](https://de.wikipedia.org/wiki/Hessisches_D
atenschutzgesetz)*)[@Burkert2000]. Notons que le concept de 
droit à l'autodétermination informationnelle 
(*informationelle Selbstbestimmung*) est employé et que 
l'autre aspect de la loi (en réalité la raison pour 
laquelle elle fut promulguée en tant que loi) était 
d'éviter un déséquilibre d'information entre le pouvoir 
législatif et le pouvoir exécutif, un peu comme si, en 
France, le projet Safari en faveur du ministère de 
l'Intérieur était passé inaperçu.
 
Comme ce fut le cas au Royaume-Uni à cette même période, 
c'est à l'occasion du recensement national en Suède en 
1969 que les inquiétudes des citoyens face à 
l'informatisation se sont cristallisées, notamment au regard 
des conditions d'accès et de l'opacité du traitement de 
l'information. Une Commission royale fut alors chargée 
d'enquêter et publia son rapport en 1972, intitulé 
*Données informatiques et vie privée* (*Data och 
integritet*)[@OoS1972]. Peu après, le parlement suédois 
vota la *Datalagen* (loi sur la protection des données), 
imposant notamment que pour tout registre créé une demande 
doive être formulée auprès d'une Commission d'inspection 
créée à cet effet (comme le fut la CNIL en France).




Dix ans plus tard, la majorité des pays européens avaient 
pris des dispositions juridiques similaires, si bien que le 
Conseil de l'Europe organisa la Convention 108 «&nbsp;pour la 
protection des personnes à l'égard du traitement 
automatisé des données à caractère personnel&nbsp;»[@EUC1981]. 

Cette parenthèse européenne permet d'illustrer à quel 
point la question juridique est en réalité fragile. En 
focalisant sur les questions éminemment urgentes et 
centrales de constitutionnalité et de démocratie, les 
pays montrèrent aussi que la valeur des données 
personnelles était relative à l'exercice du pouvoir. Dès 
lors, si les États pouvaient élaborer des cadres d'usage 
plus ou moins stricts dans les sujets de gouvernance, 
l'économie était laissée de côté pour une raison qui 
mettra encore dix ans à être posée&nbsp;: les informations 
personnelles sont-elles une affaire de propriété et 
est-ce que leur exploitation relève de l'usage de bien 
marchands&nbsp;? 


# Retour en arrière&nbsp;: la fièvre du marketing

Ce que faisaient les firmes avec les bases de données 
n'était pas une question légère. Comme elles mettaient 
en jeu des questions de sécurité et de confidentialité, 
les premières personnes à s'en inquiéter furent les 
informaticiens eux-mêmes.

L'informaticien Paul Baran, un des inventeurs de la 
communication par paquets et illustre membre de la Rand 
Corporation (le *think tank* du Département de la Défense 
des États-Unis) publia en 1967 un article 
visionnaire[@Baran1967] dans lequel il expose quelques 
recommandations et surtout trois diagnostics importants.

Le premier est de considérer (et le développement de 
l'industrie informatique lui donnera raison) que l'avancée 
majeure des ordinateurs est le temps partagé 
(*time-sharing*), c'est-à-dire la possibilité de partager 
du temps de calcul à plusieurs utilisateurs sur une même 
machine&nbsp;: «&nbsp;La conversion de la vitesse brute de 
l'ordinateur en service simultané à un grand nombre 
d'utilisateurs est un aspect clé de la révolution 
informatique à venir, car il diffuse largement les 
avantages de la technologie informatique&nbsp;».

Le second diagnostic porte sur le manque de 
réglementation. D'abord, concernant la protection de la vie 
privée, il constate que les informations circulent 
essentiellement sur des réseaux non-protégés et sont 
stockées dans des systèmes qui ne répondent à aucune 
norme où seule la confiance, «&nbsp;un manque de 
sophistication technique&nbsp;», garantit la confidentialité. 
Ensuite, s'il doit y avoir une réglementation, elle sera 
essentiellement une affaire d'argent. En effet, 
considérant le nombre d'acteurs différents, on ne peut 
que reconnaître que les intérêts ne se croisent pas&nbsp;: 
les industriels du hardware, les producteurs de software, 
les opérateurs de télécommunications, le gouvernement, 
les citoyens. Par exemple, ajoute-t-il, «&nbsp;la plus forte 
croissance de la nouvelle industrie de l'informatique est 
dans la vente des services sur les lignes téléphoniques. 
La communauté informatique ne souhaite pas que son taux de 
croissance soit déterminé par les entreprises de 
communication&nbsp;».

Le troisième diagnostic se veut constructif.
 Considérant la concurrence et le besoin de 
normalisation entre les services de télécommunications, 
le coût de ces derniers en matière de mise en réseau 
des ordinateurs est de plus en plus prépondérant. Dès 
lors, il y a deux options à envisager. La première 
consiste à développer davantage la communication par 
radio[^baranwifi], et l'autre consiste à limiter la 
concentration des services en développant plusieurs 
concentrations fédérées, mais à condition que la 
réglementation suive le mouvement&nbsp;:

[^baranwifi]: Paul Baran travailla plus tard sur les technologies liées à l'Internet sans fil.

> Aujourd'hui (&hellip;) un petit système à temps partagé ne 
peut pas offrir de service économique à des clients 
situés à plus de 50 à 100 milles de distance. Mais si 
plusieurs personnes indépendantes à Los Angeles 
souhaitaient partager un circuit de communication unique 
qui serait relié à différents systèmes informatiques 
dans la région de Boston, il est tout à fait possible 
pour elles de se regrouper pour construire un petit 
dispositif semblable à un ordinateur &ndash; appelé 
«&nbsp;concentrateur numérique&nbsp;» &ndash; qui rassemble tous les signaux et les transmet sur un seul circuit, loué 
par téléphone. Le coût des communications par 
utilisateur deviendrait si bas par cet arrangement qu'il 
serait économiquement possible de «&nbsp;parler&nbsp;» à un 
ordinateur presque n'importe où dans le pays. Il n'y a 
qu'une seule prise &ndash; cela peut être en violation des lois 
actuelles! Nous aurions créé un transporteur public non 
réglementé.

Ce que montre Baran, ici, c'est la préférence économique 
pour la concentration des données car il s'agit d'un enjeu 
stratégique relatif à chaque firme. L'exemple 
gouvernemental est négligeable, en la matière, puisque 
ses enjeux sont ceux qui relèvent de la Défense ou des 
garanties constitutionnelles. En revanche, lorsqu'il s'agit 
des firmes et particulièrement celles qui font de 
l'information une activité marketing, la concentration et 
l'accès aux données est la clé. Dès le début des 
années 1960, ce fut un point central du développement de 
l'activité marketing à l'aide du traitement et de 
l'analyse des données personnelles. En effet, tout réseau 
de cet ordre a son intérêt dans la captation et la 
rétention. Or, l'effet pervers de la prise de conscience 
sécuritaire des bases de données réside dans la 
reconnaissance implicite de leur avantage stratégique dans 
la concurrence entre monopoles détenant les informations. 
Pour ces monopoles, sécuriser revient aussi à privatiser. 

Les bases de données accédèrent donc très vite à un 
statut de facteur de croissance à la fois pour l'industrie 
informatique et aussi pour l'économie des biens de 
consommation et des services&nbsp;: informatique et marketing 
accédèrent à une nouvelle ère où le démarchage passa 
d'un statut secondaire et coûteux à un statut central et 
hautement rentable.


Dans l'histoire économique, le marketing est une activité 
dont le but général est de faire correspondre les 
comportements économiques à l'objectif de 
croissance[@Flambard1997]. On peut le qualifier de manœuvre visant à créer une relation durable avec le consommateur. Avec l'émergence du marketing relationnel, l'activité sera en elle-même 
productrice de valeur, en particulier grâce à ce qui sera 
théorisé plus tard comme la valeur vie client
 (*customer lifetime value*), c'est-à-dire la somme de profits 
réalisés durant la relation entre le client et la firme.

L'histoire du marketing au 
<span style="font-variant:small-caps;">xx</span>^e^ siècle 
peut se résumer  assez simplement en trois mouvements qui se chevauchent 
quant à leurs principes. Le découpage utilise comme clé  de lecture la transformation des 
listes de clients en bases de données[^surtedlow]&nbsp;:

- après la révolution industrielle, le marketing 
consistait à embrasser un marché de masse en nouant des 
contacts avec les consommateurs par publicité ou 
publipostage,
- durant la première moitié du 
<span style="font-variant:small-caps;">xx</span>^e^ siècle 
 apparu la segmentation des marchés et donc de la 
clientèle&nbsp;; cette dernière pouvait alors s'appréhender sous l'angle de 
groupes de consommateurs ou de communautés, le marketing 
consistant alors à mettre en œuvre des processus 
d'identification, 
- la seconde moitié du <span style="font-variant:small-caps;">xx</span>^e^
 siècle fut l'époque de la 
prospection et du marketing direct, et consista à 
optimiser les données de renseignement sur la clientèle 
en ciblant, et en enregistrant les caractéristiques 
individuelles.




[^surtedlow]: Bien sûr il existe d'autres découpages possibles. Tout dépend de la clé de lecture. Par exemple, à l'instar de Richard Tedlow, si on se focalise sur le marché des biens, le marketing peut simplement être envisagé comme une activité qui suit l'évolution des  modèles économiques&nbsp;: ère du taylorisme (mécanisation et production standardisée), ère du fordisme (production et consommation de masse), crise post-industrielle et montée en charge des biens et services. Voir [@Tedlow1993].

Concernant les rapports entre le marketing et 
l'informatique, on peut surtout envisager leur histoire
sous l'angle d'une interdépendance. Celle-ci se 
déclare aux États-Unis durant les années 1960 avec 
l'essor de l'industrie informatique qui, comme nous l'avons 
vu précédemment, adapte sa production aux besoins des 
firmes en matière de gestion et de banque de données. On 
y distingue alors deux étapes&nbsp;: un début «&nbsp;matériel&nbsp;» 
où les grandes firmes envisagent l'équipement sous 
l'angle coût-bénéfice[^filmFortune], et un aboutissement 
«&nbsp;logiciel&nbsp;» où ces firmes orientent davantage leurs 
stratégies en interne vers du management-marketing et en 
externe vers le marketing de profilage-clientèle.

[^filmFortune]: On peut se reporter aux magazine *Fortune* qui, en 1960 subventionna une série de films à destination des décideurs stratégiques des entreprises au sujet du traitement électronique de données. Un bel exemple: «&nbsp;The Computer Comes to Marketing&nbsp;», Fortune Films, Prod. Wilding Inc., 1960. [Lien](https://www.hagley.org/librarynews/selling-computer-business-1960).

Pour comprendre cette distinction entre matériel et 
logiciel, on se rapporte communément à l'année 1969 où, 
anticipant les poursuites de l'État en vertu de la loi 
antitrust, IBM changea radicalement sa stratégie  et 
marqua l'industrie informatique en dégroupant le matériel 
du logiciel.  Si auparavant, un ordinateur était vendu 
presque «&nbsp;sur mesure&nbsp;» avec une panoplie logicielle, le 
fait est que la production logicielle prit une place 
prépondérante en vertu de l'intérêt des machines pour 
les services qu'elles rendent et pas seulement pour leurs 
capacités de calcul. Cette pratique fut nommée 
l'*unbundling* et configura durablement le marché 
informatique. Mais l'*unbundling* était un mouvement 
engagé depuis le milieu de la décennie, à la fois pour 
des raisons de coûts mais aussi en raison des besoins 
spécifiques de l'administration des données des
firmes qui développaient leurs activités marketing.

Ainsi, les premiers ordinateurs en 1960 étaient surtout 
employés pour remplacer un traitement manuel des données 
par un traitement automatisé. Typiquement, les activités 
concernées étaient le mailing, l'utilisation des 
systèmes de code postaux, le traitement des listes 
(ajouter, supprimer, combiner des fiches clientèles), etc. 
 Moins de cinq ans plus tard, les  logiciels de traitement 
de dossiers clients firent leur apparition, utilisant des 
bases de données (structurées). L'*unbundling*, qui fut 
en quelque sorte «&nbsp;officialisé&nbsp;» par IBM, était en 
réalité un mouvement  amorcé en réponse aux 
entreprises qui, possédant une large part administrative 
et par conséquent un service de traitement de 
l'information hautement stratégique, avaient néanmoins 
des difficultés à engager des dépenses pour le 
développement conjoint de matériel et de logiciel. Pour 
ces entreprises, qui n'étaient autres que les organismes 
de crédit à la consommation, les compagnies d'assurance 
et les grandes enseignes de biens de consommation (tel 
Sears), la fourniture de solutions «&nbsp;packagées&nbsp;» 
(combinant matériel et logiciel) avait ses limites. Or, au 
cours de la seconde moitié des années 1960, ces 
entreprises furent contraintes de passer d'une génération 
d'ordinateurs à l'autre, par exemple des séries 
IBM 1401 ou 7040 à l'IBM System/360. Ainsi, les 
solutions «&nbsp;progicielles&nbsp;» étaient 
bienvenues[@Canning1967] dans la mesure où, à l'avenir, 
elles étaient censées réduire les coûts de maintenance. 
Ainsi, la firme Computer Science Corportation, qui 
fournissait surtout IBM en matière de programmation, 
commença à offrir des solutions logicielles aux clients 
d'IBM. Elle débuta par les organismes d'évaluation de 
crédit en fournissant une solution complète de gestion de 
paie, de portefeuille actionnaires, de liste de clientèle, 
etc.[@Haigh2002; @Steinmueller1995]


Cette transformation de l'informatique, combinée avec la 
place prépondérante des bases de données dans les 
stratégies économiques des firmes, permit un essor 
considérable dans le domaine de l'analyse des fichiers 
clientèle. L'activité de profilage s'est alors définie 
comme une activité produisant de la valeur, ce qui eut 
comme effet une croissance de la valeur fonctionnelle de 
l'information clientèle, tout particulièrement dans les 
stratégies de marketing direct. On définit le marketing 
direct comme une technique de démarchage consistant à 
contacter de manière ciblée des individus ou des groupes 
d'individus, dont on a déterminé un profil, dans le but 
d'influencer leurs stratégies d'achat (ou de non-achat) de 
manière mesurable. Dès lors qu'il est possible de mesurer 
ces comportements, il est possible d'affiner les rapports 
coûts / bénéfices du marketing direct, faisant de cette 
activité un élément central des modèles économiques, 
en particulier dans l'économie de services.

C'est ce que montre notamment John Stevenson avec son 
étude de cas sur American Express[@Stevenson1987] qui 
envoyait des millions de lettres promotionnelles aux 
consommateurs. Cette dernière firme est d'ailleurs 
qualifiée de pionnière en la matière, grâce à la 
personne de [Lester Wunderman](https://en.wikipedia.org/wiki/Lester_Wunderman), 
réputé le premier à avoir défini le marketing direct, 
non parce qu'il s'agissait d'une activité novatrice 
(l'envoi de courrier postaux par les firmes est au moins 
aussi ancienne que l'activité postale elle-même) mais 
parce que, s'agissant de millions de clients, la stratégie 
de ciblage, l'automatisation et l'informatisation 
deviennent plus que nécessaires.

Grâce à l'informatisation du marketing direct, les 
techniques employées, qui sont essentiellement d'ordre 
statistique (et le secteur emploie alors de plus en plus de 
mathématiciens), deviennent aussi des techniques d'analyse&nbsp;:
régression du marché, analyse de fréquence, 
anticipation et prédiction des comportements, 
identification des potentiels d'achat de la clientèle. On 
comprend dès lors pour quelle raison les organismes de 
crédits (rattachés à des géants de l'industrie de biens 
de consommation) se lancèrent dans l'activité de 
marketing direct&nbsp;: il ne s'agissait pas de prospecter de 
nouveaux marchés (la valeur vie client est d'autant plus 
valorisable qu'il s'agit surtout de fidéliser la 
clientèle) mais de comprendre les comportements de 
consommation de manière à élaborer des stratégies de 
présélection de clientèle, c'est-à-dire déterminer à 
l'avance le niveau de risque (en matière de crédit 
surtout) que représente un client. Dans ces conditions, 
l'exploitation de bases de données les plus exhaustives 
possible devient un enjeu crucial, dont les bénéfices 
furent mesurés et anticipés sur plusieurs années voire 
décennies[@Lauer2017]&nbsp;:

> Au cours des années 1970 et 1980, les principales 
agences d'évaluation du crédit se sont lancées 
énergiquement dans (les) programmes de présélection 
(prescreening). Bien que le terme de présélection soit 
nouveau, le concept lui-même ne l'était pas. Depuis les 
années 1950, les bureaux de crédit vérifiaient les 
programmes de cartes de débit des centres-villes et 
dressaient des listes promotionnelles. La différence 
était que ces programmes étaient organisés localement et 
traités manuellement. Les ordinateurs ont 
considérablement élargi la portée de ces opérations. En 
s'appuyant sur leurs propres bases de données, ainsi que 
sur celles de leurs clients et des courtiers en listes, les bureaux de crédit pouvaient facilement compiler d'énormes listes de clients potentiels. Avec tant de données facilement accessibles, la présélection est rapidement passée d'une activité lucrative à un service de base parmi les principaux bureaux informatisés. (En 
1976), 7% des recettes annuelles de TRW provenaient de la 
présélection. À la fin des années 1980, Equifax gagnait 
entre 10% et 20% de ses recettes totales provenant de la 
présélection.

Les années 1970  furent sans conteste des années 
d'activités dédiées à l'analyse des bases de données, 
ce qui orientait de manière définitive la société de 
consommation dans une logique où les firmes organisent le 
pouvoir informationnel. Pour illustrer cela on peut se 
rapporter aux pratiques initiées par l'industrie du tabac. 
Les résultats de l'étude de Jane Lewis et Pamela Ling 
peuvent être cités *in extenso*[@LewisLing2016]:

> Le marketing direct par publipostage utilisant les bases 
de données de l'industrie a commencé dans les années 
1970 et il est né du besoin d'une stratégie 
promotionnelle pour faire face à la baisse des taux de 
tabagisme, au nombre croissant de produits et à un paysage 
médiatique très chargé. RJ Reynolds et Philip Morris ont 
commencé avec les listes de diffusion commerciales déjà 
disponibles, mais ont par la suite décidé de créer leurs 
propres bases de données sur les noms, les adresses, les 
préférences de marque, les habitudes d'achat, les 
intérêts et les activités des fumeurs. Au milieu des 
années 1990, les bases de données de RJ Reynolds et 
Philip Morris contenaient chacune au moins 30 millions de 
noms de fumeurs. Ces entreprises appréciaient la 
souplesse, l'efficacité et la capacité unique du 
marketing direct et des bases de données, ainsi que la 
visibilité limitée du publipostage pour la lutte 
antitabac, la santé publique et les organismes de 
réglementation.


# Début des années 1980&nbsp;: techno-stats

La maîtrise de la valeur vie client étant le Graal de 
toute firme qui possédait des capacités d'analyses 
statistiques de la clientèle, l'accent fut mis sur la 
méthode&nbsp;: comment établir des relations entre les 
informations, par exemple pour déterminer des groupes de 
clients ou, dans le cas de firmes ayant de multiples 
activités (ne serait-ce qu'une activité de distribution 
de biens de consommation couplée avec une agence de 
crédit), comment établir le profil multi-client d'un seul 
individu. 

Classifier, déterminer les facteurs explicatifs, réduire 
des erreurs prédictives, tout ces éléments confèrent à 
l'analyse statistique son intérêt.  Déjà en 1963, la 
recherche en statistique connu une petite révolution 
grâce à James Morgan et John Sonquist[@Morgansonqui1963] 
qui se livrèrent à un inventaire des limites de l'analyse 
statistique, notamment la question des corrélations et les 
interrelations (par exemple entre l'âge et le pouvoir 
d'achat, le métier, le genre, etc.) ou encore les 
interactions causales (par exemple le rapport entre le fait 
d'être une femme et le niveau d'étude).  Morgan et 
Sonquist proposèrent alors une procédure en 
mathématiques statistiques, que l'on peut faire figurer au 
titre d'un premier algorithme d'analyse exploratoire de 
données informatisées&nbsp;:

> La proposition faite ici est essentiellement une 
formalisation de ce qu'un bon chercheur fait lentement et 
inefficacement, mais avec perspicacité sur une trieuse 
IBM.  Avec de grandes masses de données, des échantillons 
pondérés et le désir d'estimer la réduction des 
erreurs, nous devons toutefois être en mesure de simuler 
ce processus sur du matériel informatique à grande 
échelle. (&hellip;) Nous décrivons maintenant le processus 
d'analyse sous la forme d'une série de règles de 
décision et d'instruction.

Un an plus tard, Sonquist et Morgan décrivent dans un 
rapport de l'Université du Michigan[@SonquistMorgan1964] 
un programme à visée opérationnelle pour les sciences 
sociales nommé AID (Automatic Interaction Detector, v. 2). 
Ce programme fut éprouvé plusieurs fois, par plusieurs 
chercheurs, renommé THAID en 1972, SEARCH en 1974 puis 
re-développé et renommé Chi-Square Automatic Interaction 
Detector (CHAID) en 1980[@Mckenzie1992; @Morgan2005]. Quant 
à l'analyse exploratoire de données, elle fut théorisée 
en particulier par John Turkey en 1977[@Turkey1977].

À l'époque où Turkey théorise l'analyse exploratoire, 
les équipes de statisticiens travaillaient   déjà avec 
des logiciels forts connus. Par exemple BMDP (Bio-Medical 
Data Package) fut développé en 1965 à l'Université de 
Californie (et distribué gratuitement), SPSS (Statistical 
Package for the Social Sciences) fut mis en vente par IBM 
en 1968. On peut citer aussi TROLL, développé au MIT et 
livré pour la première fois en 1971, spécialisé dans la 
modélisation économétrique et l'analyse statistique, 
spécialement étudié pour tourner sur des machines à 
temps partagé (et avec des systèmes d'exploitation comme 
CTSS). Du côté des langages de commande on peut citer SAS 
(Statistical Analysis System) apparu en 1976. En langage de 
programmation, le langage S (de haut niveau), crée par 
John Chambers en 1976, permettait d'obtenir un 
environnement dédié à l'analyse à la fois des données 
et des graphes. Il fut distribué par Bell et disponible 
sur des machines personnelles dès le début des années 
1980 et, en effet, l'apparition de micro-ordinateurs 
doté d'un système de visualisation graphique permettait de tirer 
pleinement parti des modèles d'analyse. Ainsi, les séries 
de Macintosh furent les premières à proposer des 
logiciels accessibles sur des machines individuelles, comme 
Data Desk, un logiciel développé par un étudiant de John 
Turkey en 1985. Du côté des logiciels libres, le plus 
important pour les décennies suivantes fut xLispStat (sous 
licence BSD), développé en 1989 par Luke Tierney, 
toujours utilisé aujourd'hui mais peu à peu supplanté 
par R.

Les programmes d'analyse statistique les plus utilisés 
dans les années 1970 le sont encore aujourd'hui, sous des 
versions différentes. On peut mieux comprendre ainsi 
l'importance de ce type de logiciels dans l'usage des 
productions de l'industrie informatique, car quels que 
soient les supports (ordinateurs *mainframe* ou ordinateurs 
personnels), les développements logiciels n'ont cessés 
d'être actifs, ce qui signifie aussi que la demande n'a 
cessé de croître. 

Le panorama ne serait pas complet si l'on ignorait le rôle 
fondamental qu'a joué l'innovation en matière de 
systèmes de base de données dès le début des années 
1980. Leur histoire pourrait faire l'objet d'un livre à 
part entière, on peut citer&nbsp;:

- en 1966, IBM implémente le service ISAM (indexed 
sequential access method), organisation et manipulation de 
fichiers séquentiel indexés. Très populaire dans les 
années 1970, c'est pour l'exploiter que fut créé en 
1981[@Sumathi2007] la société RDS (Relational Database 
Systems) qui produisit Informix (et plus tard  Informix-SQL 
et Informix-4GL).
- en 1970, Edgar Frank Codd publie un article intitulé 
«&nbsp;A Relational Model of Data for Large Shared
Data Banks&nbsp;»[@Codd1970]: qui pose les bases
théoriques d'un langage
nommé Structured English QUEry Language (SEQUEL).
- En 1979, la société Software Development Laboratories 
(créé en 1977) change de nom et devient Relational 
Software, Inc. (RSI) et propose Oracle V2 comme système de 
base de données relationnelle. Devant le succès, 
l'entreprise devint en 1983, Oracle Corporation.
- En 1980, sur ses System z, IBM sort Db2, son système de 
gestion de base de données qui repose sur SQL.

Ainsi, deux transitions eurent lieu&nbsp;: dans les années 1970, 
le passage des bases de données hiérarchiques aux bases 
de données relationnelles, avec des études très 
poussées sur des thèmes tels l'informatique 
décisionnelle (arbres de décision) et l'exploration de 
données (par algorithmes)&nbsp;; puis, au début des 
années 1980, se jouait une nouvelle forme de concurrence 
dans les services&nbsp;: celle de la course à la meilleure 
exploitation des bases de données et leur gigantisme (avec 
des problématiques industrielles de stockage et de 
communication). L'innovation se jouait néanmoins 
essentiellement au niveau logiciel, puisque tout le défi 
était d'obtenir des outils d'aide à la décision capables 
d'exploiter des bases de données plus larges et avec des 
architectures différentes, ce qui nécessitait en retour 
des systèmes de gestion de bases de données (SGBD), dont 
il fut question ci-dessus. 

L'introduction des systèmes de base de données 
relationnelles dans le monde du marketing fut vécue comme 
un séisme, de deux points de vue. D'abord, petit à petit, 
les professionnels du marketing purent s'approprier 
l'activité de gestion et exploitation de bases de 
données[@Petrison1993] sans pour autant être des 
informaticiens-mathématiciens de haut niveau, grâce aux 
langages de commandes et de programmation, puis par des 
interfaces graphiques.  Ensuite, l'intérêt ne fit que 
croître grâce à la possibilité de formuler des 
requêtes et combiner des données externes avec des bases 
déjà existantes. Tout cela permit aux professionnels du 
marketing d'élaborer des stratégies agiles, et de dresser 
des bases de clientèles dont les paramètres 
d'exploitation pouvaient changer indéfiniment selon les 
contextes, les objectifs, les changements du marché, les 
segmentations.


# Fin des années 1980&nbsp;: données personnelles, productions de valeurs

L'exemple de Spiegel est très illustratif de l'usage des 
bases de données relationnelles en termes de croissance. 
Spiegel Inc. est une société historique aux États-Unis. 
Lancée fin
<span style="font-variant:small-caps;">xix</span>^e^ siècle
à Chicago, elle se fit connaître 
comme détaillant de biens de consommation d'abord par la 
publicité puis par l'intermédiaire de catalogues 
célèbres à travers les différents États. À la fin des 
années 1970, la société connut une période difficile, 
face à la concurrence des magasins discount comme Kmart. 
De nombreux licenciements s'ensuivirent mais la hausse des 
taux d'intérêts et la nouvelle configuration du marché 
restaient des obstacles délicats. 

Deux stratégies firent leurs preuves&nbsp;: la première fut de 
remanier le catalogue en proposant pour l'essentiel des 
produits haut de gamme, changeant ainsi la nature de la 
clientèle. La seconde stratégie fut, en 1982, la vente de Spiegel à 
Otto-Versand GmbH, une société allemande fort connue, 
elle aussi, dans le secteur des catalogues et vente par 
correspondance.

Sous la responsabilité du nouveau manager Hank Johnson, le 
marketing prit l'avantage des récentes technologies en 
bases de données et engagea l'avenir de l'entreprise par 
l'analyse RFM (*recency*, *frequency*, *monetary*) des 
données quantitatives de la clientèle[@Shaver1996, p. 
26]. L'analyse RFM consiste à déterminer les meilleurs 
clients en relevant les dates d'achats (*recency*), leurs fréquences (*frequency*) et les montants (*monetary*), selon l'idée que 80% du chiffre d'affaires provient de 20% des clients. La modélisation RFM permet de ce fait de déterminer le comportement sur une courte période (si on le compare, le modèle basé sur la durée de vie client est plus ambitieux puisqu'il s'agit d'étaler 
la prédiction sur une période beaucoup plus 
longue[@Gupta2006]). Dès lors, un découpage des 
consommateurs par segments permit d'en tirer une typologie, 
un profilage, et personnaliser l'édition de catalogues en 
autant de versions que de segments (26 segments furent 
identifiés). Les bénéfices de Spiegel en furent 
considérablement augmentés[^spiegelwiki], faisant de 
cette étude de cas un modèle du genre. 


[^spiegelwiki]: D'après la page Wikipédia (angl.) «&nbsp;Spiegel (catalog)&nbsp;»&nbsp;: «&nbsp;Entre 1982 et 1983, le chiffre d'affaires de Spiegel est passé de 394 millions de dollars à 513 millions de dollars, et les bénéfices avant impôts de l'entreprise ont plus que doublé, atteignant 22,5 millions de dollars en 1983.&nbsp;»


L'intérêt financier des bases de données (et auparavant 
des listes de clientèle) était certes connu depuis 
longtemps, mais la valeur de l'analyse elle-même devint un 
marché propre aux stratégies en recherche et 
développement. Les années 1980 furent des années où les 
recherches en marketing direct furent parmi les plus 
poussées. En économétrie, de nombreux modèles 
émergèrent, qu'il s'agisse de l'analyse RFM, de l'analyse 
de régression (évaluer la relation entre des variables 
pour en définir une interprétation prévisionnelle), ou 
les techniques AID. Toutes cherchent à optimiser la valeur 
prédictive de l'analyse des données clientèle. Robert 
Blattberg, professeur de marketing à Carnegie, résume la 
situation en 1987[@Blattberg1987]&nbsp;:

> L'objectif de ces modèles est de prédire la 
probabilité qu'un individu achètera à partir d'une 
nouvelle offre, compte tenu de données historiques 
précises sur cet individu. Le problème de la 
modélisation est de concevoir des techniques qui peuvent 
améliorer les méthodes actuelles. Jusqu'à présent, des 
techniques comme la régression sont raisonnablement 
prédictives. Pourtant, bon nombre des hypothèses de 
régression sont violées. Existe-t-il des méthodes qui 
peuvent être conçues pour les problèmes spécifiques 
auxquels les spécialistes du marketing direct sont 
confrontés et qui prédisent mieux que la régression ou 
l'AID?

Dans la même veine, Jay Magidson, fondateur en 1981 de 
Statistical Innovations Inc., propose de renouveler le «&nbsp;stock&nbsp;» de modèles traditionnels hérités de la 
décennie, compte tenu du recul disponible sur leur valeur 
prédictive et des récentes innovations en bases de 
données et des capacités de traitement à des échelles de 
plus en plus importantes[@Magidson1988]. 

Dans un ouvrage, Dick Shaver, expert en la matière auprès 
des plus grandes multinationales, procède à un état des 
lieux des techniques de marketing direct aux États-Unis et 
indique combien de nombreuses entreprises américaines 
utilisaient les données à leur disposition sans toutefois 
se concentrer sur les formes possibles d'atteinte à la vie 
privée. Lorsque la base de données issue du recensement 
américain de 1970 (et plus tard celle de 1980) fut 
disponible à la consultation publique, ce furent des 
millions d'informations sur les familles américaines qui 
furent disponibles. L'entreprise Claritas Inc. créa alors 
un grand système de classification des consommateurs, 
Claritas PRIZM, découpant et segmentant les informations 
sociologiques, économiques, géographiques et 
démographiques (et vendant les résultats de ses 
analyses). Chaque information client dans les bases de 
données des firmes pouvait dès lors se voir associé un 
code PRIZM. Les entreprises les plus cotées dans *Fortune 
500* et qui avaient une stratégie de marketing direct 
s'emparèrent de cette manne providentielle, tels RR 
Donnelley, MetroMail, RL Polk&hellip; Mais ce furent les 
sociétés de crédit qui furent les plus avides&nbsp;: elles 
mirent en place des bases de données croisant, de 
manière non sécurisée et sans procédure clairement 
définie, les données de leurs clients avec les données 
de type PRIZM. Dès lors, de multiples erreurs eurent lieu.

Ce fut un moment où se réalisa la prophétie de Lance 
Hoffman, spécialiste en sécurité informatique et 
professeur à l'Université George Washington. Dès 1969 
(et la suite de ses publications jusqu'aux années 1990 est 
à l'avenant[@Hoffman1995]), Hoffman mettait en garde la 
communauté des informaticiens contre deux problèmes de 
taille dont les fournisseurs en techniques d'analyse de 
base de donnée auraient dû s'emparer immédiatement, 
malgré les efforts significatifs menés auparavant, en 
particulier suite aux scandales des agences d'évaluation 
de crédit&nbsp;: la sécurité des accès (il faut standardiser 
les contrôles d'accès non plus seulement au niveau des 
fichiers mais au niveau de leur indexation), et les 
processus d'indexation qui mettent en jeu l'intégrité des 
informations (production d'erreurs donnant lieu à des 
décisions erronées)[@Hoffman1969]. Selon Hoffman, il 
s'agit là de questions de protection de vie privée autant 
que de sécurité pour les firmes, et ne pas les résoudre 
dans un avenir proche, expose la firme à de graves ennuis 
techniques et juridiques.

C'est ce qui se produisit en 1990 dans une injonction 
retentissante à l'échelle fédérale aux États-Unis. En 
raison des nombreuses erreurs dans leurs profils 
clientèle, et malgré les cadres juridiques du Fair Credit 
Reporting Act et du Privacy Act, beaucoup de consommateurs 
furent lésés par des sociétés de crédit, à tel point 
qu'une enquête fut menée par la Consumer Union pendant 
plusieurs années. Poursuivie par les procureurs généraux 
de 18 États, la société Equifax, l'une des plus 
importantes agences d'évaluation du crédit, fut 
contrainte d'assurer aux consommateurs concernés la 
protection de leurs données personnelles et en garantir 
l'accès sur demande pour une somme maximum de 8&nbsp;dollars. 
De plus, Equifax fut contrainte de s'engager à ne plus 
vendre les rapports de crédit (plus de 120 millions de 
noms) aux sociétés de marketing direct. Les autres 
concurrents de Equifax, notamment TRW et Trans Union, 
furent aussi concernés par cette injonction[@upi1991; @Kristof1992]. 

Pour anticiper à l'avance tout litige, Equifax lança «&nbsp;
Buyer's market&nbsp;», une base de données construite 
exclusivement en connaissance des clients et avec leur 
consentement écrit. Pour un abonnement de 10&nbsp;dollars par 
an, les consommateurs pouvaient s'inscrire sur des listes selon leurs préférences, et 
avaient la garantie de recevoir au moins l'équivalent de 
250 dollars de coupons de réduction. En moins de six mois, 
elle rassembla plus d'un million de clients[@Shaver1996, p. 54], un nombre largement suffisant pour segmenter, 
catégoriser, analyser, et vendre les données, 
c'est-à-dire les prémices d'un retour à la situation 
précédente. L'annonce de lancement parut notamment en 
ligne dans une *newsletter*[^teleputting]. 

[^teleputting]: *The Teleputing Hotline. The Worldwide Network Letter*. Vol. 3 No. 83 &ndash; 23/10/1990. «&nbsp;Equifax charging consumers to be in database  &ndash;  Equifax, a U.S. leader in computerized credit reports, is testing a program called Buyer's Market. Consumers pay to choose the types of mailing lists they want to be on, and list those they want to be off. This is the first time the company has tried to become a consumer brand name. The program costs $10 per year, but the company promises at least $250 in discount coupons to members, for goods and services they want to buy. The first batch of coupons are enclosed in the survey form which is part of the program. Consumers who want to join Buyer's Market can call 1-800-BUYRMKT, or 1-800-289-7658. [Lien](http://textfiles.com/magazines/MISC/v3i83.txt).

Avec cette nouveauté de la part d'Equifax, la preuve 
était faite que l'acquisition de données  permettant de 
tracer l'histoire des consommateurs et déterminer leurs 
profils pouvait très simplement passer par le 
consentement. Cela allait changer du tout au tout le 
secteur du marketing direct&nbsp;:

- le consentement est une attitude volontaire du 
consommateur qui permet d'acquérir des informations qui, 
par définition ne sont plus réputées comme faisant 
partie de sa vie privée (elle même définie comme la 
somme des informations qu'on ne souhaite pas explicitement 
divulguer),
- en corollaire, la vie privée se définit aussi comme ce 
que le consommateur refuse de divulguer pour une 
exploitation commerciale,
- par son consentement, le consommateur accepte que ses 
données soient analysées et qu'on puisse en inférer 
d'autres informations dont il n'est pas en mesure de 
maîtriser ni le contenu ni l'usage,
- les informations que le consommateur souhaite divulguer 
en échange d'une rémunération (dans le cas d'Equifax, il 
s'agit de coupons de réduction, mais cela peut être aussi 
bien un service comme une adresse e-mail, le stockage de 
fichiers, etc.[^haha]), sont par conséquent une 
propriété du consommateur dont il peut disposer comme il 
l'entend.

[^haha]: Anachronisme&nbsp;: évidemment, nous anticipons le contexte des années 2000.

# Années 1990 en vue&nbsp;: défis des consommateurs

En 1989, le comité d'experts sur la protection des 
données rendit son rapport au Conseil de l'Europe. Il 
affirmait[@CoE1989]&nbsp;: 

> Nous assistons à une *banalisation* progressive du 
phénomène du traitement informatique des données qui 
présente en soi un danger dans la mesure où les individus 
ne sont peut-être pas pleinement conscients de toutes les 
conséquences de ces actes technologiques. En fait, il y a 
désormais la possibilité d'une surveillance totale de 
l'individu.

La suite prouva néanmoins que le débat dans la société 
était bel et bien présent et que des discussions avaient 
lieu, même si elles sensibilisaient les professionnels, 
une petite frange attentive de la population.

En effet, au début des années 1990, la valeur des 
données personnelles a changé de nature. Nous avons vu 
qu'elle a toujours été un bien marchand dès lors que des 
listes pouvaient être vendues. Nous avons aussi vu que les 
techniques d'analyse permettaient d'accroître leur valeur. 
Et nous venons de voir que le consentement à la 
divulgation permettait au consommateur d'attribuer une 
valeur d'échange à ses propres données personnelles. Il 
s'engage alors à cette période une discussion au sujet de 
la valeur marchande des données personnelles du point de 
vue individuel. 

C'est Mary Gardiner Jones, la première femme commissaire 
de la Federal Trade Commission (de 1964 à 1973) et 
militante des droits des consommateurs qui ouvrit le débat 
par un point de vue très éclairé sur la question de la 
vie privée et les défis que les instances de défense des 
consommateurs auront à relever dans un avenir proche. Dans 
un article en 1991, elle exprime les difficultés, pour le 
consommateur, à protéger sa vie privée[@Jones1991]&nbsp;:

> Il faut reconnaître que les citoyens n'ont pas vraiment 
le choix de divulguer ou non des renseignements personnels 
à leur sujet ou d'éviter de les recueillir s'ils veulent 
participer à la société et à l'économie du pays. Comme 
on l'a observé dans une étude récente, peu de citoyens 
ont la possibilité d'éviter les relations avec les 
organismes  d'enregistrement. Cela revient à renoncer non 
seulement au crédit, mais aussi aux assurances, à 
l'emploi, au régime d'assurance-maladie, à l'éducation 
et à de nombreux services gouvernementaux destinés aux 
particuliers. Ainsi, les conditions dans lesquelles le 
consentement peut être donné correctement sont 
essentielles pour protéger les consommateurs contre le 
consentement forcé ou par inadvertance. Bien que plusieurs 
lois aient expressément permis la divulgation 
consensuelle, aucune ne définit les circonstances dans 
lesquelles un tel consentement doit être obtenu.

Ces arguments, semble-t-il frappés de bon sens, furent 
bientôt contredits d'un point de vue purement économique, 
lorsque dans un article retentissant (publié en 1996) 
Kenneth Laudon se livre à un état des lieux de la vie 
privée à l'heure des grands projets de base de données. 
Il définit la vie privée comme suit&nbsp;: «&nbsp;La vie privée 
est le droit (*claim*) moral des individus à être 
laissés tranquilles et de contrôler la circulation de 
l'information les concernant&nbsp;». Pour Laudon, ce contrôle 
relève des libertés individuelles et, partant, il plaide 
pour un marché de la donnée personnelle et le droit des 
individus de vendre leurs propres informations moyennant 
des contrôles par une agence fédérale et une 
standardisation. Pour lui, cette solution permettrait de 
diminuer les atteintes à la vie privée (par l'équilibre 
du marché et le jeu des coûts) et à quiconque de 
maîtriser les flux d'informations sur sa 
personne[@Laudon1993]&nbsp;: 

> L'une des possibilités est la création de marchés 
nationaux de l'information (MNI) dans lesquels les 
informations sur les particuliers sont achetées et vendues 
à un prix d'équilibre. Les institutions qui recueillent 
des renseignements personnels seraient autorisées à 
vendre des paniers de renseignements à d'autres 
institutions qui sont prêtes à payer pour les obtenir. 
Chaque panier contiendrait des renseignements standards 
choisis sur, disons, 1&nbsp;000 personnes (nom, adresse, etc.), 
des données démographiques de base lorsqu'elles sont 
disponibles et des renseignements spécifiques, par exemple sur 
la santé, les finances, le travail ou le marché. 
Différents marchés peuvent exister pour différents types 
d'informations, par exemple les actifs financiers, les 
données de crédit, les données de santé, les données 
gouvernementales et les données destinées au marketing 
général. (&hellip;) Le fait est que, dans l'ensemble, les 
atteintes à la vie privée diminueront et qu'il y aura une 
nette augmentation de la protection de la vie privée parce 
que le coût de l'atteinte à la vie privée augmentera. 
Les gens comprendront mieux la circulation de l'information 
dans la société (les marchés la rendront visible), la 
circulation sera plus institutionnalisée et moins 
secrète, et ils auront davantage de contrôle sur le sort de 
leur propre information. Le concept de marché inclut la 
possibilité de se retirer, de protéger entièrement sa 
vie privée.

La proposition de Kenneth Laudon doit être comprise comme 
un point de vue pragmatique avant tout. Il considère que, 
dans la société de consommation, un équilibre doit se 
faire entre la firme et le client. Si la firme possède 
davantage d'informations que le client, ce dernier n'est 
plus dans une position de négociation. Dès lors, puisque 
l'information est pourtant nécessaire à la firme pour 
profiler et adapter ses productions au potentiel d'achat, 
le client doit trouver un autre moyen pour rétablir 
l'équilibre de négociation&nbsp;: se transformer lui-même en 
producteur. Et que produit-il&nbsp;? des comportements de 
consommation.

Bien entendu l'idée de Kenneth Laudon sera reprise bien 
des fois&hellip; Même caricaturée à l'extrême. Sur ce point, 
on peut citer la publication de Génération Libre, 
un think-tank français ultra-libéral, intitulée *Mes data 
sont à moi. Pour une patrimonialité des données 
personnelles*[@genlibr2018]. Bien que très 
récente, cette publication s'inscrit dans la lignée d'articles  plus ou moins sensationnalistes publiés sur le même thème depuis les années 1990 à aujourd'hui. Elle est illustrative, faute d'être sérieuse, mais elle servira à la démonstration. 

Prenant comme point d'appui la notion de patrimonialité 
des informations relatives à la vie privée (une acception 
néanmoins fort discutable[^manquebrandeis])  les auteurs 
du rapport *Mes data sont à moi* plaident pour une 
conception individualiste de la production de données qui, 
tout comme un capital personnel, devrait pouvoir être 
marchandisé au titre du droit de propriété. Évidemment,
les auteurs ne pouvaient pas citer Kenneth 
Laudon, même au titre de la paternité de la théorisation 
d'un modèle économique de la marchandisation des données 
personnelles par les individus. En effet, Kenneth Laudon ne 
part pas du principe simpliste que les informations 
personnelles sont entièrement sujettes au droit de la 
propriété privée. En réalité, le principe réside 
essentiellement dans la recherche d'un compromis entre le 
besoin des firmes à obtenir des informations et le droit 
à la vie privée. En d'autres termes, la vie privée n'est 
pas négociable (et Kenneth Laudon insiste bien sur le 
rôle de l'État et des limitations légales quant au type 
de données autorisées ou non à la vente), ce qui est 
négociable, ce sont des informations sur la base de leur 
valeur utilitaire.

[^manquebrandeis]: Il aurait fallu aller chercher cette acception dans l'article &ndash; non cité dans ce rapport &ndash; de Warren et Brandeis en 1890, tout en effectuant un distinguo de taille entre la conception nord-américaine de la *privacy*-propriété et une conception européenne davantage axée sur la question de la dignité. 

Le modèle de Kenneth Laudon est donc avant tout à comprendre 
comme le fruit d'un contexte économique et, comme bien 
souvent les modèles économiques, il ne prend pas en 
compte une approche sociologique&nbsp;: que pensent les 
consommateurs de leurs informations personnelles&nbsp;?

Sur ce point, c'est le sociologue David Lyon qui, à peine 
deux ans plus tard, apporte des réponses. Spécialisé 
dans les questions liées aux technologies et leurs 
influences sociales, il publie un livre intitulé *The 
Electronic Eye: The Rise of Surveillance Society*. David 
Lyon se focalise essentiellement sur le thème de la 
société de contrôle et sur l'impact social des 
technologies, des lois et des techniques qui font de la 
surveillance un régime de gouvernement. Néanmoins, la 
surveillance effectuée par les firmes occupe une place 
importante dans son ouvrage, car cette forme de surveillance 
par l'intermédiaire de la captation des informations 
personnelles est aussi un instrument de contrôle social. 
Intrinsèquement, le marketing effectue un tri social, 
notamment parce, comme nous l'avons vu, il segmente, 
analyse et recoupe les profil en créant des groupes 
sociaux en fonction de leurs intérêts et leurs potentiels 
économiques. Cette activité, en prenant son essor 
technologique grâce aux bases de données et à 
l'informatique, exerce de fait un pouvoir de contrôle (on 
ne propose pas les mêmes produits aux mêmes personnes). 
Aussi, dans une société de consommation, faire l'objet de 
ce contrôle est une activité sociale plus ou moins 
contrainte.

L'ouvrage de David Lyon contient de nombreux exemples. Il 
cite notamment une étude réalisée dans une rue de New 
York. On y demande aux passants ce qu'ils pensent de l'
«&nbsp;atteinte à la vie privée&nbsp;» par les
«&nbsp;technologies modernes&nbsp;». 90% des interrogés se disent concernés. Le
lendemain, on offre une carte de crédit d'achat avec taux 
d'intérêt très avantageux&nbsp;: 90% des interrogés 
remplissent entièrement le bulletin d'inscription en 
fournissant leur numéro de sécurité sociale, le numéro 
de compte en banque, et d'autres informations à propos de 
leurs autres cartes de crédit. Pour David Lyon, le 
principe de la surveillance des consommateurs réside 
entièrement dans ce type d'échange où l'on présente des 
avantages de consommation pour que les firmes en tirent des 
informations valorisables. Mais le plus important est que, 
bien que créant un déséquilibre comme l'a montré 
Kenneth Laudon, ce type d'échange est réputé par les 
consommateurs comme
«&nbsp;indiscutablement désirable&nbsp;»[@Lyon1994, p. 140].

Ce que montre David Lyon, c'est que disposer de ses 
données personnelles n'est pas un choix aussi libre qu'on 
pourrait le prétendre[@Lyon1994, p. 137]&nbsp;: 

> Pour la majorité, la consommation est devenue 
l'assimilant total, le guide moral et l'intégrant social 
de la vie contemporaine dans les sociétés riches.  
L'ordre social &ndash;&nbsp;et donc une forme douce de contrôle 
social&nbsp;&ndash; est maintenu par la stimulation et la 
canalisation de la consommation, et c'est ainsi que 
survient la surveillance des consommateurs, mais ceci est 
réalisé au nom de l'individualité, du plus grand choix 
et de la liberté des consommateurs.

Pour donner une idée de la situation héritée des années 
1980 qui illustre explicitement la notion de tri social 
exprimée par David Lyon, on peut se pencher sur l'étude 
d'Eleanor Novek et *al.* parue en 1990. Cette étude 
démontre quelle est la valeur d'un nom dans l'économie 
des données personnelles. En effet, on peut rester 
étonné devant le gigantisme des conglomérats 
spécialisés dans l'information et le marketing de masse 
depuis le milieu des années 1980. À eux seuls, depuis le 
pionnier du marketing de base de données (American 
Express) jusqu'à ceux spécialisés dans le crédit (Dun & 
Bradstreet), les transactions (Equifax, Inc) ou les études 
de marché (RH Donnelly), «&nbsp;en 1986, ces sociétés et 
d'autres ont loué et vendu au total 30,8 milliards de noms&nbsp;».
En 1990, l'activité est devenue si lucrative que 
certaines sociétés gagnent «&nbsp;plus d'argent en vendant 
des noms qu'en vendant des produits traditionnels&nbsp;». Dès 
lors, l'article relate les résultats d'une étude portant 
sur la manière dont les différents critères de 
prédiction (le nombre de noms d'une liste, la période 
couverte, la nature des transactions concernées, le 
caractère démographique, géographique, etc.) influent 
sur la valeur de vente des listes. Le résultat n'est pas 
étonnant[@Novek1990]&nbsp;: 

> Le coût moyen des listes de noms de personnes à revenu 
élevé est plus élevé que celui des listes de personnes 
à revenu faible ou moyen. La valeur des noms d'hommes 
semble légèrement plus élevée que celle des femmes, 
mais il y a très peu de variations dans les moyennes des 
trois groupes d'âge. Les listes de noms minoritaires 
avaient une valeur légèrement inférieure à celle des 
listes sans identité raciale. Il est intéressant de noter 
que les listes qui ne contiennent aucune information 
démographique explicite ou implicite (autre que celle qui 
peut être déduite de la catégorie de produits) sont plus 
appréciées que les listes qui identifient clairement le 
consommateur faiblement payé et les listes minoritaires. 
Encore une fois, nous notons la dévaluation relative des 
listes de noms associés à un faible potentiel d'achat des 
consommateurs.

Ce tri social est pourtant nécessaire à l'organisation de 
la concurrence dans le marché de la consommation de masse. 
Et c'est justement un problème, selon les auteurs. En 
effet, il est démontré que plus les profils effectuent 
une segmentation des consommateurs, plus cette segmentation 
crée elle-même une segmentation de l'information aux 
consommateurs, ces derniers ayant de moins en moins de 
choix puisque les propositions du marché sont adaptées à 
leur segment. Il s'ensuit logiquement une dépendance de 
plus en plus importante du consommateur au flux 
d'information maîtrisé par les vendeurs. Or, plus ces 
derniers centralisent des quantités de données 
marketing, moins les données sont disponibles sur le marché de 
l'information, ce qui crée en retour un déséquilibre de 
la concurrence. Si la théorie néoclassique d'un marché 
de libre et équitable concurrence supposait une 
rationalité du consommateur capable de choisir en fonction des informations disponibles, le consommateur n'est clairement 
plus en mesure d'exercer ce choix, il ne développe plus de «&nbsp;compétences en recherche de marché&nbsp;», et même les nouveaux marchés ont des difficultés à émerger car les informations permettant d'identifier les besoins sont centralisées par des monopoles.

Autrement dit, Novek et *al.* expriment en termes 
économiques ce que David Lyon montre d'un point de vue 
sociologique&nbsp;: les limites de la marchandisation des 
données personnelles sont celles de la liberté des 
consommateurs. La société de surveillance-consommation, 
de ce point de vue, n'est pas une société de la 
coercition mais une société éminemment conservatrice, et 
dont les possibilités d'innovation se cantonnent à des 
niches parmi les clientèles au plus fort potentiel 
d'achat. Cela laisse seulement aux autres catégories 
l'espoir inassouvi d'accéder à un choix plus large ou 
bien la satisfaction d'obtenir des biens apparemment 
inaccessibles, une obtention dès lors «&nbsp;socialement&nbsp;» 
acceptée comme une réussite sociale dont elle n'est 
qu'une ombre fugace.


# Conclusion&nbsp;: droit dans le mur&nbsp;?


Nous nous arrêterons en ce début des années 1990 pour ce 
qui concerne la progression chronologique. En octobre 1994 
apparaissait pour la première fois une bannière 
publicitaire sur Internet, sur une idée originale (*banner 
ads*) de Hotwired (aujourd'hui Wired.com). Sans être une 
révolution, le démarchage publicitaire de services 
numériques sur Internet était né et le marketing prit 
une nouvelle dimension. À l'instar de Shoshana Zuboff, 
dont il fut largement question dans les chapitres 
précédents[@masutti201617], nous pouvons y voir l'apogée 
d'un capitalisme de surveillance, c'est-à-dire une 
économie basée sur l'exploitation des informations 
personnelles et la surveillance des individus, créant une 
concentration des capitaux par la concentration des 
services numériques et l'extraction unilatérale des 
données.

Il serait néanmoins caricatural de réduire au seul *data 
mining* amélioré la stratégie marketing des grandes 
firmes qui prirent leur essor dans les années 1990. Tout 
l'enjeu pour elles, héritant de deux décennies 
d'innovations en statistiques, bases de données et 
recherches en Intelligence Artificielle, est justement de 
faire sortir l'Intelligence Artificielle et le *machine 
learning* du monde universitaire vers le développement et 
l'amélioration des modèles de profilage. L'idée n'est 
plus tellement de travailler les processus de décision à 
partir des bases de données, mais d'automatiser les 
processus de surveillance, le ciblage et la communication. 
Cela suppose d'avoir à disposition un modèle économique 
permettant d'intégrer la recherche et le développement, 
donc des actifs financiers en conséquence, d'où le besoin 
de diversifier, monopoliser l'activité et concentrer les 
capitaux.

Nous avons montré jusqu'ici que la société de 
consommation était devenue une société de la 
surveillance entre 1960 et le début des années 1990 parce 
qu'une convergence eu lieu entre ces trois éléments&nbsp;:

1. une réaction législative et politique qui assurait les 
garanties du droit à la vie privée, mais se montrait 
insuffisante face aux pratiques d'extraction et 
d'exploitation des données personnelles par les firmes, 
qui inventèrent alors la captation de données par 
consentement, redéfinissant ainsi la vie privée comme un 
bien propre dans le cadre exclusif de la transaction,
2. un essor conjoint du marketing, de la recherche et de 
l'industrie informatique, se nourrissant mutuellement d'innovations techniques et de nouvelles pratiques, 
en particulier avec la naissance de savoir-faire en 
exploitation de bases de données,
3. la conception de la donnée personnelle comme une 
matière première hautement valorisable, si ce n'est par 
l'individu lui-même, en tout cas par une spécialisation 
de firmes à forte croissance économique exerçant des 
monopoles d'exploitation.

Chaque décennie s'est vue marquée par des réactions 
publiques, juridiques, intellectuelles mais le début des 
années 1990 inaugure une période de réflexion sur la 
pertinence des modèles économiques que la société de 
surveillance nous propose. Dès lors, comment expliquer, 
par exemple, que selon une étude de la Federal Trade 
Commission en 2017, 82% des utilisateurs de Google ne 
souhaiteraient pas payer quoi que ce soit pour empêcher la 
collecte d'information sur eux[@Fuller2017]&nbsp;? Aujourd'hui, en sommes nous seulement restés au stade de la réflexion ou bien, 
comme le montrait David Lyon, la surveillance des firmes 
est bel et bien devenue un outil de contrôle social 
surpuissant&nbsp;?

L'une des pistes que nous pouvons suivre est celle de 
l'émergence de monopoles dont les activités économiques 
couvrent une telle part de la vie sociale qu'il devient 
impossible, pour les consommateurs, d'exercer un véritable 
choix éclairé. Mais comme nous l'avons vu, l'exercice du 
monopole ne se limite pas à capter la clientèle&nbsp;: il faut 
valoriser l'information sur elle de manière à exploiter 
son potentiel en la segmentant. Pour prendre un exemple, le 
brevet que Facebook a déposé en 2016 et publié en 2018, 
est intitulé «&nbsp;Socioeconomic group classification based 
on user features&nbsp;»[^affordance2018]. En d'autres termes, il 
s'agit d'un outil permettant de classifier les utilisateurs 
en groupes socio-économiques. En réalité, ce n'est
qu'un outil, version actualisée, qui repose sur les mêmes 
principes développés par les bureaux d'évaluation de 
crédit dans les années 1960. On peut en avoir une lecture 
blasée. En fait, en ce qui concerne la méthode, 
l'innovation n'a plus cours depuis longtemps, seule la 
technologie compte au titre de l'optimisation et de 
l'efficacité de l'exploitation des données et, partant, 
l'exercice du monopole consiste essentiellement à 
monopoliser la technologie (dans le cas de Facebook, ici, 
cela passe par le dépôt de brevet).

[^affordance2018]: Olivier Ertzscheid, sur son blog Affordance.info, consacre un billet au sujet de ce brevet&nbsp;: «&nbsp;Facebook détecte notre classe sociale. Et déclenche la lutte (algorithmique) finale&nbsp;»[@Ertzscheid2018].

Enfin, ce qui permet sans doute le mieux de comprendre les 
mécanismes qui finirent par produire cette économie de la 
surveillance, c'est la difficulté avouée de la loi, à 
l'image du Privacy Act, à étendre la régulation dans les 
pratiques privées des entreprises qui collectent les 
informations. Comme le dit l'aide sénatorial Davidson lors 
des discussions au Congrès en 1974[@cgo1976, p. 1209]&nbsp;: 

> Nous reconnaissons la difficulté de définir la façon 
dont les organismes utilisent l'information, mais ils 
devraient au moins fournir certaines normes pour permettre 
à une personne de contrôler l'information.

Cette impuissance avouée l'était en raison de la croyance 
libérale en la capacité des individus à disposer 
librement de leurs informations et en la croyance 
non-interventionniste que le marché de l'information ne 
peut s'équilibrer qu'avec la volonté des opérateurs. 
Cette pensée a marqué jusqu'à aujourd'hui les 
équilibres et les déséquilibres du rapport entre le 
droit à la vie privée et les concessions plus ou moins 
contraintes que les utilisateurs-consommateurs doivent 
faire. Elle fut cristallisée par le principe du 
consentement, une forme moderne de  la tartufferie que l'on 
retrouve dans les Conditions Générales d'Utilisation des 
services de nombre de services en ligne.


La réponse est à la charnière entre la technique et le 
social. Elle réside peut-être dans les principes du 
logiciel libre et des règles éthiques qui président au 
partage du code et, donc, de l'utilisation des données. Le 
partage et l'ouverture, s'ils étaient hier aux sources des 
plus pertinentes innovations, dont le réseau Internet et 
ses protocoles, sont devenus aujourd'hui des outils de 
réappropriation de l'économie numérique, à la fois par les concepteurs techniques et les utilisateurs. C'est sans doute comme cela qu'il faut comprendre rétrospectivement cette idée de Paul 
Baran en 1969[@Baran1969; cité par @Hoffman1969]&nbsp;:

> Quelle belle opportunité pour l'ingénieur informaticien 
d'exercer une nouvelle forme de responsabilité sociale. Il 
n'est pas nécessaire de craindre l'avènement des 
nouvelles technologies informatiques et des communications 
à l'approche de 1984. Au contraire, nous avons en notre 
pouvoir une force qui, si elle est bien maîtrisée, peut 
aider, et non pas entraver, l'aspiration à l'exercice de 
notre droit personnel à la vie privée. Si nous ne 
parvenons pas à exercer ce pouvoir méconnu que seuls 
nous, ingénieurs informaticiens, détenons, le mot 
«&nbsp;personnes&nbsp;» sera réduit à n'être plus qu'un simple nom 
collectif et non une description d'êtres humains 
individuels vivant dans une société ouverte. Cela peut 
sembler paradoxal, mais une société ouverte impose à ses 
membres un droit à la vie privée, et nous aurons assumé 
en grande partie la responsabilité de préserver ce droit.


# Bibliographie

