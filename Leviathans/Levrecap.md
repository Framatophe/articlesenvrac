---
title: "Les nouveaux Leviathans"
date: "01 mars 2018 -- v.0.1 (non publiable)"
author: "Christophe Masutti"
link-citations: true
documentclass: book
classoption: twoside
header-includes:
        \usepackage[utf8]{inputenc}
        \usepackage[french]{babel}
        \usepackage{csquotes}     
        \usepackage{relsize,etoolbox}
        \AtBeginEnvironment{quote}{\smaller}

fontsize: 11pt
papersize: a4
toc-depth: 3
linkcolor: magenta
citecolor: magenta
urlcolor: blue
toccolor: blue
links-as-notes: true
keywords: informatique, histoire, surveillance, marketing, vie privée, consommation, base de données, économie, capitalisme
---

**Licence&nbsp;:** les écrits présents dans ce fichier sont placés sous [Licence Art Libre](http://artlibre.org/).

**Couverture&nbsp;:** *Behemoth and Leviathan*, par William Blake, 1826 (``commons.wikimedia.org``).

**Les sources** de ce fichier sont accessibles depuis [ce dépôt Framagit](https://framagit.org/Framatophe/articlesenvrac/)</a>.

Les articles qui composent ce fichier ont été publiés sur [Framablog](https://framablog.org).

- Les nouveaux Léviathans [Ia](https://framablog.org/?p=6617) et [Ib](https://framablog.org/?p=6626) (2016)
- Les nouveaux Léviathans  [IIa](https://framablog.org/?p=6629) et [IIb](https://framablog.org/?p=6631) (2016)
- Les nouveaux Léviathans  [III](https://framablog.org/2017/07/07/les-nouveaux-leviathans-iii-du-capitalisme-de-surveillance-a-la-fin-de-la-democratie/) (2017)
- Les nouveaux Léviathans  [IV](https://framablog.org/2017/07/07/les-nouveaux-leviathans-iv-la-surveillance-qui-vient/) (2018)
- Les anciens Léviathans [I](https://framablog.org/?p=6637) (2016) et [II](https://framablog.org/?p=6661) (2015)



[Christophe Masutti](http://christophe.masutti.name) est administrateur  de [Framasoft](https://framasoft.org/). À ses moments perdus, il note et partage les réflexions qui motivent son engagement dans le Libre.


# Histoire d'une conversion capitaliste

Dans ce premier chapitre, je tente de synthétiser les transformations des utopies numériques des débuts de l'économie informatique vers ce que S. Zuboff nomme le «&nbsp;capitalisme de surveillance&nbsp;». Dans cette histoire, le logiciel libre apparaît non seulement comme un élément critique présent dès les premiers âges de cette conversion capitaliste, mais aussi comme le moyen de faire valoir la primauté de nos libertés individuelles face aux comportements imposés par un nouvel ordre numérique. La dégooglisation d'Internet n'est plus un souhait, c'est un impératif&nbsp;!


## Techno-capitalisme

Longtemps nous avons cru que le versant obscur du capitalisme était le
monopole. En écrasant le marché, en pratiquant des prix arbitraires, en
contrôlant les flux et la production à tous les niveaux, un monopole est
un danger politique. Devant la tendance monopolistique de certaines
entreprises, le gouvernement américain a donc très tôt mis en place une
stratégie juridique visant à limiter les monopoles. C'est le [Sherman
Anti-Trust Act](https://fr.wikipedia.org/wiki/Sherman_Antitrust_Act) du 2 juillet 1890, ce qui ne rajeunit pas l'économie moderne. Avec cette loi dont beaucoup de pays ont adopté les principes,
le fameux droit de la concurrence a modelé les économies à une échelle
mondiale. Mais cela n'a pas pour autant empêché l'apparition de
monopoles. Parmi les entreprises ayant récemment fait l'objet de
poursuites au nom de lois anti-trust sur le territoire américain et
[en Europe](https://en.wikipedia.org/wiki/Microsoft_Corp_v_Commission), on peut citer Microsoft, qui s'en est toujours tiré à bon compte (compte tenu de son placement financier).

Que les procès soient gagnés ou non, les firmes concernées dépensent des
millions de dollars pour leur défense et, d'une manière ou d'une autre,
trouvent toujours un moyen de contourner les procédures. C'est le cas de
Google qui, en août 2015, devant l'obésité due à ses multiples
activités, a préféré éclater en plusieurs sociétés regroupées sous une
sorte de Holding nommée Alphabet. Crainte de se voir poursuivie au nom
de lois anti-trust&nbsp;? non, du moins ce n'est pas le premier objectif[@obs2015]&nbsp;: ce qui a motivé cet éclatement, c'est de pouvoir rendre plus claires ses différentes activités pour les investisseurs.


![Alphabet. CC-by-sa, Framatophe](imgs/alphabet.png)

Investir dans les sociétés qui composent Alphabet, ce serait donc leur
permettre à toutes de pouvoir initier de nouveaux projets. N'en a-t-il
toujours pas été ainsi, dans le monde capitaliste&nbsp;? Investir, innover,
produire pour le bien de l'humanité. Dans ce cas, quel danger
représenterait le capitalisme&nbsp;? aucun. Dans le monde des technologies
numériques, au contraire, il constitue le moteur idéal pour favoriser
toute forme de progrès social, technique et même politique. Dans nos
beaux pays industriels avancés (hum&nbsp;!) nous n'en sommes plus au temps
des mines à charbon et des revendications sociales [à la Zola](https://fr.wikisource.org/wiki/Germinal)&nbsp;: tout cela est d'un autre âge, celui où le capitalisme était aveugle. Place au
capitalisme éclairé.

Convaincu&nbsp;? pas vraiment n'est-ce pas&nbsp;? Et pourtant cet exercice de remise en question du capitalisme a déjà été effectué entre la fin des années 1960 et les années 1980. Cette histoire est racontée par Fred
Turner, dans son excellent livre *Aux sources de l'utopie numérique. De la contre-culture à la cyberculture, Stewart Brand un homme d'influence*[@turner2012]. Dans ce livre,
Fred Turner montre comment les mouvements communautaires de
contre-culture ont soit échoué par désillusion, soit se sont recentrés
(surtout dans les années 1980) autour de *techno-valeurs*, en
particulier portées par des leaders charismatiques géniaux à la manière
de Steve Jobs un peu plus tard. L'idée dominante est que la
revendication politique a échoué à bâtir un monde meilleur&nbsp;; c'est en
apportant des solutions techniques que nous serons capables de résoudre
nos problèmes.

Ne crachons pas dans la soupe&nbsp;! Certains principes qui nous permettent
aujourd'hui de surfer et d'aller sur Wikipédia, sont issus de ce
mouvement intellectuel. Prenons par exemple [Ted Nelson](https://fr.wikipedia.org/wiki/Ted_Nelson), qui n'a
rien d'un informaticien et tout d'un sociologue. Au milieu des années
1960, il invente le terme *hypertext* par lequel il entend la
possibilité de lier entre eux des documents à travers un réseau
documentaire. Cela sera formalisé par la suite en système hypermédia
(notamment avec le travail de Douglas Engelbart). Toujours est-il que
Ted Nelson, en fondant le [projet Xanadu](https://fr.wikipedia.org/wiki/Projet_Xanadu) a
proposé un modèle économique d'accès à la documentation et de partage
d'information (pouvoir par exemple acheter en ligne tel ou tel document
et le consulter en lien avec d'autres). Et il a fallu attendre dix ans
plus tard l'Internet de papa pour développer de manière concrète ce qui
passait auparavant pour des utopies et impliquait un changement radical
de modèle (ici, le rapport livresque à la connaissance, remplacé par une
appropriation hypertextuelle des concepts).

La conversion des hippies de la contre-culture nord-américaine (et
d'ailleurs aussi) au techno-capitalisme ne s'est donc pas faite à partir
de rien. Comme bien souvent en histoire des techniques, c'est la
convergence de plusieurs facteurs qui fait naître des changements
technologiques. Ici les facteurs sont&nbsp;:

- des concepts travaillés théoriquement comme l'*hypertext*,
  l'homme augmenté, l'intelligence artificielle, et tout ce qui sera par
  la suite dénommé «&nbsp;cyber-culture&nbsp;»,
- des innovations informatiques&nbsp;: les réseaux (par exemple, la
  commutation de paquets), les micro-computers et les systèmes
  d'exploitation, les langages informatiques (Lisp, C, etc.),
- des situations politiques et économiques particulières&nbsp;: la guerre
  froide (développement des réseaux et de la recherche informatique),
  les déréglementations des politiques néolibérales américaines et
  anglaises, notamment, qui permirent une expansion des marchés
  financiers et donc l'émergence de start-ups dont la Silicon Valley est
  l'emblème le plus frappant, puis plus tard l'arrivée de la «&nbsp;bulle Internet&nbsp;», etc.


## Le logiciel libre montre la  faille

Pour tout changement technologique, il faut penser les choses de manière
globale. Ne jamais se contenter de les isoler comme des émergences
sporadiques qui auraient révolutionné tout un contexte par un jeu de
dominos. Parfois les enchaînements n'obéissent à aucun enchaînement
historique linéaire et ne sont dus qu'au hasard d'un contexte favorable
qu'il faut identifier. On peut se demander quelle part favorable de ce
contexte intellectuellement stimulant a permis l'émergence de concepts
révolutionnaires, et quelle part a mis en lumière les travers de la
«&nbsp;contre-culture dominante&nbsp;» de la future Silicon Valley.

Tel l'œuf et la poule, on peut parler du
[logiciel libre](https://fr.wikipedia.org/wiki/Logiciel_libre) dont
le concept a été formalisé par Richard Stallman[@stallman2010]. Ce dernier évoluait
dans le milieu high-tech du M.I.T. durant cette période qui vit
l'émergence des hackers &ndash;&nbsp;voir sur ce point le livre de Steven Lévy,
 *L'Éthique des hackers*[@Levy2013, p. 169 et suiv.]. Dans le
groupe de hackers dont R. Stallman faisait partie, certains se
disputaient les programmes de Machines Lisp[@stallman2010, chap. 7]. Deux sociétés avaient été fondées, distribuant ce types de
machines. Le marché tacite qui était passé était de faire en sorte que
tous les acteurs (l'université et ces entreprises) puissent profiter des
améliorations apportées par les uns et les autres. Néanmoins, une firme
décida de ne plus partager, accusant l'université de fausser la
concurrence en reversant ses améliorations à l'autre entreprise. Cet
évènement, ajouté à d'autres épisodes aussi peu glorieux dans le monde
des hackers, amena Richard Stallman à formaliser juridiquement l'idée
qu'un programme puisse non seulement être partagé et amélioré par tous,
mais aussi que cette caractéristique puisse être virale et toucher
toutes les améliorations et toutes les versions du programme.

En fait, le logiciel libre est doublement révolutionnaire. Il s'oppose
en premier lieu à la logique qui consiste à privatiser la connaissance
et l'usage pour maintenir les utilisateurs dans un état de dépendance et
établir un monopole basé uniquement sur le non-partage d'un programme
(c'est à dire du code issu d'un langage informatique la plupart du temps
connu de tous). Mais surtout, il s'oppose au contexte dont il est
lui-même issu&nbsp;: un monde dans lequel on pensait initier des modèles
économiques basés sur la capacité de profit que représente l'innovation
technologique. Celle-ci correspondant à un besoin, elle change le monde
mais ne change pas le modèle économique de la génération de profit par
privatisation de la production. Or, avec le logiciel libre,
l'innovation, ou plutôt le programme, ne change rien de lui-même&nbsp;: il
est appelé à être modifié, diffusé et amélioré par tous les utilisateurs
qui, parce qu'ils y participent, améliorent le monde et partagent à la
fois l'innovation, son processus et même le domaine d'application du
programme. Et une fois formalisé en droit par le concept de licence
libre, cela s'applique désormais à bien d'autres productions que les
seuls programmes informatiques&nbsp;: les connaissances, la musique, la
vidéo, le matériel, etc.

Le logiciel libre représente un épisode divergent dans le *story telling* des Silicon Valley et des Steve Jobs du monde entier. Il
correspond à un moment où, dans cette logique de conversion des
techno-utopistes au capitalisme, la principale faille fut signalée&nbsp;:
l'aliénation des utilisateurs des technologies. Et le remède fut
compris&nbsp;: pour libérer les utilisateurs, il faut qu'il participent
eux-mêmes en partageant les connaissances et la contrepartie de ce
partage est l'obligation du partage. Et on sait, depuis lors, que ce
modèle alternatif n'empêche nullement le profit puisque celui-ci se loge
dans l'exercice des connaissances (l'expertise est monnayable) ou dans
la plus-value elle-même de programmes fortement innovants qui permettent
la production de services ou de nouveaux matériels, etc.

## Économie du partage&nbsp;?

Il faut comprendre, néanmoins, que les fruits de l'intellectualisme
californien des seventies n'étaient pas tous intentionnellement tournés
vers le dieu Capital, un peu à la manière dont Mark Zuckerberg a fondé
Facebook en s'octroyant cette fonction salvatrice, presque morale, de
facilitateur de lien social. Certains projets, voire une grande
majorité, partaient réellement d'une intention parfaitement en phase
avec l'idéal d'une meilleure société fondée sur le partage et le bien
commun. La plupart du temps, ils proposaient un modèle économique fondé
non pas sur les limitations d'usage (payer pour avoir l'exclusivité de
l'utilisation) mais sur l'adhésion à l'idéologie qui sous-tend
l'innovation. Certains objets techniques furent donc des véhicules
d'idéologie.

Dans les années 1973-1975, le projet
[Community Memory](https://en.wikipedia.org/wiki/Community_Memory) véhiculait de l'idéologie[@doub2016]. Ses fondateurs, en particulier [Lee Felsenstein](https://en.wikipedia.org/wiki/Lee_Felsenstein) (inventeur du premier ordinateur portable en 1981), se retrouvèrent par la suite au [Homebrew Computer Club](https://en.wikipedia.org/wiki/Homebrew_Computer_Club), le club d'informatique de la Silicon Valley naissante parmi lesquels on retrouva plus tard Steve Jobs. La Silicon Valley doit
beaucoup à ce club, qui regroupa, à un moment ou à un autre, l'essentiel
des hackers californiens. Avec la Community Memory, l'idée était de
créer un système de communication non hiérarchisé où les membres
pouvaient partager de l'information de manière décentralisée. Community
Memory utilisait les services de Resource One, une organisation non
gouvernementale crée lors de la crise de 1970 et l'invasion américaine
du Cambodge. Il s'agissait de mettre en place un accès à un calculateur
pour tous ceux qui se reconnaissaient dans le mouvement de
contre-culture de l'époque. Avec ce calculateur (Resource One
Generalized Information Retrieval System &ndash;&nbsp;ROGIRS), des terminaux
maillés sur le territoire américain et les lignes téléphoniques WATTS,
les utilisateurs pouvaient entrer des informations sous forme de texte
et les partager avec tous les membres de la communauté, programmeurs ou
non, à partir de terminaux en accès libre. Il s'agissait généralement de
petites annonces, de mini-tracts, etc. dont les mots-clé permettaient le
classement.

![Community Memory Project 1975 Computer History Museum Mountain View California CC By Sa Wikipedia](imgs/communitymemory.jpg)

Pour Lee Felsenstein, le principal intérêt du projet était de
démocratiser l'usage de l'ordinateur, en réaction au monde universitaire
ou aux élus des grandes entreprises qui s'en réservaient l'usage. Mais
les caractéristiques du projet allaient beaucoup plus loin&nbsp;: pas de
hiérarchie entre utilisateurs, respect de l'anonymat, aucune autorité ne
pouvait hiérarchiser l'information, un accès égalitaire à l'information.
En cela, le modèle s'opposait aux modèles classiques de communication
par voie de presse, elle-même dépendante des orientations politiques des
journaux. Il s'opposait de même à tout contrôle de l'État, dans un
climat de méfiance entretenu par les épisodes de la Guerre du Vietnam ou
le scandale du WaterGate. Ce système supposait donc, pour y accéder, que
l'on accepte&nbsp;:

1. de participer à et d'entrer dans une communauté et
2. que les comptes à rendre se font au sein de cette communauté
  indépendamment de toute structure politique et surtout de l'État. Le
  système de pair à pair s'oppose frontalement à tout moyen de contrôle
  et de surveillance externe.

Quant au fait de payer quelques cents pour écrire (entrer des
informations), cela consistait essentiellement à couvrir les frais
d'infrastructure. Ce don participatif à la communauté finissait de
boucler le cercle vertueux de l'équilibre économique d'un système que
l'on peut qualifier d'humaniste.

Si beaucoup de hackers avaient intégré ce genre de principes et si
Internet fut finalement conçu (pour sa partie non-militaire) autour de
protocoles égalitaires, la part libertarienne et antiétatiste ne fut pas
toujours complètement (et aveuglément) acceptée. L'exemple du logiciel
libre le montre bien, puisqu'il s'agit de s'appuyer sur une justice
instituée et réguler les usages.

Pour autant, combinée aux mécanismes des investissements financiers
propres à la dynamique de la Silicon Valley, la logique du «&nbsp;moins
d'État&nbsp;» fini par l'emporter. En l'absence (volontaire ou non) de
principes clairement formalisés comme ceux du logiciel libre, les
sociétés high-tech entrèrent dans un système concurrentiel dont les
motivations consistaient à concilier le partage communautaire,
l'indépendance vis-à-vis des institutions et la logique de profit. Le
résultat ne pouvait être autre que la création d'un marché privateur
reposant sur la mise en commun des pratiques elles-mêmes productrices de
nouveaux besoins et de nouveaux marchés. Par exemple, en permettant au
plus grand nombre possible de personnes de se servir d'un ordinateur
pour partager de l'information, l'informatique personnelle devenait un
marché ouvert et prometteur. Ce n'est pas par hasard que la fameuse
«&nbsp;Lettre aux hobbyistes&nbsp;» de Bill Gates[@gates1976] fut écrite en premier lieu à l'intention
des membres du Homebrew Computer Club&nbsp;: le caractère mercantile et
(donc) privateur des programmes informatiques est la contrepartie
obligatoire, la conciliation entre les promesses d'un nouveau monde
technologique a-politique et le marché capitaliste qui peut les
réaliser.

C'est de ce bain que sortirent les principales avancées informatiques
des années 1980 et 1990, ainsi que le changement des pratiques qu'elles
finirent par induire dans la société, de la gestion de nos comptes
bancaires, aux programmes de nos téléphones portables.

## Piller le code, imposer des usages

À la fin des années 1990, c'est au nom de ce réalisme capitaliste, que
les promoteurs de l'[Open Source Initiative](https://fr.wikipedia.org/wiki/Open_Source_Initiative)[@perens1999] avaient compris l'importance de maintenir des codes
sources ouverts pour faciliter un terreau commun permettant d'entretenir
le marché. Ils voyaient un frein à l'innovation dans les contraintes des
licences du logiciel libre tel que le proposaient Richard Stallman et la
Free Software Foundation (par exemple, l'obligation de diffuser les
améliorations d'un logiciel libre sous la même licence, comme l'exige la
licence GNU GPL &ndash;&nbsp;General Public License). Pour eux, l'ouverture du
code est une opportunité de création et d'innovation, ce qui n'implique
pas forcément de placer dans le bien commun les résultats produits grâce
à cette ouverture. Pas de *fair play*&nbsp;: on pioche dans le bien
commun mais on ne redistribue pas, du moins, pas obligatoirement.

Les exemples sont nombreux de ces entreprises qui utilisent du code
source ouvert sans même participer à l'amélioration de ce code, voire en
s'octroyant des pans entiers de ce que des généreux programmeurs ont
choisi de verser dans le bien commun. D'autres entreprises trouvent
aussi le moyen d'utiliser du code sous licence libre GNU GPL en y
ajoutant tant de couches successives de code privateur que le système
final n'a plus rien de libre ni d'ouvert. C'est le cas du système
Android de Google, [dont le noyau est Linux](http://www.gnu.org/philosophy/android-and-users-freedom.fr.html)[@stallman2010].

Jamais jusqu'à aujourd'hui le logiciel libre n'avait eu de plus dur
combat que celui non plus de proposer une simple alternative à
informatique privateur, mais de proposer une alternative au modèle
économique lui-même. Pas seulement l'économie de l'informatique, dont on
voudrait justement qu'il ne sorte pas, mais un modèle beaucoup plus
général qui est celui du pillage intellectuel et physique qui aliène les
utilisateurs et, donc, les citoyens. C'est la raison pour laquelle le
discours de Richard Stallman est un discours politique avant tout.

La fameuse dualité entre open source et logiciel libre n'est finalement
que triviale. On n'a pas tort de la comparer à une querelle d'église
même si elle reflète un mal bien plus général. Ce qui est pillé
aujourd'hui, ce n'est plus seulement le code informatique ou les
libertés des utilisateurs à pouvoir disposer des programmes. Même si le
principe est (très) loin d'être encore communément partagé dans
l'humanité, le fait de cesser de se voir imposer des programmes n'est
plus qu'un enjeu secondaire par rapport à une nouvelle voie qui se
dévoile de plus en plus&nbsp;: pour maintenir la pression capitaliste sur un
marché verrouillé par leurs produits, les firmes cherchent désormais à
imposer des comportements. Elles ne cherchent plus à les induire comme
on pouvait dire d'Apple que le design de ses produits provoquait un
effet de mode, une attitude «&nbsp;cool&nbsp;». Non&nbsp;: la stratégie a depuis un
moment déjà dépassé ce stade.

Un exemple révélateur et particulièrement cynique, la population belge
en a fait la terrible expérience à l'occasion des attentats commis à
Bruxelles en mars 2016 par de sombres crétins, au prix de dizaines de
morts et de centaines de blessés. Les médias déclarèrent en choeur,
quelques heures à peine après l'annonce des attentats, que Facebook
déclenchait le «&nbsp;Safety Check&nbsp;». Ce dispositif propre à la firme avait
déjà été éprouvé lors des attentats de Paris en novembre 2015 et
[un article de Slate.fr](http://www.slate.fr/story/110009/attentats-facebook-safety-check-responsabilite)[@Newm2015] en montre bien les enjeux. Avec ce dispositif, les
personnes peuvent signaler leur statut à leurs amis sur Facebook en
situation de catastrophe ou d'attentat. Qu'arrive-t-il si vous n'avez
pas de compte Facebook ou si vous n'avez même pas l'application
installée sur votre smartphone&nbsp;? Vos amis n'ont plus qu'à se consumer
d'inquiétude pour vous.

![Facebook safety check fr](imgs/safetycheck.png)

La question n'est pas tant de s'interroger sur l'utilité de ce
dispositif de Facebook, mais plutôt de s'interroger sur ce que cela
implique du point de vue de nos comportements&nbsp;:


- Le devoir d'information&nbsp;: dans les médias, on avait l'habitude, il y a
  encore peu de temps, d'avoir à disposition un «&nbsp;numéro vert&nbsp;» ou un
  site internet produits par les services publics pour informer la
  population. Avec les attentats de Bruxelles, c'est le «&nbsp;Safety Check&nbsp;»
  qui fut à l'honneur. Ce n'est plus à l'État d'assurer la prise en
  charge de la détresse, mais c'est à Facebook de le faire. L'État, lui,
  a déjà prouvé son impuissance puisque l'attentat a eu lieu, CQFD. On
  retrouve la doctrine du «&nbsp;moins d'État&nbsp;».
- La morale&nbsp;: avec la crainte qu'inspire le contexte du terrorisme
  actuel, Facebook joue sur le sentiment de sollicitude et propose une
  solution technique à un problème moral&nbsp;: ai-je le devoir ou non,
  envers mes proches, de m'inscrire sur Facebook&nbsp;?
- La norme&nbsp;: le comportement normal d'un citoyen est d'avoir un
  smartphone, de s'inscrire sur Facebook, d'être en permanence connecté
  à des services qui lui permettent d'être localisé et tracé. Tout
  comportement qui n'intègre pas ces paramètres est considéré comme
  déviant non seulement du point de vue moral mais aussi, pourquoi pas,
  du point de vue sécuritaire.


Ce cas de figure est extrême mais son principe (conformer les
comportements) concerne des firmes aussi gigantesques que Google, Apple,
Facebook, Amazon, Microsoft (GAFAM) et d'autres encore, parmi les plus
grandes capitalisations boursières du monde. Ces dernières sont
aujourd'hui capables d'imposer des usages et des comportements parce
qu'elles proposent toutes une seule idéologie&nbsp;: leurs solutions
techniques peuvent remplacer plus efficacement les pouvoirs publics à la
seule condition d'adhérer à la communauté des utilisateurs, de «&nbsp;prendre la citoyenneté&nbsp;» Google, Apple, Facebook, Microsoft. Le rêve californien
de renverser le Léviathan est en train de se réaliser.

## Vers le capitalisme de surveillance

Ce mal a récemment été nommé par une professeure de Harvard,
[Shoshana Zuboff](https://en.wikipedia.org/wiki/Shoshana_Zuboff)
dans un article intitulé
«&nbsp;Big Other: Surveillance Capitalism and the Prospects of an Information
Civilization&nbsp;»[@zuboff_big_2015]. S. Zuboff analyse les implications de ce qu'elle nomme
le «&nbsp;capitalisme de surveillance&nbsp;» dans notre société connectée.

L'expression a récemment été reprise par Aral Balkan, dans un texte
traduit sur le Framablog intitulé&nbsp;:
«&nbsp;La nature du soi à l'ère numérique&nbsp;»[@Balkan2016]. A. Balkan interroge l'impact du
capitalisme de surveillance sur l'intégrité de nos identités, à travers
nos pratiques numériques. Pour le citer&nbsp;:

> La Silicon Valley est la version moderne du système colonial d'exploitation bâti par la Compagnie des Indes Orientales, mais elle n'est ni assez vulgaire, ni assez stupide pour entraver les individus avec des chaînes en fer. Elle ne veut pas être propriétaire de votre corps, elle se contente d'être propriétaire de votre avatar. Et maintenant, (&hellip;) plus ces entreprises ont de données sur vous, plus votre avatar est ressemblant, plus elles sont proches d'être votre propriétaire.


C'est exactement ce que démontre S. Zuboff (y compris à travers toute sa
bibliographie). Dans l'article cité, à travers l'exemple des pratiques
de Google, elle montre que la collecte et l'analyse des données
récoltées sur les utilisateurs permet l'émergence de nouvelles formes
contractuelles (personnalisation, expérience immersive, etc) entre la
firme et ses utilisateurs. Cela induit des mécanismes issus de
l'extraction des données qui débouchent sur deux principes&nbsp;:


- la marchandisation des comportements&nbsp;: par exemple, les sociétés
  d'assurance ne peuvent que s'émouvoir des données géolocalisées des
  véhicules&nbsp;;
- le contrôle des comportements&nbsp;: il suffit de penser, par exemple, aux
  applications de e-health promues par des assurances-vie et des
  mutuelles, qui incitent l'utilisateur à marcher X heures par jour.


Dans un article paru dans le *Frankfurter Allgemeine Zeitung* le 24
mars 2016, «&nbsp;The Secrets of Surveillance Capitalism&nbsp;»[@zuboff_secrets_2106], S. Zuboff relate les propos de
l'analyste (de données) en chef d'une des plus grandes firmes de la
Silicon Valley&nbsp;:

> Le but de tout ce que nous faisons est de modifier le comportement des
gens à grande échelle. Lorsqu'ils utilisent nos applications, nous
pouvons enregistrer leurs comportements, identifier les bons et les
mauvais comportements, et développer des moyens de récompenser les bons
et pénaliser les mauvais.


Pour S. Zuboff, cette logique d'accumulation et de traitement des
données aboutit à un projet de surveillance lucrative qui change
radicalement les mécanismes habituels entre l'offre et la demande du
capitalisme classique. En cela, le *capitalisme de surveillance*
modifie radicalement les principes de la concurrence libérale qui
pensait les individus autonomes et leurs choix individuels, rationnels
et libres, censés équilibrer les marchés. Qu'on ait adhéré ou non à
cette logique libérale (plus ou moins utopiste, elle aussi, sur certains
points) le fait est que, aujourd'hui, ce capitalisme de surveillance est
capable de bouleverser radicalement les mécanismes démocratiques.
J'aurai l'occasion de revenir beaucoup plus longuement sur le
capitalisme de surveillance dans les chapitres suivants, mais on peut néanmoins affirmer sans plus d'élément qu'il pose en pratique des questions politiques d'une rare envergure.

Ce ne serait rien, si, de surcroît certains décideurs politiques
n'étaient particulièrement pro-actifs, face à cette nouvelle forme du
capitalisme. En France, par exemple, la première version du projet de
Loi Travail soutenu par la ministre El Khomri en mars 2016 entrait
parfaitement en accord avec la logique du capitalisme de surveillance.
Dans la première version du projet, au chapitre *Adaptation du
droit du travail à l'ère numérique*, l'article 23 portait sur les
plateformes collaboratives telles Uber. Cet article rendait impossible
la possibilité pour les «&nbsp;contributeurs&nbsp;» de Uber (les chauffeurs VTC)
de qualifier leur emploi au titre de salariés de cette firme[@Simonnet2016], ceci afin d'éviter les luttes sociales comme les travailleurs de Californie
 qui se sont retournés contre Uber[@Lauer2015] dans une bataille juridique mémorable. Si le projet de loi El Khomri cherche à éliminer le salariat du rapport entre travail et justice, l'enjeu dépasse largement le seul point de vue
juridique.

Google est l'un des actionnaires majoritaires de Uber, et ce n'est pas
pour rien&nbsp;: d'ici 5 ou 6 ans, nous verrons sans doute les premières
voitures sans chauffeur de Google arriver sur le marché. Dès lors, que
faire de tous ces salariés chauffeurs de taxi&nbsp;? La meilleure manière de
s'en débarrasser est de leur supprimer le statut de salariés&nbsp;: en créant
une communauté de contributeurs Uber à leur propre compte, il devient
possible de se passer des chauffeurs puisque ce métier n'existera plus
(au sens de corporation) ou sera en voie d'extinction. Ce faisant, Uber
fait d'une pierre deux coups&nbsp;: il crée aussi une communauté
d'utilisateurs, habitués à utiliser une plate-forme de service de
voiturage pour accomplir leurs déplacements. Uber connaît donc les
besoins, analyse les déplacements, identifie les trajets et rassemble un
nombre incroyable de données qui prépareront efficacement la venue de la
voiture sans chauffeur de Google. Que cela n'arrange pas l'émission de
pollution et empêche de penser à des moyens plus collectifs de
déplacement n'est pas une priorité (quoique Google a déjà son service de bus[@Marin2014]).

## Il faut dégoogliser&nbsp;!

Parmi les moyens qui viennent à l'esprit pour s'en échapper, on peut se
demander si le capitalisme de surveillance est soluble dans la bataille
pour le chiffrement qui refait surface à l'occasion des vagues
terroristes successives. L'idée est tentante&nbsp;: si tous les utilisateurs
étaient en mesure de chiffrer leurs communications, l'extraction de
données de la part des firmes n'en serait que contrariée. Or, la
question du chiffrement n'est presque déjà plus d'actualité que pour les
représentants politiques en mal de sensations. Tels certains ministres
qui ressassent le sempiternel refrain selon lequel le chiffrement permet
aux terroristes de communiquer.

Outre le fait que la pratique «&nbsp;terroriste&nbsp;» du chiffrement reste
encore largement à prouver[@Fradin2016], on se rappelle la bataille récente entre le
FBI et Apple dans le cadre d'une enquête terroriste, où le FBI
souhaitait obtenir de la part d'Apple un moyen (exploitation de
*backdoor*) de faire sauter le chiffrement d'un IPhone. Le FBI
ayant finalement trouvé une solution alternative[@AFP2016], que nous apprend cette dispute&nbsp;? Certes, Apple
veut garder la confiance de ses utilisateurs. Certes le chiffrement a
bien d'autres applications bénéfiques, en particulier lorsqu'on consulte
à distance son compte en banque ou que l'on transfère des données
médicales. Dès lors, la mesure est vite prise entre d'un côté des
gouvernements cherchant à déchiffrer des communications (sans même être
sûr d'y trouver quoique ce soit d'intéressant) et la part gigantesque de
marché que représente le transfert de données chiffrées. Peu importe ce
qu'elles contiennent, l'essentiel est de comprendre non pas ce qui est
échangé, mais qui échange avec qui, pour quelles raisons, et comment
s'immiscer en tant qu'acteur de ces échanges. Ainsi, encore un exemple
parmi d'autres, Google a déployé depuis longtemps des systèmes de
consultations médicale à distance, chiffrées bien entendu&nbsp;: «&nbsp;si vous
voulez un tel service, nous sommes capables d'en assurer la sécurité,
contrairement à un État qui veut déchiffrer vos communications&nbsp;». Le
chiffrement est un élément essentiel du capitalisme de surveillance,
c'est pourquoi Apple y tient tant&nbsp;: il instaure un degré de confiance et
génère du marché.

Nous pourrions passer en revue plusieurs systèmes alternatifs qui
permettraient de s'émanciper plus ou moins du capitalisme de
surveillance. Les solutions ne sont pas uniquement techniques&nbsp;: elles
résident dans le degré de connaissance des enjeux de la part des
populations. Il ne suffit pas de ne plus utiliser les services de
courriel de Google, surtout si on apprécie l'efficacité de ce service.
Il faut se poser la question&nbsp;: «&nbsp;si j'ai le choix d'utiliser ou non
Gmail, dois-je pour autant imposer à mon correspondant qu'il entre en
relation avec Google en me répondant à cette adresse&nbsp;?&nbsp;». C'est
exactement le même questionnement qui s'impose lorsque j'envoie un
document en format Microsoft en exigeant indirectement de mon
correspondant qu'il ait lui aussi le même logiciel pour l'ouvrir.

L'enjeu est éthique. Dans la [Règle d'Or](https://fr.wikipedia.org/wiki/\%C3\%89thique_de_r\%C3\%A9ciprocit\%C3\%A9), c'est la réciprocité qui est importante («&nbsp;ne fais pas à autrui ce que tu n'aimerais pas qu'on te fasse&nbsp;»). Appliquer cette règle à nos rapports technologiques permet une prise de conscience, l'évaluation des enjeux qui dépasse le seul rapport individuel, nucléarisé, que j'ai avec mes pratiques numériques et que le capitalisme de surveillance cherche à m'imposer. Si je choisis d'installer sur mon smartphone l'application de géolocalisation que m'offre mon assureur en guise de test contre un avantage quelconque, il faut que je prenne conscience que je participe
directement à une mutation sociale qui imposera des comportements pour
faire encore davantage de profits. C'est la même logique à l'œuvre avec
l'ensemble des solutions gratuites que nous offrent les GAFAM, à
commencer par le courrier électronique, le stockage en ligne, la
cartographie et la géolocalisation.

Faut-il se passer de toutes ces innovations&nbsp;? Bien sûr que non&nbsp;! le
retranchement anti-technologique n'est jamais une solution. D'autant
plus qu'il ne s'agit pas non plus de dénigrer les grands bienfaits
d'Internet. Par contre, tant que subsisteront dans l'ADN d'Internet les
concepts d'ouverture et de partage, il sera toujours possible de
proposer une alternative au capitalisme de surveillance. Comme le phare
console le marin dans la brume, le logiciel libre et les modèles
collaboratifs qu'il véhicule représentent l'avenir de nos libertés. Nous
ne sommes plus libres si nos comportements sont imposés. Nous ne sommes
pas libres non plus dans l'ignorance technologique.

Il est donc plus que temps, à l'instar de Framasoft, de[Dégoogliser Internet](http://degooglisons-internet.org/) en
proposant non seulement d'autres services alternatifs et respectueux des
utilisateurs, mais surtout les moyens de multiplier les solutions et
décentraliser les usages. Car ce qu'il y a de commun entre le
capitalisme classique et le capitalisme de surveillance, c'est le besoin
centralisateur, qu'il s'agisse des finances ou des données. Ah&nbsp;! si tous
les CHATONS du monde pouvaient se donner la patte&hellip;

[^chatons]: CHATONS est le Collectif des Hébergeurs Alternatifs,Transparents, Ouverts, Neutres et Solidaires. Il rassemble des structures souhaitant éviter la collecte et la centralisation des données personnelles au sein de silos numériques du type de ceux proposés par les GAFAM (Google, Apple, Facebook, Amazon, Microsoft). Voir le site chatons.org.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


# Surveillance et confiance

Ce second chapitre vise à approfondir les concepts survolés
	précédemment. Nous avons vu que les monopoles de l'économie numérique
	ont changé le paradigme libéral jusqu'à instaurer un *capitalisme de surveillance*. Pour en rendre compte et comprendre ce nouveau
	système, il faut brosser plusieurs aspects de nos rapports avec la
	technologie en général et les services numériques en particulier, puis
	voir comment des modèles s'imposent par l'usage des big data et la
	conformisation du marché. J'expliquerai, à partir de la lecture de S.
	Zuboff, quelles sont les principales caractéristiques du capitalisme de
	surveillance.

En identifiant ainsi ces nouvelles conditions de
	l'aliénation des individus, il devient évident que le rétablissement de
	la confiance aux sources de nos relations contractuelles et
	démocratiques, passe immanquablement par l'autonomie, le partage et la
	coopération, sur le modèle du logiciel libre.

## Techniques d'anticipation

On pense habituellement les objets techniques et les phénomènes sociaux
comme deux choses différentes, voire parfois opposées. En plus de la
nature, il y aurait deux autres réalités&nbsp;: d'un côté un monde constitué
de nos individualités, nos rapports sociaux, nos idées politiques, la
manière dont nous structurons ensemble la société&nbsp;; et de l'autre côté
le monde des techniques qui, pour être appréhendé, réclamerait
obligatoirement des compétences particulières (artisanat, ingénierie).
Nous refusons même parfois de comprendre nos interactions avec les
techniques autrement que par automatisme, comme si notre accomplissement
social passait uniquement par le statut d'utilisateur et jamais comme
producteurs. Comme si une mise à distance devait obligatoirement
s'établir entre ce que nous sommes (des êtres sociaux) et ce que nous
produisons. Cette posture est au plus haut point paradoxale puisque non
seulement la technologie est une activité sociale mais nos sociétés sont
à bien des égards elles-mêmes configurées, transformées, et même
régulées par les objets techniques. Certes, la compréhension des
techniques n'est pas toujours obligatoire pour comprendre les faits
sociaux, mais il faut avouer que bien peu de faits sociaux se
comprennent sans la technique.

L'avènement des sociétés industrielles a marqué un pas supplémentaire
dans l'interdépendance entre le social et les objets techniques&nbsp;: le
travail, comme activité productrice, est en soi un rapport réflexif sur
les pratiques et les savoirs qui forment des systèmes techniques. Nous
baignons dans une *culture technologique*, nous sommes des êtres
technologiques. Nous ne sommes pas que cela. La technologie n'a pas le
monopole du signifiant&nbsp;: nous trouvons dans l'art, dans nos codes
sociaux, dans nos intentions, tout un ensemble d'éléments culturels dont
la technologie est censée être absente. Nous en usons parfois même pour
effectuer une mise à distance des techniques omniprésentes. Mais ces
dernières ne se réduisent pas à des objets produits. Elles sont à la
fois des pratiques, des savoir-faire et des objets. La raison pour
laquelle nous tenons tant à instaurer une mise à distance par rapport
aux technologies que nous utilisons, c'est parce qu'elles s'intègrent
tellement dans notre système économique qu'elle agissent aussi comme une
forme d'aliénation au travail comme au quotidien.

Ces dernières années, nous avons vu émerger des technologies qui
prétendent prédire nos comportements. Amazon teste à grande échelle des
dépôts locaux de produits en prévision des commandes passées par les
internautes sur un territoire donné. Les stocks de ces dépôts seront
gérés au plus juste par l'exploitation des données inférées à partir de
l'analyse de l'attention des utilisateurs (mesurée au clic), les
tendances des recherches effectuées sur le site, l'impact des promotions
et propositions d'Amazon, etc. De son côté, Google ne cesse d'analyser
nos recherches dans son moteur d'indexation, dont les suggestions
anticipent nos besoins souvent de manière troublante. En somme, si nous
éprouvons parfois cette nécessité d'une mise à distance entre notre être
et la technologie, c'est en réponse à cette situation où, intuitivement,
nous sentons bien que la technologie n'a pas à s'immiscer aussi
profondément dans l'intimité de nos pensées. Mais est-ce vraiment le
cas&nbsp;? Ne conformons-nous pas aussi, dans une certaine mesure, nos envies
et nos besoins en fonction de ce qui nous est présenté comme nos envies
et nos besoins&nbsp;? La lutte permanente entre notre volonté d'indépendance
et notre adaptation à la société de consommation, c'est-à-dire au
marché, est quelque chose qui a parfaitement été compris par les grandes
firmes du secteur numérique. Parce que nous laissons tellement de traces
transformées en autant d'informations extraites, analysées, quantifiées,
ces firmes sont désormais capables d'anticiper et de conformer le marché
à leurs besoins de rentabilité comme jamais une firme n'a pu le faire
dans l'histoire des sociétés capitalistes modernes.

## Productivisme et information

Si vous venez de perdre deux minutes à lire ma prose ci-dessus hautement
dopée à l'EPO (Enfonçage de Portes Ouvertes), c'est parce qu'il m'a tout
de même paru assez important de faire quelques rappels de base avant
d'entamer des considérations qui nous mènerons petit à petit vers une
compréhension en détail de ce système. J'ai toujours l'image de cette
enseignante de faculté, refusant de lire un mode d'emploi pour appuyer
deux touches sur son clavier afin de basculer l'affichage écran vers
l'affichage du diaporama, et qui s'exclamait&nbsp;: «&nbsp;il faut appeler un
informaticien, ils sont là pour ça, non&nbsp;?&nbsp;». Ben non, ma grande, un
informaticien a autre chose à faire. C'est une illustration très
courante de ce que la transmission des savoirs (activité sociale) dépend
éminemment des techniques (un video-projecteur couplé un ordinateur
contenant lui-même des supports de cours sous forme de fichiers) et que
toute tentative volontaire ou non de mise à distance (ne pas vouloir
acquérir un savoir-faire technique) cause un bouleversement a-social (ne
plus être en mesure d'assurer une transmission de connaissance, ou plus
prosaïquement passer pour quelqu'un de stupide). La place que nous
occupons dans la société n'est pas uniquement une affaire de codes ou
relations interpersonnelles, elle dépend pour beaucoup de l'ordre
économique et technique auquel nous devons plus ou moins nous ajuster au
risque de l'exclusion (et la forme la plus radicale d'exclusion est
l'absence de travail et/ou l'incapacité de consommer).

Des études déjà anciennes sur les organisations du travail, notamment à
propos de l'industrialisation de masse, ont montré comment la
technologie, au même titre que l'organisation, détermine les
comportements des salariés, en particulier lorsqu'elle conditionne
l'automatisation des tâches[@Liu1981]. Souvent,
cette automatisation a été étudiée sous l'angle des répercussions
socio-psychologiques, accroissant les sentiments de perte de sens ou
d'aliénation sociale[@Blauner1965]. D'un point de vue plus
populaire, c'est le remplacement de l'homme par la machine qui fut
tantôt vécu comme une tragédie (l'apparition du chômage de masse) tantôt
comme un bienfait (diminuer la pénibilité du travail). Aujourd'hui, les
enjeux sont très différents. On s'interroge moins sur la place de la
machine dans l'industrie que sur les taux de productivité et la gestion
des flux. C'est qu'un changement de paradigme s'est accompli à partir
des années 1970-1980, qui ont vu apparaître l'informatisation quasi
totale des systèmes de production et la multiplication des ordinateurs
*mainframe* dans les entreprises pour traiter des quantités sans
cesse croissantes de données.

Très peu de sociologues ou d'économistes ont travaillé sur ce qu'une
illustre chercheuse de la Harvard Business School, [Shoshana Zuboff](https://en.wikipedia.org/wiki/Shoshana_Zuboff), a
identifié dans la transformation des systèmes de production&nbsp;:
l'informationnalisation (*informating*). En effet, dans un livre
très visionnaire, *In the Age Of The Smart Machine* en
1988[@zuboff1988],
S. Zuboff montre que les mécanismes du capitalisme productiviste du <span style="font-variant:small-caps;">xx</span>^e^ siècle ont connu plusieurs mouvements, plus ou moins concommitants selon les secteurs&nbsp;: la mécanisation, la rationalisation des tâches,
l'automatisation des processus et l'informationnalisation. Ce dernier
mouvement découle des précédents&nbsp;: dès l'instant qu'on mesure la
production et qu'on identifie des processus, on les documente et on les
programme pour les automatiser. L'informationnalisation est un processus
qui transforme la mesure et la description des activités en information,
c'est-à-dire en données extractibles et quantifiables, calculatoires et
analytiques.

Là où S. Zuboff s'est montrée visionnaire dans les années 1980, c'est
qu'elle a expliqué aussi comment l'informationnalisation modèle en retour
les apprentissages et déplace les enjeux de pouvoir. On ne cherche plus
à savoir qui a les connaissances suffisantes pour mettre en œuvre telle
procédure, mais qui a accès aux données dont l'analyse déterminera la
stratégie de développement des activités. Alors que le «&nbsp;vieux&nbsp;»
capitalisme organisait une concurrence entre moyens de production et
maîtrise de l'offre, ce qui, dans une économie mondialisée n'a plus
vraiment de sens, un nouveau capitalisme (nommé *capitalisme de surveillance*) est né, et repose sur la production d'informations, la maîtrise des données et donc des processus.

Pour illustrer, il suffit de penser à nos smartphones. Quelle que soit
la marque, ils sont produits de manière plus ou moins redondante,
parfois dans les mêmes chaînes de production (au moins en ce qui
concerne les composants), avec des méthodes d'automatisation très
similaires. Dès lors, la concurrence se place à un tout autre niveau&nbsp;:
l'optimisation des procédures et l'identification des usages, tous deux
producteurs de données. Si bien que la plus-value ajoutée par la firme à
son smartphone pour séduire le plus d'utilisateurs possible va de pair
avec une optimisation des coûts de production et des procédures. Cette
optimisation relègue l'innovation organisationnelle au moins au même
niveau, si ce n'est plus, que l'innovation du produit lui-même qui ne
fait que répondre à des besoins d'usage de manière marginale. En matière
de smartphone, ce sont les utilisateurs les plus experts qui sauront
déceler la pertinence de telle ou telle nouvelle fonctionnalité alors
que l'utilisateur moyen n'y voit que des produits similaires.

L'enjeu dépasse la seule optimisation des chaînes de production et
l'innovation. S. Zuboff a aussi montré que l'analyse des données plus
fines sur les processus de production révèle aussi les comportements des
travailleurs&nbsp;: l'attention, les pratiques quotidiennes, les risques
d'erreur, la gestion du temps de travail, etc. Dès lors
l'informationnalisation est aussi un moyen de rassembler des données sur
les aspects sociaux de la production[^guiseillus], et par conséquent les conformer aux impératifs de rentabilité. L'exemple le plus frappant aujourd'hui de cet ajustement comportemental à la rentabilité par les moyens de l'analyse
de données est le phénomène d'*Ubérisation* de l'économie[^uberisa].


[^guiseillus]: En guise d'illustration de tests (*proof of concepts*) discrets menés dans les entreprises aujourd'hui, on peut se reporter à cet article de *Rue 89 Strasbourg* sur cette société alsacienne qui propose, sous couvert de
  *challenge* collectif, de monitorer l'activité physique de ses
  employés[@Boulle2016].

[^uberisa]: Par exemple, dans le cas des livreurs à vélo de plats préparés utilisés par des sociétés comme Foodora ou Take Eat Easy, on constate que ces sociétés ne sont qu'une interface entre les restaurants et les livreurs. Comme à l'âge du tâcheronnage du <span style="font-variant:small-caps;">xix</span>^e^ siècle, ces derniers sont auto-entrepreneurs, payés à la course et censés assumer toutes les cotisations sociales et leurs salaires uniquement sur la base des courses que l'entreprise principale pourra leur confier. Ce rôle est automatisé par une application de calcul qui rassemble les données géographiques et les performances (moyennes, vitesse, assiduité, etc.) des livreurs et distribue automatiquement les tâches de livraisons. Charge aux livreurs de se trouver au bon endroit au bon moment. Ce modèle économique permet de détacher l'entreprise de toutes les externalités ou tâches habituellement intégrées qui pourraient influer sur son chiffre d'affaires&nbsp;: le matériel du livreur, l'assurance, les cotisations, l'accidentologie, la gestion du temps de travail, etc., seule demeure l'activité elle-même automatisée et le livreur devient un exécutant dont on analyse les performances. L'*Ubérisation* est un processus qui automatise le travail humain, élimine les contraintes sociales et utilise l'information que l'activité produit pour ajuster la production à la demande (quels que soient les risques pris par les livreurs, par exemple pour remonter les meilleures informations concernant les vitesses de livraison et se voir confier les prochaines missions)[@Anquetil2016 ; @Castelliti2016].




Ramené aux utilisateurs finaux des produits, dans la mesure où il est
possible de rassembler des données sur l'utilisation elle-même, il
devrait donc être possible de conformer les usages et les comportements
de consommation (et non plus seulement de production) aux mêmes
impératifs de rentabilité. Cela passe par exemple par la communication
et l'apprentissage de nouvelles fonctionnalités des objets. Par exemple,
si vous aviez l'habitude de stocker vos fichiers MP3 dans votre
smartphone pour les écouter comme avec un baladeur, votre fournisseur
vous permet aujourd'hui avec une connexion 4G d'écouter de la musique en
streaming illimité, ce qui permet d'analyser vos goûts, vous proposer
des playlists, et faire des bénéfices. Mais dans ce cas, si vous vous
situez dans un endroit où la connexion haut débit est défaillante, voire
absente, vous devez vous passer de musique ou vous habituer à prévoir à
l'avance cette éventualité en activant un mode hors-connexion. Cette
adaptation de votre comportement devient prévisible&nbsp;: l'important n'est
pas de savoir ce que vous écoutez mais comment vous le faites, et ce
paramètre entre lui aussi en ligne de compte dans la proposition
musicale disponible.

Le nouveau paradigme économique du <span style="font-variant:small-caps;">xxi</span>^e^ siècle, c'est le rassemblement des données, leur traitement et leurs valeurs prédictives, qu'il s'agisse des données de production comme des données d'utilisation par les consommateurs.

## Big Data

Ces dernières décennies, l'informatique est devenu un média pour
l'essentiel de nos activités sociales. Vous souhaitez joindre un ami
pour aller boire une bière dans la semaine&nbsp;? c'est avec un ordinateur
que vous l'appelez. Voici quelques étapes grossières de cet épisode&nbsp;:

1. votre mobile a signalé vers des antennes relais pour s'assurer de la
  couverture réseau,
2. vous entrez un mot de passe pour déverrouiller l'écran,
3. vous effectuez une recherche dans votre carnet d'adresse
  (éventuellement synchronisé sur un serveur distant),
4. vous lancez le programme de composition du numéro, puis l'appel
  téléphonique (numérique) proprement dit,
5. vous entrez en relation avec votre correspondant, convenez d'une date,
6. vous envoyez ensuite une notification de rendez-vous avec votre agenda
  vers la messagerie de votre ami&hellip;
7. qui vous renvoie une notification d'acceptation en retour,
8. l'une de vos applications a géolocalisé votre emplacement et vous
  indique le trajet et le temps de déplacement nécessaire pour vous
  rendre au point de rendez-vous, etc.


Durant cet épisode, quel que soit l'opérateur du réseau que vous
utilisez et quel que soit le système d'exploitation de votre mobile (à
une ou deux exceptions près), *vous avez produit des données exploitables*. Par exemple (liste non exhaustive)&nbsp;:

- le bornage de votre mobile indique votre position à l'opérateur, qui
  peut croiser votre activité d'émission-réception avec la nature de
  votre abonnement,
- la géolocalisation donne des indications sur votre vitesse de
  déplacement ou votre propension à utiliser des transports en commun,
- votre messagerie permet de connaître la fréquence de vos contacts, et
  éventuellement l'éloignement géographique.

Ramené à des millions d'utilisateurs, l'ensemble des données ainsi
rassemblées entre dans la catégorie des *Big Data*. Ces dernières
ne concernent pas seulement les systèmes d'informations ou les services
numériques proposés sur Internet. La massification des données est un
processus qui a commencé il y a longtemps, notamment par l'analyse de
productivité dans le cadre de la division du travail, les analyses
statistiques des achats de biens de consommation, l'analyse des
fréquences de transaction boursières, etc. Tout comme ce fut le cas pour
les processus de production qui furent automatisés, ce qui caractérise
les *Big Data* c'est l'obligation d'automatiser leur traitement
pour en faire l'analyse.

Prospection, gestion des risques, prédictibilité, etc., les *Big
Data* dépassent le seul degré de l'analyse statistique dite
*descriptive*, car si une de ces données renferme en général très
peu d'information, des milliers, des millions, des milliards permettent
d'*inférer* des informations dont la pertinence dépend à la fois de
leur traitement et de leur gestion. Le tout dépasse la somme des
parties&nbsp;: c'est parce qu'elles sont rassemblées et analysées en masse
que des données d'une même nature produisent des valeurs qui dépassent
la somme des valeurs unitaires.

Antoinette Rouvroy, dans un rapport récent[@Rouvroy2016] auprès du Conseil de l'Europe, précise que les capacités techniques de stockage et de traitement connaissent une progression exponentielle[^enmemtemps]. Elle définit plusieurs catégories de *data* en fonction de leurs sources, et qui quantifient l'essentiel des actions humaines&nbsp;:

[^enmemtemps]: En même temps qu'une réduction de leur coût de production (leur coût d'exploitation ou d'analyse, en revanche, explose).

- les *hard data*, produites par les institutions et
  administrations publiques&nbsp;;
- les *soft data*, produites par les individus, volontairement (via
  les réseaux sociaux, par exemple) ou involontairement (analyse des
  flux, géolocalisation, etc.)&nbsp;;
- les *métadonnées*, qui concernent notamment la provenance des
  données, leur trafic, les durées, etc.&nbsp;;
- l'*Internet des objets*, qui une fois mis en réseau, produisent
  des données de performance, d'activité, etc.


Les *Big Data* n'ont cependant pas encore de définition
claire[Sur l'origine de l'expression, @Diebold2012]. Il y a
des chances pour que le terme ne dure pas et donne lieu à des divisions
qui décriront plus précisément des applications et des méthodes dès lors
que des communautés de pratiques seront identifiées avec leurs
procédures et leurs réseaux. Aujourd'hui, l'acception générique désigne
avant tout un secteur d'activité qui regroupe un ensemble de
savoir-faire et d'applications très variés. Ainsi, le potentiel
scientifique des *Big Data* est encore largement sous-évalué,
notamment en génétique, physique, mathématiques et même en sociologie.
Il en est de même dans l'industrie. En revanche, si on place sur un même
plan la politique, le marketing et les activités marchandes (à bien des
égards assez proches) on conçoit aisément que l'analyse des données
optimise efficacement les résultats et permet aussi, par leur dimension
prédictible, de conformer les biens et services.

## Google fait peur ?

Eric Schmidt, actuellement directeur exécutif d'Alphabet Inc., savait
exprimer en peu de mots les objectifs de Google lorsqu'il en était le
président. Depuis 1998, les activités de Google n'ont cessé d'évoluer,
jusqu'à reléguer le service qui en fit la célébrité, la recherche sur la
Toile, au rang d'activité *has been* pour les internautes. En 2010,
il dévoilait le secret de Polichinelle[@Jenkins2010]&nbsp;:

> Le jour viendra où la barre de recherche de Google &ndash;&nbsp;et l'activité
qu'on nomme *Googliser*&nbsp;&ndash; ne sera plus au centre de nos vies en
ligne. Alors quoi&nbsp;? Nous essayons d'imaginer ce que sera l'avenir de la
recherche (&hellip;). Nous sommes toujours content d'être dans le
secteur de la recherche, croyez-moi. Mais une idée est que de plus en
plus de recherches sont faites en votre nom sans que vous ayez à taper
sur votre clavier.
> 
> En fait, je pense que la plupart des gens ne veulent pas que Google
réponde à leurs questions. Ils veulent que Google dise ce qu'ils doivent
faire ensuite. (&hellip;) La puissance du ciblage individuel &ndash;&nbsp;la
technologie sera tellement au point qu'il sera très difficile pour les
gens de regarder ou consommer quelque chose qui n'a pas été adapté pour
eux.


Google n'a pas été la première firme à exploiter des données en masse.
Le principe est déjà ancien avec les premiers *data centers* du
milieu des années 1960. Google n'est pas non plus la première firme à
proposer une indexation générale des contenus sur Internet, par contre
Google a su se faire une place de choix parmi la concurrence farouche de
la fin des années 1990 et sa croissance n'a cessé d'augmenter depuis
lors, permettant des investissements conséquents dans le développement
de technologies d'analyse et dans le rachat de brevets et de plus
petites firmes spécialisées. Ce que Google a su faire, et qui explique
son succès, c'est proposer une expérience utilisateur captivante de
manière à rassembler assez de données pour proposer en retour des
informations adaptées aux profils des utilisateurs. C'est la
radicalisation de cette stratégie qui est énoncée ici par Eric Schmidt.
Elle trouve pour l'essentiel son accomplissement en utilisant les
données produites par les utilisateurs du *cloud computing*. Les
*Big Data* en sont les principaux outils.

D'autres firmes ont emboîté le pas. En résultat, la concentration des
dix premières firmes de services numériques génère des bénéfices
spectaculaires et limite la concurrence. Cependant, l'exploitation des
données personnelles des utilisateurs et la manière dont elles sont
utilisées sont souvent mal comprises dans leurs aspects techniques
autant que par les enjeux économiques et politiques qu'elles soulèvent.
La plupart du temps, la quantité et la pertinence des informations
produites par l'utilisateur semblent négligeables à ses yeux, et leur
exploitation d'une importance stratégique mineure. Au mieux, si elles
peuvent encourager la firme à mettre d'autres services gratuits à
disposition du public, le prix est souvent considéré comme largement
acceptable. Tout au plus, il semble *a priori* moins risqué
d'utiliser Gmail et Facebook que d'utiliser son smartphone en pleine
manifestation syndicale en France, puisque le gouvernement français a
voté publiquement des lois liberticides et que les GAFAM ne livrent aux
autorités que les données manifestement suspectes (disent-elles) ou sur
demande rogatoire officielle (disent-elles).

## Gigantismus

En 2015, Google Analytics couvrait plus
de 70% des parts de marché[@Juhan2015] des outils de mesure d'audience. Google
Analytics est un outil d'une puissance pour l'instant incomparable dans
le domaine de l'analyse du comportement des visiteurs. Cette puissance
n'est pas exclusivement due à une supériorité technologique (les firmes
concurrentes utilisent des techniques d'analyse elles aussi très
performantes), elle est surtout dûe à l'exhaustivité monopolistique des
utilisations de Google Analytics, disponible en version gratuite et
premium, avec un haut degré de configuration des variables qui
permettent la collecte, le traitement et la production très rapide de
rapports (pratiquement en temps réel). La stratégie commerciale, afin
d'assurer sa rentabilité, consiste essentiellement à coupler le
profilage avec la régie publicitaire
([Google AdWords](https://fr.wikipedia.org/wiki/AdWords)). Ce
modèle économique est devenu omniprésent sur Internet. Qui veut être lu
ou regardé doit passer l'épreuve de l'analyse «&nbsp;à la Google&nbsp;», mais pas uniquement pour mesurer l'audience&nbsp;: le monopole de Google sur ces
outils impose un [web rédactionnel](https://fr.wikipedia.org/wiki/\%C3\%89criture_web), c'est-à-dire une méthode d'écriture des contenus qui se
prête à l'extraction de données.

Google n'est pas la seule entreprise qui réussi le tour de force
d'adapter les contenus, quelle que soit leur nature et leurs propos, à
sa stratégie commerciale. C'est le cas des GAFAM qui reprennent en chœur
le même modèle économique qui conforme l'information à ses propres
besoins d'information. Que vous écriviez ou non un pamphlet contre
Google, l'extraction et l'analyse des données que ce contenu va générer
indirectement en produisant de l'activité (visites, commentaires,
échanges, temps de connexion, provenance, etc.) permettra de générer une
activité commerciale. *Money is money*, direz-vous. Pourtant, la
concentration des activités commerciales liées à l'exploitation des
*Big Data* par ces entreprises qui forment les premières
capitalisations boursières du monde, pose un certain nombre répercutions
sociales&nbsp;: l'omniprésence mondialisée des firmes, la réduction du marché
et des choix, l'impuissance des États, la sur-financiarisation.

### Omniprésence

En produisant des services gratuits (ou très accessibles), performants
et à haute valeur ajoutée pour les données qu'ils produisent, ces
entreprises captent une gigantesque part des activités numériques des
utilisateurs. Elles deviennent dès lors les principaux fournisseurs de
services avec lesquels les gouvernements doivent composer s'ils veulent
appliquer le droit, en particulier dans le cadre de la surveillance des
populations et des opérations de sécurité. Ainsi, dans une démarche très
pragmatique, des ministères français parmi les plus importants réunirent
le 3 décembre 2015 les «&nbsp;les grands acteurs de l'internet et des réseaux
sociaux&nbsp;» dans le cadre de la stratégie anti-terroriste[@premin2015]. Plus anciennement, le programme américain de surveillance électronique[PRISM](https://fr.wikipedia.org/wiki/PRISM_\%28programme_de_surveillance\%29),
dont bien des aspects furent révélés par Edward Snowden en 2013, a
permis de passer durant des années des accords entre la NSA et
l'ensemble des GAFAM. Outre les questions liées à la sauvegarde du droit
à la vie privée et à la liberté d'expression, on constate aisément que
la surveillance des populations par les gouvernements trouve nulle part
ailleurs l'opportunité de récupérer des données aussi exhaustives,
faisant des GAFAM les principaux prestataires de *Big Data* des
gouvernements, quels que soient les pays[^Memesiapple].

[^Memesiapple]: Même si Apple a déclaré ne pas entrer dans le secteur des Big Data, il n'en demeure pas moins qu'il en soit un consommateur comme un producteur d'envergure mondiale.

### Réduction du marché

Le monopole de Google sur l'indexation du web montre qu'en vertu du
gigantesque chiffre d'affaires que cette activité génère, celle-ci
devient la première et principale interface du public avec les contenus
numériques. En 2000, le journaliste Laurent Mauriac signait dans
*Libération* un article élogieux sur Google[@Mauriac2000]. À propos de
la méthode d'indexation qui en fit la célébrité, L. Mauriac écrit&nbsp;:

> Autre avantage&nbsp;: ce système empêche les sites de tricher. Inutile de
farcir la partie du code de la page invisible pour l'utilisateur avec
quantité de mots-clés&hellip;


Or, en 2016, il devient assez évident de décortiquer les stratégies de
manipulation de contenus que les compagnies qui en ont les moyens
financiers mettent en œuvre pour dominer l'index de Google en diminuant
ainsi la diversité de l'offre[@Abry2016]. Que la concentration des entreprises limite mécaniquement
l'offre, cela n'est pas révolutionnaire. Ce qui est particulièrement
alarmant, en revanche, c'est que la concentration des GAFAM implique par
effets de bord la concentration d'autres entreprises de secteurs
entièrement différents mais qui dépendent d'elles en matière
d'information et de visibilité. Cette concentration de second niveau,
lorsqu'elle concerne la presse en ligne, les éditeurs scientifiques ou
encore les libraires, pose à l'évidence de graves questions en matière
de liberté d'information.

### Impuissances

La concentration des services, qu'il s'agisse de ceux de Google comme
des autres géants du web, sur des secteurs biens découpés qui assurent
les monopoles de chaque acteur, cause bien des soucis aux États qui
voient leurs économies impactées selon les revenus fiscaux et
l'implantation géographique de ces compagnies. Les lois anti-trust
semblent ne plus suffire lorsqu'on voit, par exemple, la Commission
Européenne avoir toutes les peines du monde à freiner l'abus de position
dominante de Google sur le système Android[@CommEU2016], sur Google
Shopping[@CommEU2015], sur l'indexation[@CommEU2014], et de manière
générale depuis 2010[@CommEU2010].

Illustration cynique devant l'impuissance des États à réguler cette
concentration, Google a davantage à craindre de ses concurrents directs
qui ont des moyens financiers largement supérieurs aux États pour
bloquer sa progression sur les marchés. Ainsi, en 2016 cet
 accord entre Microsoft et Google[@Powles2016], qui conviennent de régler désormais leurs différends uniquement en privé, selon leurs propres règles, pour ne se concentrer que sur leur concurrence de marché et non plus sur la
législation. Le message est double&nbsp;: en plus d'instaurer leur propre
régulation de marché, Google et Microsoft en organiseront eux-mêmes les
conditions juridiques, reléguant le rôle de l'État au second plan, voire
en l'éliminant purement et simplement de l'équation. C'est l'avènement
des Léviathans.

### Capitaux financiers

La capitalisation boursière des GAFAM a atteint un niveau jamais
envisagé jusqu'à présent dans l'histoire, et ces entreprises figurent
toutes dans le top&nbsp;40 de l'année 2015, une position qui n'a fait que s'accroître jusqu'en  2017[@PwC2015 ; PwC2017]. La valeur cumulée de
GAFAM en termes de capital boursier en 2015 s'élève à 1&nbsp;838 milliards de
dollars. À titre de comparaison, le PIB de la France en 2014 était de
2&nbsp;935 milliards de dollars et celui des Pays Bas 892 milliards de
dollars.

La
[liste des acquisitions de Google](https://fr.wikipedia.org/wiki/Liste_des_acquisitions_de_Google) avant 2016 explique en partie la cumulation de valeurs boursières. Pour une autre partie, la plus importante, l'explication
concerne l'activité principale de Google-Alphabet&nbsp;: la valorisation des *big data* et l'automatisation des tâches d'analyse de données qui réduisent
les coûts de production (à commencer par les frais d'infrastructure&nbsp;:
Google a besoin de gigantesques *data centers* pour le stockage,
mais l'analyse, elle, nécessite plus de virtualisation que de nouveaux
matériels ou d'employés). Bien que les cheminements ne soient pas les
mêmes, les entreprises GAFAM ont abouti au même modèle économique qui
débouche sur le *status quo* de monopoles sectorisés. Un élément de
comparaison est donné par le *Financial Times* en août 2014[@Manyika2014 ; @zuboff_big_2015], à propos de l'industrie automobile *vs* l'économie numérique&nbsp;:

> Comparons Detroit en 1990 et la Silicon Valley en 2014. Les 3 plus
importantes compagnies de Detroit produisaient des bénéfices à hauteur
de 250 milliards de dollars avec 1,2 million d'employés et la cumulation
de leurs capitalisations boursières totalisait 36 milliards de dollars.
Le 3 premières compagnies de la Silicon Valley en 2014 totalisaient 247
milliards de dollars de bénéfices, avec seulement 137&nbsp;000 employés, mais
un capital boursier de 1,09 mille milliards de dollars.


La captation d'autant de valeurs par un petit nombre d'entreprises et
d'employés, aurait été analysée, par exemple, par Schumpeter comme une
chute inévitable vers la stagnation en raison des ententes entre les
plus puissants acteurs, la baisse des marges des opérateurs et le manque
d'innovation. Pourtant cette approche est dépassée&nbsp;: l'innovation à
d'autres buts (produire des services, même à perte, pour capter des
données), il est très difficile d'identifier le secteur d'activité
concerné (Alphabet regroupe des entreprises ultra-diversifiées) et donc
le marché est ultra-malléable autant qu'il ne représente que des données
à forte valeur prédictive et possède donc une logique de plus en plus
maîtrisée par les grands acteurs.

## Trouver les bonnes clés

Cette logique du marché, doit être analysée avec d'autres outils que
ceux de l'économie classique. L'impact de l'informationnalisation et la
concentration des acteurs de l'économie numérique ont sans doute été
largement sous-estimés, non pas pour les risques qu'ils font courir aux
économies post-industrielles, mais pour les bouleversements sociaux
qu'ils impliquent. Le besoin de défendre les droits et libertés sur
Internet et ailleurs n'est qu'un effet collatéral d'une situation contre
laquelle il est difficile d'opposer seulement des postures et des
principes.

Il faut entendre les mots de Marc Rotenberg, président de l'Electronic
Privacy Information Center (EPIC), pour qui le débat dépasse désormais
la seule question de la neutralité du réseau ou de la liberté
d'expression[@Rotenberg2016].Pour lui, nous avons besoin d'analyser ce qui structure la concentration du marché. Mais le phénomène de concentration dans tous les services de communication que nous utilisons implique des échelles tellement grandes, que nous avons besoin d'instruments de mesure au moins aussi
grands. Nous avons besoin d'une nouvelle science pour comprendre
Internet, le numérique et tout ce qui découle des formes
d'automatisation.

On s'arrête bien souvent sur l'aspect le plus spectaculaire de la fuite
d'environ [1,7 millions de documents](https://fr.wikipedia.org/wiki/R\%C3\%A9v\%C3\%A9lations_d\%27Edward_Snowden) grâce à Edward Snowden en 2013, qui montrent que
les États, à commencer par les États-Unis, ont créé des systèmes de
surveillance de leurs populations au détriment du respect de la vie
privée. Pour beaucoup de personnes, cette atteinte doit être ramenée à
la dimension individuelle&nbsp;: dois-je avoir quelque chose à cacher&nbsp;?
comment échapper à cette surveillance&nbsp;? implique-t-elle un contrôle des
populations&nbsp;? un contrôle de l'expression&nbsp;? en quoi ma liberté est-elle
en danger&nbsp;? etc. Mais peu de personnes se sont réellement interrogées
sur le fait que si la NSA s'est fait livrer ces quantités gigantesques
d'informations par des fournisseurs de services (comme par exemple, la
totalité des données téléphoniques de Verizon ou les données d'échanges
de courriels du service Hotmail de Microsoft) c'est parce que ces
entreprises avaient effectivement les moyens de les fournir et que par
conséquent de telles quantités d'informations sur les populations sont
tout à fait exploitables et interprétables en premier lieu par ces mêmes
fournisseurs.

Face à cela, plusieurs attitudes sont possibles. En les caricaturant,
elles peuvent être&nbsp;:



   - Positivistes. On peut toujours s'extasier devant les innovations de Google surtout parce que la [culture de l'innovation](https://apps.google.com/learn-more/creating_a_culture_of_innovation.html) à la Google est une composante du [story telling organisé](https://www.thinkwithgoogle.com/intl/fr-fr/article/le-storytelling-creatif-sappuie-sur-des-idees-et-tendances-pour-toucher-les-consommateurs-presses/) pour capter l'attention des consommateurs. La communication est non seulement exemplaire mais elle constitue un modèle d'après Google qui en donne même les recettes (cf. les deux liens précédents). Que Google, Apple, Microsoft, Facebook ou Amazon possèdent des données me concernant, cela ne serait qu'un détail car nous avons tous l'opportunité de participer à cette grande aventure du numérique (hum&nbsp;!).
   - Défaitistes. On n'y peut rien, il faut donc se contenter de protéger sa vie privée par des moyens plus ou moins dérisoires, comme par exemple faire du bruit pour brouiller les pistes[@Richard2016] et faire des concessions car c'est le prix à payer pour profiter des applications bien utiles. Cette attitude consiste à échanger l'utilité contre l'information, ce que nous faisons à chaque fois que nous validons les clauses d'utilisation des services GAFAM.
   - Complotistes. Les procédés de captation des données servent ceux qui veulent nous espionner. Les utilisations commerciales seraient finalement secondaires car c'est contre l'emploi des données par les États qu'il faut lutter. C'est la logique du moins d'État contre la liberté du nouveau monde numérique, comme si le choix consistait à donner notre confiance soit aux États soit aux GAFAM, liberté par contrat social ou liberté par consommation.


Les objectifs de cette accumulation de données sont effectivement
différents que ceux poursuivis par la NSA et ses institutions
homologues. La concentration des entreprises crée le besoin crucial
d'organiser les monopoles pour conserver un marché stable. Les ententes
entre entreprises, repoussant toujours davantage les limites juridiques
des autorités de régulations, créent une logique qui ne consiste plus à
s'adapter à un marché aléatoire, mais adapter le marché à l'offre en
l'analysant en temps réel, en utilisant les données quotidiennes des
utilisateurs de services. Il faut donc analyser ce *capitalisme de
surveillance*, ainsi que le nomme Shoshana Zuboff, car comme nous
l'avons vu&nbsp;:

- les États ont de moins en moins les capacités de réguler ces
  activités,
- nous sommes devenus des produits de variables qui configurent nos
  comportements et nos besoins.


L'ancienne conception libérale du marché, l'acception économique du
contrat social, reposait sur l'idée que la démocratie se consolide par
l'égalitarisme des acteurs et l'équilibre du marché. Que l'on adhère ou
pas à ce point de vue, il reste qu'aujourd'hui le marché ne s'équilibre
plus par des mécanismes libéraux mais par la seule volonté de quelques
entreprises. Cela pose inévitablement une question démocratique.

Dans son rapport, Antoinette Rouvroy en vient à adresser une série de
questions sur les répercutions de cette morphologie du marché. Sans y
apporter franchement de réponse, elle en souligne les enjeux[@Rouvroy2016]&nbsp;:

> Ces dispositifs d'«&nbsp;anticipation performative&nbsp;» des intentions d'achat (qui est aussi un court-circuitage du processus de transformation de la
pulsion en désir ou en intention énonçable), d'optimisation de la force
de travail fondée sur la détection anticipative des performances futures
sur base d'indicateurs produits automatiquement au départ d'analyses de
type Big Data (qui signifie aussi une chute vertigineuse du «&nbsp;cours de
l'expérience&nbsp;» et des mérites individuels sur le marché de l'emploi),
posent des questions innombrables. L'anticipation performative des
intentions &ndash;et les nouvelles possibilités d'actions préemptives fondées
sur la détection des intentions&nbsp;&ndash; est-elle compatible avec la poursuite de l'autodétermination des personnes&nbsp;? Ne faut-il pas concevoir que la possibilité d'énoncer par soi-même et pour soi-même ses intentions et
motivations constitue un élément essentiel de l'autodétermination des
personnes, contre cette utopie, ou cette dystopie, d'une société
dispensée de l'épreuve d'un monde hors calcul, d'un monde où les
décisions soient autre chose que l'application scrupuleuse de
recommandations automatiques, d'un monde où les décisions portent encore
la marque d'un engagement subjectif&nbsp;? Comment déterminer la loyauté de
ces pratiques&nbsp;? L'optimisation de la force de travail fondée sur le
profilage numérique est-il compatible avec le principe d'égalité et de
non-discrimination&nbsp;?


Ce qui est questionné par A. Rouvroy, c'est ce que Zuboff nomme le
*capitalisme de surveillance*, du moins une partie des pratiques
qui lui sont inhérentes. Dès lors, au lieu de continuer à se poser des
questions, il est important d'intégrer l'idée que si le paradigme a
changé, les clés de lecture sont forcément nouvelles.

## Déconstruire le capitalisme de surveillance

Shoshana Zuboff a su identifier les pratiques qui déconstruisent
l'équilibre originel et créent ce nouveau capitalisme. Elle pose ainsi
les jalons de cette nouvelle «&nbsp;science&nbsp;» qu'appelle de ses vœux Marc
Rotenberg, et donne des clés de lecture pertinentes. L'essentiel des
concepts qu'elle propose se trouve dans son article «&nbsp;Big other:
surveillance capitalism and the prospects of an information
civilization&nbsp;», paru en 2015[@zuboff_big_2015]. Dans cet article S. Zuboff décortique deux
discours de Hal Varian, chef économiste de Google et s'en sert de trame
pour identifier et analyser les processus du capitalisme de
surveillance. Dans ce qui suit, je vais essayer d'expliquer quelques
clés issues du travail de S. Zuboff.

### Sur quelle stratégie repose le capitalisme de surveillance&nbsp;?

Il s'agit de la stratégie de commercialisation de la
*quotidienneté* en modélisant cette dernière en autant de données
analysées, inférences et prédictions. La logique d'accumulation,
l'automatisation et la dérégulation de l'extraction et du traitement de
ces données rendent floues les frontières entre vie privée et
consommation, entre les firmes et le public, entre l'intention et
l'information.

### Qu'est-ce que la quotidienneté ?

La quotidienneté est un terme qui cherche son origine dans une conception moderne du style de vie : avec l'apparition du capitalisme et de l'économie de la consommation notre quotidien contient de manière plus ou moins importante une part d'habitudes, de répétitions, de ce que Henri Lefebvre[@lefebvre1961] appelle l'«&nbsp;écrasante banalité&nbsp;», celle du quotidien moderne dans la sphère privée comme dans le travail. L'informationnalisation de nos profils individuels provient de ces sources, transformant notre quotidien en autant de procédures que l'on peut alors profiler.

Il peut s'agir des informations conventionnelles (au sens propre) telles que nos données d'identité, nos données bancaires, etc. Elles peuvent résulter de croisements de flux de données sur nos comportements et donner
lieu à des inférences (les *big datas* en réseau). Elles peuvent aussi provenir des objets connectés, agissant comme des *sensors* ou encore résulter des
activités de surveillance plus ou moins connues. Si toutes ces données
permettent le profilage c'est aussi par elles que nous déterminons notre
présence numérique. Avoir une connexion Internet est aujourd'hui reconnu
comme un droit&nbsp;: nous ne distinguons plus nos activités d'expression sur
Internet des autres activités d'expression. Les activités numériques
n'ont plus à être considérées comme des activités virtuelles, distinctes
de la réalité. Nos pages Facebook ne sont pas plus virtuelles que notre
compte Amazon ou notre dernière déclaration d'impôt en ligne. Si bien
que cette activité en réseau est devenue si quotidienne et si banale qu'elle génère elle-même de nouveaux comportements et besoins, produisant de nouvelles
données analysées, agrégées, commercialisées.

### Extraction des données

S. Zuboff dresse un inventaire des types de données, des pratiques
d'extraction, et les enjeux de l'analyse. Il faut cependant comprendre
qu'il y a de multiples manières de «&nbsp;traiter&nbsp;» les données. La Directive 95/46/CE (Commission Européenne) en dressait un inventaire, déjà en
1995[@DirCE1995]&nbsp;:

> «&nbsp;traitement de données à caractère personnel&nbsp;» (traitement)&nbsp;: toute opération ou ensemble d'opérations effectuées ou non à l'aide de
procédés automatisés et appliquées à des données à caractère personnel,
telles que la collecte, l'enregistrement, l'organisation, la
conservation, l'adaptation ou la modification, l'extraction, la
consultation, l'utilisation, la communication par transmission,
diffusion ou toute autre forme de mise à disposition, le rapprochement
ou l'interconnexion, ainsi que le verrouillage, l'effacement ou la
destruction.


Le processus d'extraction de données n'est pas un processus autonome, il
ne relève pas de l'initiative isolée d'une ou quelques firmes sur un
marché de niche. Il constitue désormais la norme commune à une multitude
de firmes, y compris celles qui n'œuvrent pas dans le secteur numérique.
Il s'agit d'un modèle de développement qui remplace l'accumulation de
capital. Autant cette dernière était, dans un modèle économique révolu,
un facteur de production (par les investissements, par exemple), autant
l'accumulation de données est désormais la clé de production de capital,
lui-même réinvesti en partie dans des solutions d'extraction et
d'analyse de données, et non dans des moyens de production, et pour une
autre partie dans la valorisation boursière, par exemple pour le rachat
d'autres sociétés, à l'image des multiples rachats de Google.

### Neutralité et indifférence

Les données qu'exploite Google sont très diverses. On s'interroge
généralement sur celles qui concernent les catégories auxquelles nous
accordons de l'importance, comme par exemple lorsque nous effectuons une
recherche au sujet d'une mauvaise grippe et que Google Now nous propose
une consultation médicale à distance. Mais il y a une multitude de
données dont nous soupçonnons à peine l'existence, ou apparemment
insignifiantes pour soi, mais qui, ramenées à des millions, prennent un
sens tout à fait probant pour qui veut les exploiter. On peut citer par
exemple le temps de consultation d'un site. Si Google est prêt à
signaler des contenus à l'administration des États-Unis, comme
l'a dénoncé Julian Assange[@Assange2014chap] ou si les relations publiques de la firme montrent une
certaine proximité avec les autorités de différents pays et l'impliquent
dans des programmes d'espionnage, il reste que cette attitude coopérante
relève essentiellement d'une stratégie économique. En réalité,
l'accumulation et l'exploitation des données suppose une
*indifférence formelle* par rapport à leur signification pour
l'utilisateur. Quelle que soit l'importance que l'on accorde à
l'information, le postulat de départ du capitalisme de surveillance,
c'est l'accumulation de quantités de données, non leur qualité. Quant au
sens, c'est le marché publicitaire qui le produit.

### L'extraction et la confiance

S. Zuboff dresse aussi un court inventaire des pratiques d'extraction de
Google. L'exemple emblématique est Google Street View, qui rencontra de
multiples obstacles à travers le monde&nbsp;: la technique consiste à
s'immiscer sur des territoires et capturer autant de données possible
jusqu'à rencontrer de la résistance qu'elle soit juridique, politique ou
médiatique. Il en fut de même pour l'analyse du courrier électronique
sur Gmail, la capture des communications vocales, les paramètres de
confidentialité, l'agrégation des données des comptes sur des services
comme Google+, la conservation des données de recherche, la
géolocalisation des smartphones, la reconnaissance faciale, etc.

En somme, le processus d'extraction est un processus à sens unique, qui
ne tient aucun compte (ou le moins possible) de l'avis de l'utilisateur.
C'est l'absence de réciprocité.

On peut ajouter à cela la puissance des Big Data dans l'automatisation
des relations entre le consommateur et le fournisseur de service. Google
Now propose de se subsituer à une grande partie de la confidentialité
entre médecin et patient en décelant au moins les causes subjectives de
la consultation (en interprétant les recherches). Hal Varian lui-même
vante les mérites de l'automatisation de la relation entre assureurs et
assurés dans un monde où l'on peut couper à distance le démarrage de la
voiture si le client n'a pas payé ses traites. Des assurances santé et
même des mutuelles (!) proposent aujourd'hui des applications
d'auto-mesure[^quantifiedself] de l'activité physique, dont on sait pertinemment que cela résultera sur autant d'ajustements des prix voire des refus d'assurance pour les moins chanceux.

[^quantifiedself]: «&nbsp;Mesure de soi ou personal analytics est un mouvement qui regroupe les outils, les principes et les méthodes permettant à chacun de mesurer ses données personnelles, de les analyser et de les partager&nbsp;» (d'après Wikipédia, notice «&nbsp;quantified self&nbsp;») de l'activité physique, dont on sait pertinemment que cela résultera sur autant d'ajustements des prix voire des refus d'assurance pour les moins chanceux.

La relation contractuelle est ainsi automatisée, ce qui élimine d'autant
le risque lié aux obligations des parties. En d'autres termes, pour
éviter qu'une partie ne respecte pas ses obligations, on se passe de la
confiance que le contrat est censé formaliser, au profit de
l'automatisation de la sanction. Le contrat se résume alors à
l'acceptation de cette automatisation. Si l'arbitrage est automatique,
plus besoin de recours juridique. Plus besoin de la confiance dont la
justice est censée être la mesure.

Entre l'absence de réciprocité et la remise en cause de la relation de
confiance dans le contrat passé avec l'utilisateur, ce sont les bases
démocratiques de nos rapports sociaux qui sont remises en question
puisque le fondement de la loi et de la justice, c'est le contrat et la
formalisation de la *confiance*.

### Qu'implique cette nouvelle forme du marché ?

Plus Google a d'utilisateurs, plus il rassemble des données. Plus il y a
de données plus leur valeur prédictive augmente et optimise le potentiel
lucratif. Pour des firmes comme Google, le marché n'est pas composés
d'agents rationnels qui choisissent entre plusieurs offres et
configurent la concurrence. La société «&nbsp;à la Google&nbsp;» n'est plus fondée
sur des relations de réciprocité, même s'il s'agit de luttes sociales ou
de revendications.

L'uberisation de la société (voir le début de ce chapitre) qui nous
transforme tous en employés ou la transformation des citoyens en purs
consommateurs, c'est exactement le cauchemar d'Hannah Arendt que S.
Zuboff cite longuement[@Arendt1958, p. 400-401]&nbsp;:

> Le dernier stade de la société de travail, la société d'employés, exige de ses membres un pur fonctionnement automatique, comme si la vie individuelle était réellement submergée par le processus global de la vie de l'espèce, comme si la seule décision encore requise de l'individu était de lâcher, pour ainsi dire, d'abandonner son individualité, sa peine et son inquiétude de vivre encore individuellement senties, et d'acquiescer à un type de comportement, hébété, «&nbsp;tranquillisé&nbsp;» et fonctionnel. Ce qu'il y a de fâcheux dans les théories modernes du comportement, ce n'est pas qu'elles sont fausses, c'est qu'elles peuvent devenir vraies, c'est qu'elles sont, en fait, la meilleure mise en concept possible de certaines tendances évidentes de la société moderne. On peut parfaitement concevoir que l'époque moderne (&hellip;) s'achève dans la passivité la plus inerte, la plus stérile que l'Histoire ait jamais connue.

Devant cette anticipation catastrophiste, l'option choisie ne peut être
l'impuissance et le laisser-faire. Il faut d'abord nommer ce à quoi nous
avons affaire, c'est-à-dire un système (total) face auquel nous devons
nous positionner.

### Qu'est-ce que Big Other et que peut-on faire ?

Il s'agit des mécanismes d'extraction, marchandisation et contrôle. Ils
instaurent une dichotomie entre les individus (utilisateurs) et leur
propre comportement (pour les transformer en données) et simultanément
ils produisent de nouveaux marchés basés justement sur la prédictibilité
de ces comportements[@zuboff_big_2015, p. 81].

> (Big Other) est un régime institutionnel, omniprésent, qui enregistre, modifie, commercialise l'expérience quotidienne, du grille-pain au corps biologique, de la communication à la pensée, de manière à établir de nouveaux chemins vers les bénéfices et les profits. Big other est la puissance souveraine d'un futur proche qui annihile la liberté que l'on gagne avec les règles et les lois.

Avoir pu mettre un nom sur ce nouveau régime dont Google constitue un
peu plus qu'une illustration (presque initiateur) est la grande force de
Shoshana Zuboff. Sans compter plusieurs articles récents de cette
dernière dans la *Frankfurter Allgemeine Zeitung*, il est étonnant
que le rapport d'Antoinette Rouvroy, postérieur d'un an, n'y fasse
aucune mention. Est-ce étonnant&nbsp;? Pas vraiment, en réalité, car dans un
rapport officiellement remis aux autorités européennes, il est important
de préciser que la lutte contre Big Other (ou de n'importe quelle
manière dont on peut l'exprimer) ne peut que passer par la légitimité
sans faille des institutions. Ainsi, A. Rouvroy oppose les deux seules
voies envisageables dans un tel contexte officiel&nbsp;:


> À l'approche «&nbsp;law and economics&nbsp;», qui est aussi celle des partisans d'un «&nbsp;marché&nbsp;» des données personnelles, qui tendrait donc à considérer les données personnelles comme des «&nbsp;biens&nbsp;» commercialisables (&hellip;) s'oppose l'approche qui consiste à aborder les données personnelles plutôt en fonction du pouvoir qu'elles confèrent à ceux qui les contrôlent, et à tenter de prévenir de trop grandes disparités informationnelles et de pouvoir entre les responsables de traitement et les individus. C'est, à l'évidence, cette seconde approche qui prévaut en Europe.


Or, devant Google (ou devant ce *Big Other*) nous avons vu l'impuissance des États à faire valoir un arsenal juridique, à commencer par les lois anti-trust. D'une part, l'arsenal ne saurait être exhaustif&nbsp;: il concernerait quelques aspects seulement de l'exploitation des données, la transparence ou une série d'obligations comme l'expression des finalités ou des consentements. Il ne saurait y avoir de procès d'intention. D'autre part, nous pouvons toujours faire valoir que des compagnies telles Microsoft ont été condamnées par le passé, au moins par les instances européennes, pour abus de position dominante, et que quelques millions d'euros de menace pourraient suffire à tempérer les ardeurs. C'est faire peu de cas d'au moins trois éléments&nbsp;:

   1. l'essentiel des compagnies dont il est question sont nord-américaines, possèdent des antennes dans des paradis fiscaux et sont capables, elles, de menacer financièrement des pays entiers par simples effets de leviers boursiers.
   2. L'arsenal juridique qu'il faudrait déployer avant même de penser à saisir une cour de justice, devrait être rédigé, voté et décrété en admettant que les jeux de lobbies et autres obstacles formels soient entièrement levés. Cela prendrait un minimum de dix ans, si l'on compte les recours envisageables après une première condamnation.
   3. Il faut encore caractériser les *Big Data* dans un contexte où leur nature est non seulement extrêmement diverse mais aussi soumise à des process d'automatisation et calculatoires qui permettent d'en inférer d'autres quantités au moins similaires. Par ailleurs, les sources des données sont sans cesse renouvelées, et leur stockage et analyse connaissent une progression technique exponentielle.

Dès lors, quels sont les moyens de s'échapper de ce *Big Other*&nbsp;? Puisque nous avons changé de paradigme économique, nous devons changer de paradigme de consommation et de production. Grâce à des dispositifs comme la neutralité du réseau, garantie fragile et obtenue de [longue lutte](https://www.laquadrature.net/fr/node/9762), il est possible de produire nous même l'écosystème dans lequel nos données sont maîtrisées et les maillons de la chaîne de confiance sont identifiés.



## Confiance et espaces de confiance

Comment évoluer dans le monde du capitalisme de surveillance&nbsp;? telle est la question que nous pouvons nous poser dans la mesure où visiblement nous ne pouvons plus nous extraire d'une situation de soumission à Big Other. Quelles que soient nos compétences en matière de camouflage et de chiffrement, les sources de données sur nos individualités sont tellement diverses que le résultat de la distanciation avec les technologies du marché serait une dé-socialisation. Puisque les technologies sur le marché sont celles qui permettent la production, l'analyse et l'extraction automatisées de données, l'objectif n'est pas tant de s'en passer que de créer un système où leur pertinence est tellement limitée qu'elle laisse place à un autre principe que l'accumulation de données&nbsp;: l'accumulation de confiance.

Voyons un peu&nbsp;: quelles sont les actuelles échappatoires de ce capitalisme de surveillance&nbsp;? Il se trouve qu'elles impliquent à chaque fois de ne pas céder aux facilités des services proposés pour utiliser des solutions plus complexes mais dont la simplification implique toujours une nouvelle relation de confiance. Illustrons ceci par des exemples relatifs à la messagerie par courrier électronique&nbsp;:

   - Si j'utilise Gmail, c'est parce que j'ai confiance en Google pour respecter la confidentialité de ma correspondance privée (*no comment*&nbsp;!). Ce jugement n'implique pas que moi, puisque mes correspondants auront aussi leurs messages scannés, qu'ils soient utilisateurs ou non de Gmail. La confiance repose exclusivement sur Google et les conditions d'utilisation de ses services.
   - L'utilisation de clés de chiffrement pour correspondre avec mes amis demande un apprentissage de ma part comme de mes correspondants. Certains services, comme [ProtonMail](https://protonmail.com/), proposent de leur faire confiance pour transmettre des données chiffrées tout en simplifiant les procédures avec un *webmail* facile à prendre en main. Les briques *open source* de Protonmail permettent un niveau de transparence acceptable sans que j'en aie toutefois l'expertise (si je ne suis pas moi-même un informaticien sérieux).
   - Si je monte un serveur chez moi, avec un système *libre* et/ou *open source* comme une instance [Cozycloud](https://cozy.io/fr/) sur un [Raspeberry PI](https://www.raspberrypi.org/), je dois avoir confiance dans tous les domaines concernés et sur lesquels je n'ai pas forcément l'expertise ou l'accès physique (la fabrication de l'ordinateur, les contraintes de mon fournisseur d'accès, la sécurité de mon serveur, etc.), c'est-à-dire tous les aspects qui habituellement font l'objet d'une division du travail dans les entreprises qui proposent de tels services, et que j'ai la prétention de rassembler à moi tout seul.

Les espaces de relative autonomie existent donc mais il faut structurer la confiance. Cela peut se faire en deux étapes.


La première étape consiste à définir ce qu'est un tiers de confiance et quels moyens de s'en assurer. Le blog de la FSFE a récemment publié un article qui résume très bien pourquoi la protection des données privées ne peut se contenter de l'utilisation plus ou moins maîtrisée des technologies de chiffrement[@h22016]. La FSFE rappelle que tous les usages des services sur Internet nécessitent un niveau de confiance accordé à un tiers. Quelle que soit son identité&hellip;

>&hellip;vous devez prendre en compte&nbsp;:
> 
>   - la bienveillance&nbsp;: le tiers ne veut pas compromettre votre vie privée et/ou il est lui-même concerné&nbsp;;
>   - la compétence&nbsp;: le tiers est techniquement capable de protéger votre vie privée et d’identifier et de corriger les problèmes&nbsp;;
>   - l’intégrité&nbsp;: le tiers ne peut pas être acheté, corrompu ou infiltré par des services secrets ou d'autres tiers malveillants&nbsp;;

Comment s'assurer de ces trois qualités&nbsp;? Pour les défenseurs et promoteurs du logiciel libre, seules les communautés libristes sont capables de proposer des alternatives en vertu de l'ouverture du code. Sur les quatre libertés qui définissent une [licence libre](https://fr.wikipedia.org/wiki/Licence_libre), deux impliquent que le code puisse être accessible, audité, vérifié.

Est-ce suffisant&nbsp;? non&nbsp;: ouvrir le code ne conditionne pas ces qualités, cela ne fait qu'endosser la responsabilité et l'usage du code (via la licence libre choisie) dont seule l'expertise peut attester de la fiabilité et de la probité.

En complément, la seconde étape consiste donc à définir une chaine de confiance. Elle pourra déterminer à quel niveau je peux m'en remettre à l'expertise d'un tiers, à ses compétences, à sa bienveillance&hellip;. Certaines entreprises semblent l'avoir compris et tentent d'anticiper ce besoin de confiance. Pour reprendre les réflexions du Boston Consulting Group, l'un des plus prestigieux cabinets de conseil en stratégie au monde[@Umhoeffer2014]&nbsp;:


> (&hellip;) dans un domaine aussi sensible que le Big Data, la confiance sera l’élément déterminant pour permettre à l’entreprise d’avoir le plus large accès possible aux  données de ses clients, à condition qu’ils soient convaincus que ces données seront utilisées de façon loyale et contrôlée. Certaines entreprises parviendront à créer ce lien de confiance, d’autres non. Or, les enjeux économiques de ce lien de confiance sont très importants. Les entreprises qui réussiront à le créer pourraient multiplier par cinq ou dix le volume d’informations auxquelles elles sont susceptibles d’avoir accès. Sans la confiance du consommateur, l’essentiel des milliards d’euros de valeur économique et sociale que le Big Data pourrait représenter dans les années à venir risquerait d’être perdu.

En d'autres termes, il existe un marché de la confiance. En tant que tel, soit il est lui aussi soumis aux effets de Big Other, soit il reste encore dépendant de la rationalité des consommateurs. Dans les deux cas, le logiciel libre devra obligatoirement trouver le créneau de persuasion pour faire adhérer le plus grand nombre à ses principes.


Une solution serait de déplacer le curseur de la chaîne de confiance non plus sur les principaux acteurs de services, plus ou moins dotés de bonnes intentions, mais dans la sphère publique. Je ne fais plus confiance à tel service ou telle entreprise parce qu'ils indiquent dans leurs [CGU](https://fr.wikipedia.org/wiki/Conditions_g%C3%A9n%C3%A9rales_d%27utilisation) qu'ils s'engagent à ne pas utiliser mes données personnelles, mais parce que cet engagement est non seulement vérifiable mais aussi accessible&nbsp;: en cas de litige, le droit du pays dans lequel j'habite peut s'appliquer, des instances peuvent se porter parties civiles, une surveillance collective ou policière est possible, etc. La question n'est plus tellement de savoir ce que me garantit le fournisseur mais comment il propose un audit public et expose sa responsabilité vis-à-vis des données des utilisateurs.

S'en remettre à l'État pour des services fiables&nbsp;? Les États ayant démontré leurs impuissances face à des multinationales surfinancées, les tentatives de *cloud souverain* ont elles aussi été des échecs cuisants[@Auffray2015]. Créer des entreprises de toutes pièces sur fonds publics pour proposer une concurrence directe à des monopoles géants, était inévitablement un suicide&nbsp;: il aurait mieux valu proposer des ressources numériques et en faciliter l'accès pour des petits acteurs capables de proposer de multiples solutions différentes et alternatives adaptées aux besoins de confiance des utilisateurs, éventuellement avec la caution de l'État, la réputation d'associations nationales (association de défense des droits de l'homme, associations de consommateurs, etc.), bref, des assurances concrètes qui impliquent les utilisateurs, c'est-à-dire comme le modèle du logiciel libre qui ne sépare pas utilisation et conception.

Ce que propose le logiciel libre, c'est davantage que du code, c'est un modèle de coopération et de cooptation. C'est la possibilité pour les dynamiques collectives de réinvestir des espaces de confiance. Quel que soit le service utilisé, il dépend d'une communauté, d'une association, ou d'une entreprise  qui toutes s'engagent par contrat (les licences libres sont des contrats) à respecter les données des utilisateurs. Le tiers de confiance devient collectif, il n'est plus un, il est multiple. Il ne s'agit pas pour autant de diluer les responsabilités (chaque offre est indépendante et s'engage individuellement) mais de forcer l'engagement public et la transparence des recours.



## Chatons solidaires

Si s'échapper seul du marché de Big Other ne peut se faire qu'au prix de l'exclusion, il faut opposer une logique différente capable, devant l'exploitation des données et notre aliénation de consommateurs, de susciter l'action et non la passivité, le collectif et non l'individu. Si je produis des données, il faut qu'elles puissent profiter à tous ou à personne. C'est la logique de l'*open data* qui garanti le libre accès à des données placées dans le *bien commun*. Ce commun a ceci d'intéressant que tout ce qui y figure est alors identifiable. Ce qui n'y figure pas relève alors du privé et diffuser une donnée privée ne devrait relever que du seul choix délibéré de l'individu.

Le modèle proposé par [le projet CHATONS](http://framablog.org/2016/02/09/chatons-le-collectif-anti-gafam/) (Collectif d’Hébergeurs Alternatifs, Transparents, Ouverts, Neutres et Solidaires) de Framasoft repose en partie sur ce principe. En adhérant à la charte, un hébergeur s'engage publiquement à remplir certaines obligations et, parmi celles-ci, l'interdiction d'utiliser les données des utilisateurs pour autre chose que l'évaluation et l'amélioration des services.

Comme écrit dans [son manifeste](https://framagit.org/framasoft/CHATONS/tree/master) (en version 0.9), l'objectif du collectif CHATONS est simple&nbsp;:

> L'objectif de ce collectif est de mailler les initiatives de services basés sur des solutions de logiciels libres et proposés aux utilisateurs de manière à diffuser toutes les informations utiles permettant au public de pouvoir choisir leurs services en fonction de leurs besoins, avec un maximum de confiance vis-à-vis du respect de leur vie privée, sans publicité ni clause abusive ou obscure.

En lançant l'initiative de ce collectif, sans toutefois se placer dans une posture verticale fédérative, Framasoft ouvre une porte d'échappatoire. L'essentiel n'est pas tant de maximiser la venue de consommateurs sur quelques services identifiés mais de maximiser les chances pour les consommateurs d'utiliser des services de confiance qui, eux, pourront essaimer et se multiplier. Le postulat est que l'utilisateur est plus enclin à faire confiance à un hébergeur qu'il connaît et qui s'engage devant d'autres hébergeurs semblables, ses pairs, à respecter ses engagements éthiques. Outre l'utilisation de logiciels libres, cela se concrétise par trois principes&nbsp;

   1. Mutualiser l'expertise&nbsp;: les membres du collectif s'engagent à partager les informations pour augmenter la qualité des services et pour faciliter l'essaimage des solutions de logiciels libres utilisées. C'est un principe de solidarité qui permettra, à terme, de consolider sérieusement le tissu des hébergeurs concernés.
   2. Publier et adhérer à une [charte commune](https://framagit.org/framasoft/CHATONS/blob/master/docs/Charter-fr.md). Cette charte donne les principaux critères techniques et éthiques de l'activité de membre. L'engagement suppose une réciprocité des échanges tant avec les autres membres du collectif qu'avec les utilisateurs.
   3. Un ensemble de contraintes dans la charte publique implique non seulement de ne s'arroger aucun droit sur les données privées des utilisateurs, mais aussi de rester transparent y compris jusque dans la publication de ses comptes et rapports d'activité. En tant qu'utilisateur je connais les engagements de mon hébergeur, je peux agir pour le contraindre à les respecter tout comme les autres hébergeurs peuvent révoquer de leur collectif un membre mal intentionné. 

Le modèle CHATONS n'est absolument pas exclusif. Il constitue même une caricature, en quelque sorte l'alternative contraire mais pertinente à GAFAM. Des entreprises tout à fait bien intentionnées, respectant la majeure partie de la charte, pourraient ne pas pouvoir entrer dans ce collectif à cause des contraintes liées à leur modèle économique. Rien n'empêche ces entreprises de monter leur propre collectif, avec un niveau de confiance différent mais néanmoins potentiellement acceptable. Si le collectif CHATONS décide de restreindre au domaine associatif des acteurs du mouvement, c'est parce que l'idée est d'essaimer au maximum le modèle&nbsp;: que l'association philatélique de Trifouille-le-bas puisse avoir son serveur de courriel, que le club de football de Trifouille-le-haut puisse monter un serveur owncloud et que les deux puissent mutualiser en devenant des CHATONS pour en faire profiter, pourquoi pas, les deux communes (et donc la communauté).



## Conclusion

La souveraineté numérique est souvent invoquée par les autorités publiques bien conscientes des limitations du pouvoir qu'implique Big Other. Par cette expression, on pense généralement se situer à armes égales contre les géants du web. Pouvoir économique contre pouvoir républicain. Cet affrontement est perdu d'avance par tout ce qui ressemble de près ou de loin à une procédure institutionnalisée. Tant que les citoyens ne sont pas eux-mêmes porteurs d'alternatives au marché imposé par les GAFAM, l'État ne peut rien faire d'autre que de lutter pour sauvegarder son propre pouvoir, c'est-à-dire celui des partis comme celui des lobbies. Le serpent se mord la queue.

Face au capitalisme de surveillance, qui a depuis longtemps dépassé le stade de l'ultra-libéralisme (car malgré tout il y subsistait encore un marché et une autonomie des agents), c'est à l'État non pas de proposer un *cloud souverain* mais de se positionner comme le garant des principes démocratiques appliqués à nos données numériques et à Internet&nbsp;: neutralité sans faille du réseau, garantie de l'équité, surveillance des accords commerciaux, politique anti-trust affirmée, etc. L'éducation populaire, la solidarité, l'intelligence collective, l'expertise des citoyens, feront le reste, quitte à ré-inventer d'autres modèles de gouvernance (et de gouvernement).

Nous ne sommes pas des êtres a-technologiques. Aujourd'hui, nos vies sont pleines de données numériques. Avec Internet, nous en usons pour nous rapprocher, pour nous comprendre et mutualiser nos efforts, tout comme nous avons utilisé en leurs temps le téléphone, les voies romaines, le papyrus, et les tablettes d'argile. Nous avons des choix à faire. Nous pouvons nous diriger vers des modèles exclusifs, avec ou sans GAFAM, comme la généralisation de *[block chains](https://fr.wikipedia.org/wiki/Cha%C3%AEne_de_blocs)* pour toutes nos transactions. En automatisant les contrats, même avec un système ouvert et neutre, nous ferions alors reposer la confiance sur la technologie (et ses failles), en révoquant toute relation de confiance entre les individus, comme si toute relation interpersonnelle était vouée à l'échec. L'autre voie repose sur l'éthique et le partage des connaissances. Le projet CHATONS n'est qu'un exemple de système alternatif parmi des milliers d'autres dans de multiples domaines, de l'agriculture à l'industrie, en passant par la protection de l'environnement. Saurons-nous choisir la bonne voie&nbsp;? Saurons-nous comprendre que les relations d'interdépendances qui nous unissent sur terre passent aussi bien par l'écologie que par le partage du code&nbsp;?


# Du capitalisme de surveillance à la fin de la démocratie&nbsp;?

Dans ce chapitre, nous partirons de cette affirmation : derrière les discours politiques modernistes et nourris à la *corporate novlangue*[^delaportenote] se cache une conception de l'État non seulement dépassée mais aussi socialement rétrograde. Nous allons voir comment ce que Shoshana Zuboff nomme *Big Other*[@zuboff_big_2015] trouve dans ces archaïques conceptions de l'État un lieu privilégié pour déployer une nouvelle forme d'organisation sociale et politique. L'idéologie-Silicon ne peut plus être aujourd'hui analysée comme un élan ultra-libéral auquel on opposerait des valeurs d'égalité ou de solidarité. Cette dialectique est dépassée car c'est le Contrat Social qui change de nature~: la légitimité de l'État repose désormais sur des mécanismes d'expertise[^notedelmas] par lesquels le capitalisme de surveillance impose une logique de marché à tous les niveaux de l'organisation socio-économique, de la décision publique à l'engagement politique. Pour comprendre comment le terrain démocratique a changé à ce point et ce que cela implique dans l'organisation d'une nation, il faut analyser tour à tour le rôle des monopoles numériques, les choix de gouvernance qu'ils impliquent, et comprendre comment cette idéologie est non pas théorisée, mais en quelque sorte auto-légitimée, rendue presque nécessaire, parce qu'aucun choix politique ne s'y oppose. Le capitalisme de surveillance impliquerait-il la fin de la démocratie~?

[^delaportenote]: Une chronique de Xavier De La Porte en juin 2017 sur le site de la radio France Culture pointe une sortie du tout nouveau président Emmanuel Macron parue sur le compte Twitter officiel~: «&nbsp;Une start-up nation est une nation où chacun peut se dire qu'il pourra créer une startup. Je veux que la France en soit une&nbsp;». Xavier De La Porte montre à quel point cette conception de la France en «&nbsp;start-up nation&nbsp;» est en réalité une vieille idée, qui reprend les archaïsmes des penseurs libéraux du <span style="font-variant:small-caps;">xvii</span>^e^ siècle, tout en provoquant un «&nbsp;désenchantement politique&nbsp;»[@Delaporte2017].

[^notedelmas]: C'est ce que montre Corinne Delmas, d'un point de vue sociologique[@Delmas2011]. Alain Supiot, dans *La gouvernance par les nombres* (cité plus loin), choisit quant à lui une approche avec les clés de lecture du Droit.

Une chronique de Xavier De La Porte[@Delaporte2017] sur le site de la radio France Culture pointe une sortie du tout nouveau président Emmanuel Macron parue sur le compte Twitter officiel&nbsp;: «&nbsp;Une start-up nation est une nation où chacun peut se dire qu’il pourra créer une startup. Je veux que la France en soit une&nbsp;». Xavier De La Porte montre à quel point cette conception de la France en «&nbsp;start-up nation&nbsp;» est en réalité une vieille idée, qui reprend les archaïsmes des penseurs libéraux du XVII^e^ siècle, tout en provoquant un «&nbsp;désenchantement politique&nbsp;». [La série des Nouveaux Léviathans](https://framablog.org/tag/leviathans/), dont voici le troisième numéro, part justement de cette idée et cherche à en décortiquer les arguments.

## Libéralisme et *Big Other*


Dans le chapitre précédent, j'abordais la question du capitalisme de surveillance sous l'angle de la fin du modèle économique du marché libéral. L'utopie dont se réclame ce dernier, que ce soit de manière rhétorique ou réellement convaincue, suppose une auto-régulation du marché, théorie maintenue en particulier par Friedrich Hayek[@Hayek2013]. À l'opposé de cette théorie qui fait du marché la seule forme (auto-)équilibrée de l'économie, on trouve des auteurs comme Karl Polanyi[@Polanyi2009] qui, à partir de l'analyse historique et anthropologique, démontre non seulement que l'économie n'a pas toujours été organisée autour d'un marché libéral, mais aussi que le capitalisme «&nbsp;désencastre&nbsp;» l'économie des relations sociales, et provoque un déni du contrat social. Toutefois, avec le capitalisme de surveillance, cette opposition (qui date tout de même de la première moitié du <span style="font-variant:small-caps;">xx</span>^e^ siècle) a vécu.

Lorsque Shoshana Zuboff aborde la genèse du capitalisme de surveillance, elle montre comment, à partir de la logique de rationalisation du travail, on est passé à une société de marché dont les comportements individuels et collectifs sont quantifiés, analysés, surveillés, grâce aux *big data*, tout comme le (un certain) management d'entreprise quantifie et rationalise les procédures. Pour S. Zuboff, tout ceci concourt à l'avènement de *Big Other*, c'est-à-dire un régime socio-économique régulé par des mécanismes d’extraction des données, de marchandisation et de contrôle. Cependant, ce régime ne se confronte pas à l'État comme on pourrait le dire du libertarisme sous-jacent au néolibéralisme qui considère l'État au pire comme contraire aux libertés individuelles, au mieux comme une instance limitative des libertés. Encore pourrait-on dire qu'une dialectique entre l'État et le marché pourrait être bénéfique et aboutirait à une forme d'équilibre acceptable. Or, avec le capitalisme de surveillance, le politique lui-même devient un point d'appui pour *Big Other*, et il le devient parce que nous avons basculé d'un régime politique à un régime a-politique qui organise les équilibres sociaux sur les principes de l'offre marchande. Les instruments de cette organisation sont les *big datas* et la capacité de modeler la société sur l'offre. C'est que je précisais en 2016 dans un ouvrage coordonné par Tristan Nitot, Nina Cercy, *Numérique&nbsp;: reprendre le contrôle*[@Masutti2016chap], en ces termes&nbsp;:

> (L)es firmes mettent en œuvre des pratiques d’extraction de données qui annihilent toute réciprocité du contrat avec les utilisateurs, jusqu’à créer un marché de la quotidienneté (nos données les plus intimes et à la fois les plus sociales). Ce sont nos comportements, notre expérience quotidienne, qui deviennent l’objet du marché et qui conditionne même la production des biens industriels (dont la vente dépend de nos comportements de consommateurs). Mieux&nbsp;: ce marché n’est plus soumis aux contraintes du hasard, du risque ou de l’imprédictibilité, comme le pensaient les chantres du libéralisme du <span style="font-variant:small-caps;">xx</span>^e^ siècle&nbsp;: il est devenu malléable parce que ce sont nos comportements qui font l’objet d’une prédictibilité d’autant plus exacte que les *big data* peuvent être analysées avec des méthodes de plus en plus fiables et à grande échelle.

Si j'écris que nous sommes passés d'un régime politique à un régime a-politique, cela ne signifie pas que cette transformation soit radicale, bien entendu. Il existe et il existera toujours des tensions idéologiques à l'intérieur des institutions de l'État. C'est plutôt une question de proportions&nbsp;: aujourd'hui, la plus grande partie des décisions et des organes opérationnels sont motivés et guidés par des considérations relevant de situations déclarées impératives et non par des perspectives politiques. On peut citer par exemple le grand mouvement de «&nbsp;rigueur&nbsp;» incitant à la «&nbsp;maîtrise&nbsp;» des dépenses publiques imposée par les organismes financiers européens&nbsp;; des décisions motivées uniquement par le remboursement des dettes et l'expertise financière et non par une stratégie du bien-être social. On peut citer aussi, d'un point de vue plus local et français, les contrats des institutions publiques avec Microsoft, à l'instar de l'Éducation Nationale, à l'encontre de l'avis d'une grande partie de la société civile, au détriment d'une offre différente (comme le libre et l'open source) et dont la justification est uniquement donnée par l'incapacité de la fonction publique à envisager d'autres solutions techniques, non par ignorance, mais à cause du détricotage massif des compétences internes. Ainsi «&nbsp;rationaliser&nbsp;» les dépenses publiques revient en fait à se priver justement de rationalité au profit d'une simple adaptation de l'organisation publique à un état de fait, un déterminisme qui n'est pas remis en question et condamne toute idéologie à être non pertinente.

Ce n'est pas pour autant qu'il faut ressortir les vieilles théories de la fin de l'histoire. Qui plus est, les derniers essais du genre, comme la thèse de Francis Fukuyama[@Fukuyama1992], se sont concentrés justement sur l'avènement de la démocratie libérale conçue comme le consensus ultime mettant fin aux confrontations idéologiques (comme la fin de la Guerre Froide). Or, le capitalisme de surveillance *a minima* repousse toute velléité de consensus, au-delà du libéralisme, car il finit par définir l'État tout entier comme un instrument d'organisation, quelle que soit l'idéologie&nbsp;: si le nouveau régime de *Big Other* parvient à organiser le social, c'est aussi parce que ce dernier a désengagé le politique et relègue la décision publique au rang de validation des faits, c'est-à-dire l'acceptation des contrats entre les individus et les outils du capitalisme de surveillance. Les mécanismes ne sont pas si nombreux et tiennent en quatre points&nbsp;:

-   le fait que les firmes soient des multinationales et surfent sur l'offre de la moins-disance juridique pour s'établir dans les pays (c'est la pratique du *law shopping*),
-   le fait que l'utilisation des données personnelles soit déloyale envers les individus-utilisateurs des services des firmes qui s'approprient les données,
-   le fait que les firmes entre elles adoptent des processus loyaux (pactes de non-agression, partage de marchés, acceptation de monopoles, rachats convenus, etc.) et passent des contrats iniques avec les institutions, avec l'appui de l'expertise, faisant perdre aux États leur souveraineté numérique,
-   le fait que les monopoles «&nbsp;du numérique&nbsp;» diversifient tellement leurs activités vers les secteurs industriels qu'ils finissent par organiser une grande partie des dynamiques d'innovation et de concurrence à l'échelle mondiale.

Pour résumer les trois conceptions de l'économie dont il vient d'être question, on peut dresser ce tableau&nbsp;:




| Économie          |  Forme        |  Individus       |        État |
|:------------------|:--------------|:-----------------|:------------|
| Économie spontanée | Diversité et créativité des formes d'échanges, du don à la financiarisation | Régulent l'économie par la démocratie&nbsp;; les échanges sont d'abord des relations sociales | Garant de la redistribution équitable des richesses&nbsp;; régulateur des échanges et des comportements |
| Marché libéral | Auto-régulation, défense des libertés économiques contre la décision publique (conception libérale de la démocratie&nbsp;: liberté des échanges et de la propriété) | Agents consommateurs décisionnaires dans un milieu concurrentiel | Réguler le marché contre ses dérives inégalitaires&nbsp;; maintient une démocratie plus ou moins forte |
| Capitalisme de surveillance | Les monopoles façonnent les échanges, créent (tous) les besoins en fonction de leurs capacités de production et des *big data* | Sont exclusivement utilisateurs des biens et services | Automatisation du droit adapté aux besoins de l'organisation économique&nbsp;; sécurisation des conditions du marché |


Il est important de comprendre deux aspects de ce tableau&nbsp;:

-   il ne cherche pas à induire une progression historique et linéaire entre les différentes formes de l'économie et des rapports de forces&nbsp;: ces rapports sont le plus souvent diffus, selon les époques, les cultures. Il y a une économie spontanée à l'Antiquité comme on pourrait par exemple, comprendre les monnaies alternatives d'aujourd'hui comme des formes spontanées d'organisation des échanges.
-   aucune de ces cases ne correspond réellement à des conceptions théorisées. Il s'agit essentiellement de voir comment le capitalisme de surveillance induit une distorsion dans l'organisation économique&nbsp;: alors que dans des formes classiques de l'organisation économique, ce sont les acteurs qui produisent l'organisation, le capitalisme de surveillance induit non seulement la fin du marché libéral (vu comme place d'échange équilibrée de biens et services concurrentiels) mais exclut toute possibilité de régulation par les individus / citoyens&nbsp;: ceux-ci sont vus uniquement comme des utilisateurs de services, et l'État comme un pourvoyeur de services publics. La décision publique, elle, est une affaire d'accord entre les monopoles et l'État.



## Les monopoles et l'État


Pour sa première visite en Europe, Sundar Pichai qui était alors en février 2016 le nouveau CEO de Google Inc., choisit les locaux de Sciences Po. Paris pour tenir une conférence de presse[@durand2016], en particulier devant les élèves de l'école de journalisme. Le choix n'était pas anodin, puisqu'à ce moment-là Google s'est présenté en grand défenseur de la liberté d'expression (par un ensemble d'outils, de type *reverse-proxy* que la firme est prête à proposer aux journalistes pour mener leurs investigations), en pourvoyeur de moyens efficaces pour lutter contre le terrorisme, en proposant à qui veut l'entendre des partenariats avec les éditeurs, et de manière générale en s'investissant dans l'innovation numérique en France (voir le partenariat Numa / Google). Tout cela démontre, s'il en était encore besoin, à quel point la firme Google (et Alphabet en général) est capable de proposer une offre si globale qu'elle couvre les fonctions de l'État&nbsp;: en réalité, à Paris lors de cette conférence, alors que paradoxalement elle se tenait dans les locaux où étudient ceux qui demain sont censés remplir des fonctions régaliennes, Sundar Pichai ne s'adressait pas aux autorités de l'État mais aux entreprises (éditeurs) pour leur proposer des instruments qui garantissent leurs libertés. Avec comme sous-entendu&nbsp;: vous évoluez dans un pays dont la liberté d'expression est l'un des fleurons, mais votre gouvernement n'est pas capable de vous le garantir mieux que nous, donc adhérez à Google. Les domaines de la santé, des systèmes d'informations et l'éducation en sont pas exempts de cette offre «&nbsp;numérique&nbsp;».

Du côté du secteur public, le meilleur moyen de ne pas perdre la face est de monter dans le train suivant l'adage «&nbsp;Puisque ces mystères nous dépassent, feignons d'en être l'organisateur&nbsp;». Par exemple, si Google et Facebook ont une telle puissance capable de mener efficacement une lutte, au moins médiatique, contre le terrorisme, à l'instar de leurs campagnes
de propagande[@lesechos2017], il faut créer des accords de collaboration entre l'État et ces firmes[@Cassini2017], quitte à les faire passer comme une exigence gouvernementale (mais quel État ne perdrait pas la face devant le poids financier des GAFAM&nbsp;?).

… Et tout cela crée un marché de la gouvernance dans lequel on ne compte plus les millions d'investissement des GAFAM. Ainsi, la gouvernance est un marché pour Microsoft, qui lance un [Office 2015 spécial «&nbsp;secteur public&nbsp;»](https://products.office.com/fr-fr/government/office-365-web-services-for-government), ou, mieux, qui sait admirablement se situer dans les appels d'offre en promouvant des solutions pour tous les besoins d'organisation de l'État. Par exemple, la présentation des activités de [Microsoft dans le secteur public](https://partner.microsoft.com/fr-fr/solutions/government) sur son site comporte ces items&nbsp;:

> -   Stimulez la transformation numérique du secteur public
> -   Optimisez l'administration publique
> -   Transformez des services du secteur public
> -   Améliorez l'efficacité des employés du secteur public
> -   Mobilisez les citoyens


![Microsoft dans le secteur public](imgs/743906437.png)

![Microsoft Office pour le secteur public](imgs/1257700459.png)


D'aucuns diraient que ce que font les GAFAM, c'est proposer un nouveau modèle social. Par exemple dans une enquête percutante sur les entreprises de la Silicon Valley, Philippe Vion-Dury définit ce nouveau modèle comme «&nbsp;politiquement technocratique, économiquement libéral, culturellement libertaire, le tout nimbé de messianisme typiquement américain&nbsp;»[@philviodur2016]. Et il a raison, sauf qu'il ne s'agit pas d'un modèle social, c'est justement le contraire, c'est un modèle de gouvernance sans politique, qui considère le social comme la juxtaposition d'utilisateurs et de groupes d'utilisateurs. Comme le montre l'offre de Microsoft, si cette firme est capable de fournir un service propre à «&nbsp;mobiliser les citoyens&nbsp;» et si en même temps, grâce à ce même fournisseur, vous avez les outils pour transformer des services du secteur public, quel besoin y aurait-il de voter, de persuader, de discuter&nbsp;? si tous les avis des citoyens sont analysés et surtout anticipés par les *big datas*, et si les seuls outils efficaces de l'organisation publique résident dans l'offre des GAFAM, quel besoin y aurait-il de parler de démocratie&nbsp;?

En réalité, comme on va le voir, tout cette nouvelle configuration du capitalisme de surveillance n'est pas seulement rendue possible par la puissance novatrice des monopoles du numérique. C'est peut-être un biais&nbsp;: penser que leur puissance d'innovation est telle qu'aucune offre concurrente ne peut exister. En fait, même si l'offre était moindre, elle n'en serait pas moins adoptée car tout réside dans la capacité de la décision publique à déterminer la nécessité d'adopter ou non les services des GAFAM. C'est l'aménagement d'un terrain favorable qui permet à l'offre de la gouvernance numérique d'être proposée. Ce terrain, c'est la décision par l'expertise. 


## L'accueil favorable au capitalisme de surveillance


Dans son livre *The united states of Google*[@Hamman2015], Götz Haman fait un compte-rendu d'une conférence durant laquelle interviennent Eric Schmidt, alors président du conseil d’administration de Google, et son collègue Jared Cohen. Ces derniers ont écrit un ouvrage (*The New Digital Age*) qu'ils présentent dans les grandes lignes. Götz Haman le résume en ces termes&nbsp;: «&nbsp;Aux yeux de Google, les États sont dépassés. Ils n’ont rien qui permette de résoudre les problèmes du XXI^e^ siècle, tels le changement climatique, la pauvreté, l’accès à la santé. Seules les inventions techniques peuvent mener vers le Salut, affirment Schmidt et son camarade Cohen.&nbsp;»

Une fois cette idéologie &ndash;&nbsp;celle du capitalisme de surveillance[^morozov]&nbsp;&ndash;  évoquée, il faut s'interroger sur la raison pour laquelle les États renvoient cette image d'impuissance. En fait, les sociétés occidentales modernes ont tellement accru leur consommation de services que l'offre est devenue surpuissante, à tel point, comme le montre Shoshanna Zuboff, que les utilisateurs eux-mêmes sont devenus à la fois les pourvoyeurs de matière première (les données) et les consommateurs. Or, si nous nous plaçons dans une conception de la société comme un unique marché où les relations sociales peuvent être modelées par l'offre de services (ce qui se cristallise aujourd'hui par ce qu'on nomme dans l'expression-valise «&nbsp;Uberisation de la société&nbsp;»), ce qui relève de la décision publique ne peut être motivé que par l'analyse de ce qu'il y a de mieux pour ce marché, c'est-à-dire le calcul de rentabilité, de rendement, d'efficacité&hellip; d'utilité. Or cette analyse ne peut être à son tour fournie par une idéologie visionnaire, une utopie ou simplement l'imaginaire politique&nbsp;: seule l'expertise de l'état du monde pour ce qu'il est à un instant T permet de justifier l'action publique. Il faut donc passer du concept de gouvernement politique au concept de gouvernance par les instruments. Et ces instruments doivent reposer sur les GAFAM.

[^morozov]: On pourrait ici affirmer que ce qui est en jeu ici est le solutionnisme technologique, tel que le critique Evgeny Morozov. Certes, c'est aussi ce que Götz Haman démontre&nbsp;: à vouloir adopter des solutions web-centrées et du *data mining* pour mécaniser les interactions sociales, cela revient à les privatiser par les GAFAM. Mais ce que je souhaite montrer ici, c'est que la racine du capitalisme de surveillance est une idéologie dont le solutionnisme technologique n'est qu'une résurgence (un rhizome, pour filer la métaphore végétale). Le phénomène qu'il nous faut comprendre, c'est que l'avènement du capitalisme de surveillance n'est pas dû uniquement à cette tendance solutionniste, mais il est le résultat d'une convergence entre des renversements idéologiques (fin du libéralisme classique et dénaturation du néo-libéralisme), des nouvelles organisations (du travail, de la société, du droit), des innovations technologiques (le web, l'extraction et l'exploitation des données), de l'abandon du politique. On peut néanmoins lire avec ces clés le remarquable ouvrage de Evgeny Morozov, *Pour tout résoudre cliquez ici&nbsp;: L'aberration du solutionnisme technologique*[@Morozov2014].

Pour comprendre au mieux ce que c'est que gouverner par les instruments, il faut faire un petit détour conceptuel.

### L'expertise et les instruments

Prenons un exemple. La situation politique qu'a connue l'Italie après novembre 2011 pourrait à bien des égards se comparer avec la récente élection en France d'Emmanuel Macron et les élections législatives qui ont suivi. En effet, après le gouvernement de Silvio Berlusconi, la présidence italienne a nommé Mario Monti pour former un gouvernement dont les membres sont essentiellement reconnus pour leurs compétences techniques appliquées en situation de crise économique. La raison du soutien populaire à cette nomination pour le moins discutable (M. Monti a été nommé sénateur à vie, reconnaissance habituellement réservée aux anciens présidents de République Italienne) réside surtout dans le désaveu de la *casta*, c'est-à-dire le système des partis qui a dominé la vie politique italienne depuis maintes années et qui n'a pas réussi à endiguer les effets de la crise financière de 2008. Si bien que le gouvernement de Mario Monti peut être qualifié de «&nbsp;gouvernement des experts&nbsp;», non pas un gouvernement technocratique noyé dans le fatras administratif des normes et des procédures, mais un gouvernement à l'image de Mario Monti lui-même, ex-commissaire européen au long cours, motivé par la nécessité *technique* de résoudre la crise en coopération avec l'Union Européenne. Pour reprendre les termes de l'historien Peppino Ortoleva, à propos de ce gouvernement dans l'étude de cas qu'il consacre à l'Italie en 2012[@Ortoleva2012]&nbsp;:

> Le «&nbsp;gouvernement des experts&nbsp;» se présente d’un côté comme le gouvernement de l’objectivité et des chiffres, celui qui peut rendre compte à l’Union européenne et au système financier international, et d’un autre côté comme le premier gouvernement indépendant des partis.

Peppino Ortoleva conclut alors que cet exemple italien ne représente que les prémices pour d'autres gouvernements du même acabit dans d'autres pays, avec tous les questionnements que cela suppose en termes de débat politique et démocratique&nbsp;: si en effet la décision publique n'est mue que par la nécessité (ici la crise financière et la réponse aux injonctions de la Commission européenne) quelle place peut encore tenir le débat démocratique et l'autonomie décisionnaire des peuples&nbsp;?

En son temps déjà le «&nbsp;There is no alternative&nbsp;» de Margaret Thatcher imposait par la force des séries de réformes au nom de la nécessité et de l'expertise économiques. On ne compte plus, en Europe, les gouvernements qui nomment des groupes d'expertise, conseils et autres comités censés répondre aux questions techniques que pose l'environnement économique changeant, en particulier en situation de crise.

Cette expertise a souvent été confondue avec la technocratie, à l'instar de l'ouvrage de Vincent Dubois et Delphine Dulong publié en 2000, *La question technocratique*[@Dubois2000]. Lorsqu'en effet la décision publique se justifie exclusivement par la nécessité, cela signifie que cette dernière est définie en fonction d'une certaine compréhension de l'environnement socio-économique. Par exemple, si l'on part du principe que la seule réponse à la crise financière est la réduction des dépenses publiques, les technocrates inventeront les instruments pour rendre opérationnelle la décision publique, les experts identifieront les méthodes et l'expertise justifiera les décisions (on remet en cause un avis issu d'une estimation de ce que devrait être le monde, mais pas celui issu d'un calcul d'expert).

La technocratie comme l'expertise se situent hors des partis, mais la technocratie concerne surtout l'organisation du gouvernement. Elle répond souvent aux contraintes de centralisation de la décision publique. Elle crée des instruments de surveillance, de contrôle, de gestion, etc. capables de permettre à un gouvernement d'imposer, par exemple, une transformation économique du service public. L'illustration convaincante est le gouvernement Thatcher, qui dès 1979 a mis en place plusieurs instruments de contrôle visant à libéraliser le secteur public en cassant les pratiques locales et en imposant un système concurrentiel. Ce faisant, il démontrait aussi que le choix des instruments suppose aussi des choix d'exercice du pouvoir, tels ceux guidés par la croyance en la supériorité des mécanismes de marché pour organiser l'économie[@Lascoumes2004].

Gouverner par l'expertise ne signifie donc pas que le gouvernement manque de compétences en son sein pour prendre les (bonnes ou mauvaises) décisions publiques. Les technocrates existent et sont eux aussi des experts. En revanche, l'expertise permet surtout de justifier les choix, les stratégies publiques, en interprétant le monde comme un environnement qui contraint ces choix, sans alternative.

En parlant d'alternative, justement, on peut s'interroger sur celles qui relèvent de la société civile et portées tant bien que mal à la connaissance du gouvernement. La question du logiciel libre est, là encore, un bon exemple.

En novembre 2016, Framasoft publiait un billet retentissant intitulé «&nbsp;Pourquoi Framasoft n’ira plus prendre le thé au ministère de l’Éducation Nationale&nbsp;»[@Framasoft2016]. La raison de ce billet est la prise de conscience qu'après plus de treize ans d'efforts de sensibilisation au logiciel libre envers les autorités publiques, et en particulier l'Éducation Nationale, Framasoft ne pouvait plus dépenser de l'énergie à coopérer avec une telle institution si celle-ci finissait fatalement par signer contrats sur contrats avec Microsoft ou Google. En fait, le raisonnement va plus loin et j'y reviendrai plus tard dans ce texte. Mais il faut comprendre que ce à quoi Framasoft s'est confronté est exactement ce gouvernement par l'expertise. En effet, les communautés du logiciel libre n'apportent une expertise que dans la mesure où elles proposent de changer de modèle&nbsp;: récupérer une autonomie numérique en développant des compétences et des initiatives qui visent à atteindre un fonctionnement idéal (des données protégées, des solutions informatiques modulables, une contribution collective au code, etc.). Or, ce que le gouvernement attend de l'expertise, ce n'est pas un but à atteindre, c'est savoir comment adapter l'organisation au modèle existant, c'est-à-dire celui du marché.

Dans le cadre des élections législatives, l'infatigable association APRIL («&nbsp;promouvoir et défendre le logiciel libre&nbsp;») lance sa [campagne de promotion](https://www.april.org/nouvelle-legislature-deja-26-depute-e-s-engage-e-s-en-faveur-de-la-priorite-au-logiciel-libre-dans-l) de la priorité au logiciel libre dans l'administration publique. À chaque fois, la campagne connaît un certain succès et des députés s'engagent réellement dans cette cause qu'ils plaident même à l'intérieur de l'Assemblée Nationale. Sous le gouvernement de F. Hollande, on a entendu des députés comme Christian Paul ou Isabelle Attard avancer les arguments les plus pertinents et sans ménager leurs efforts, convaincus de l'intérêt du Libre. À leur image, il serait faux de dire que la sphère politique est toute entière hermétique au logiciel libre et aux équilibres numériques et économiques qu'il porte en lui. Peine perdue&nbsp;? À voir les contrats passés entre le gouvernement et les GAFAM, c'est un constat qu'on ne peut pas écarter et sans doute au profit d'une autre forme de mobilisation, celle du peuple lui-même, car lui seul est capable de porter une alternative là où justement la politique a cédé la place&nbsp;: dans la décision publique.

La rencontre entre la conception du marché comme seule organisation gouvernementale des rapports sociaux et de l'expertise qui détermine les contextes et les nécessités de la prise de décision a permis l'émergence d'un terrain favorable à l'État-GAFAM. Pour s'en convaincre il suffit de faire un tour du côté de ce qu'on a appelé la «&nbsp;modernisation de l'État&nbsp;».

### Les firmes à la gouvernance numérique

Anciennement la Direction des Systèmes d'Information (DSI), la DINSIC (Direction Interministérielle du Numérique et du Système d'Information et de Communication) définit les stratégies et pilote les structures informationnelles de l'État français. Elle prend notamment part au mouvement de «&nbsp;modernisation&nbsp;» de l'État. Ce mouvement est en réalité une cristallisation de l'activité de réforme autour de l'informatisation commencée dans les années 1980. Cette activité de réforme a généré des compétences et assez d'expertise pour être institutionnalisée (DRB, DGME, aujourd'hui DIATP —&nbsp;Direction interministérielle pour l'accompagnement des transformations publiques). On se perd facilement à travers les acronymes, les ministères de rattachement, les changements de noms au rythme des fusions des services entre eux. Néanmoins, le concept même de réforme n'a pas évolué depuis les grandes réformes des années 1950&nbsp;: il faut toujours adapter le fonctionnement des administrations publiques au monde qui change, en particulier le numérique.

La différence, aujourd'hui, c'est que cette adaptation ne se fait pas en fonction de stratégies politiques, mais en fonction d'un cadre de productivité, dont on dit qu'il est un «&nbsp;contrat de performance »&nbsp;; cette performance étant évaluée par des outils de contrôle&nbsp;: augmenter le rendement de l'administration en «&nbsp;rationalisant&nbsp;» les effectifs, automatiser les services publics (par exemple déclarer ses impôts en ligne, payer ses amendes en lignes, etc.), expertiser (accompagner) les besoins des systèmes d'informations selon les offres du marché, limiter les instances en adaptant des méthodes agiles de prise de décision basées sur des outils numériques de l'analyse de data, maîtrise des coûts…

C'est que nous dit en substance la Synthèse présentant le Cadre stratégique commun du système d'information de l'Etat[@Dinsic2013], c'est-à-dire la feuille de route de la DINSIC. Dans une section intitulée «&nbsp;Pourquoi se transformer est une nécessite&nbsp;?&nbsp;», on trouve&nbsp;:

> Continuer à faire évoluer les systèmes d’information est nécessaire pour répondre aux enjeux publics de demain&nbsp;: il s’agit d'un outil de production de l'administration, qui doit délivrer des services plus performants aux usagers, faciliter et accompagner les réformes de l'État, rendre possible les politiques publiques transverses à plusieurs administrations, s'intégrer dans une dimension européenne.


Cette feuille de route concerne en fait deux grandes orientations&nbsp;: l'amélioration de l'organisation interne aux institutions gouvernementales et les interfaces avec les citoyens. Il est flagrant de constater que, pour ce qui concerne la dimension interne, certains projets que l'on trouve mentionnés dans le [Panorama des grands projets SI de l'Etat](http://www.modernisation.gouv.fr/ladministration-change-avec-le-numerique/par-son-systeme-dinformation/panorama-des-grands-projets-si-de-letat) font appel à des solutions *open source* et les opérateurs sont publics, notamment par souci d'efficacité, comme c'est le cas, par exemple pour le projet [VITAM](https://linuxfr.org/forums/general-petites-annonces/posts/annonce-de-presentation-aux-entreprises-d-un-projet-open-source-de-l-etat-vitam), relatif à l'archivage. En revanche, lorsqu'il s'agit des relations avec les citoyens-utilisateurs, c'est-à-dires les «&nbsp;usagers&nbsp;», ce sont des entreprises comme Microsoft qui entrent en jeu et se substituent à l'État, comme c'est le cas par exemple du grand projet France Connect, dont Microsoft France [est partenaire](https://experiences.microsoft.fr/Video/contribuer-a-lecosysteme-franceconnect/99895f63-fdd1-4be7-8e79-b3ce0dfb02da).

En effet, France Connect est une plateforme centralisée visant à permettre aux citoyens d'effectuer des démarches en ligne (pour les particuliers, pour les entreprises, etc.). Pour permettre aux collectivités et aux institutions qui mettent en place une «&nbsp;offre&nbsp;» de démarche en ligne, Microsoft propose en *open source* des «&nbsp;kit de démarrage&nbsp;», c'est à dire des modèles, qui vont permettre à ces administrations d'offrir ces services aux usagers. En d'autres termes, c'est chaque collectivité ou administration qui va devenir fournisseur de service, dans un contexte de développement technique mutualisé (d'où l'intérêt ici de l'*open source*). Ce faisant, l'État n'agit plus comme maître d’œuvre, ni même comme arbitre&nbsp;: c'est Microsoft qui se charge d'orchestrer (par les outils techniques choisis, et ce n'est jamais neutre) un marché de l'offre de services dont les acteurs sont les collectivités et administrations. De là, il est tout à fait possible d'imaginer une concurrence, par exemple entre des collectivités comme les mairies, entre celles qui auront une telle offre de services permettant d'attirer des contribuables et des entreprises sur son territoire, et celles qui resteront coincées dans les procédures administratives réputées archaïques. 


![Microsoft&nbsp;: contribuer à FranceConnect](imgs/782568710.png)



En se plaçant ainsi non plus en prestataire de produits mais en tuteur, Microsoft organise le marché de l'offre de service public numérique. Mais la firme va beaucoup plus loin, car elle bénéficie désormais d'une grande expérience, reconnue, en matière de service public. Ainsi, lorsqu'il s'agit d'anticiper les besoins et les changements, elle est non seulement à la pointe de l'expertise mais aussi fortement enracinée dans les processus de la décision publique. Sur le site *Econocom* en 2015, l'interview de Raphaël Mastier[@Mastier2015], directeur du pôle Santé de Microsoft France, est éloquent sur ce point. Partant du principe que «&nbsp;historiquement le numérique n'a pas été considéré comme stratégique dans le monde hospitalier&nbsp;», Microsoft propose des outils «&nbsp;d'analyse et de pilotage&nbsp;», et même l'utilisation de l'analyse prédictive des *big data* pour anticiper les temps d'attentes aux urgences&nbsp;: «&nbsp;grâce au *machine learning*, il sera possible de s'organiser beaucoup plus efficacement&nbsp;». Avec de tels arguments, en effet, qui irait à l'encontre de l'expérience microsoftienne dans les services publics si c'est un gage d'efficacité&nbsp;? on comprend mieux alors, dans le monde hospitalier, l'accord-cadre CAIH-Microsoft[@Berne2015] qui consolide durablement le marché Microsoft avec les hôpitaux.

Au-delà de ces exemples, on voit bien que cette nouvelle forme de gouvernance à la *Big Other* rend ces instruments légitimes car ils produisent le marché et donc l'organisation sociale. Cette transformation de l'État est parfaitement assumée par les autorités, arguant par exemple dans un billet sur *gouvernement.fr* intitulé «&nbsp;[Le numérique&nbsp;: instrument de la transformation de l'État](http://www.gouvernement.fr/action/le-numerique-instrument-de-la-transformation-de-l-etat)&nbsp;», en faveur de l'allégement des procédures, de la dématérialisation, de la mise à disposition des bases de données (qui va les valoriser&nbsp;?), etc. En somme autant d'arguments dont il est impossible de nier l'intérêt collectif et qui font, en règle générale, l'objet d'un consensus. Le groupe canadien CGI, l'un des leaders mondiaux en technologies et gestion de l'information, œuvre aussi en France, notamment [en partenariat avec l'UGAP](https://www.cgi.fr/news/CGI-UGAP-accelerent-transformation-numerique-Etat) (Union des Groupements d'Achats Publics). Sur son blog, dans un [Billet du 2 mai 2017](https://admin.fr.cgi.com/blog/secteurs/services-aux-citoyens-administration-simplification)[^lev317], CGI résume très bien le discours dominant de l'action publique dans ce domaine (et donc l'intérêt de son offre de services), en trois points&nbsp;:

1.  *Réduire les coûts*. Le sous-entendu consiste à affirmer que si l'État organise seul sa transformation numérique, le budget sera trop conséquent. Ce qui reste encore à prouver au vu des montants en jeu dans les accords de partenariat entre l'État et les firmes, et la nature des contrats (on peut souligner les clauses concernant les mises à jour chez Microsoft)&nbsp;;
2.  *Le secteur public accuse un retard numérique*. C'est l'argument qui justifie la délégation du numérique sur le marché, ainsi que l'urgence des décisions, et qui, par effet de bord, contrevient à la souveraineté numérique de l'État.
3.  *Il faut améliorer «&nbsp;l'expérience citoyen&nbsp;»*. C'est-à-dire que l'objectif est de transformer tous les citoyens en utilisateurs de services publics numériques et, comme on l'a vu plus haut, organiser une offre concurrentielle de services entre les institutions et les collectivités.

Du côté des décideurs publics, les choix et les décisions se justifient sur un mode Thatchérien (il n'y a pas d'alternative). Lorsqu'une alternative est proposée, tel le logiciel libre, tout le jeu consiste à donner une image politique positive pour ensuite orienter la stratégie différemment.

Sur ce point, l'exemple de Framasoft est éloquent et c'est quelque chose qui n'a pas forcément été perçu lors de la publication de la déclaration «&nbsp;Pourquoi Framasoft n'ira plus prendre le thé&hellip;&nbsp;»[@Framasoft2016]. Il s'agit de l'utilisation de l'alternative libriste pour légitimer l'appel à une offre concurrentielle sur le marché des firmes. En effet, les personnels de l'Éducation Nationale utilisent massivement les services que Framasoft propose dans le cadre de sa campagne «&nbsp;Degooglisons Internet&nbsp;». Or, l'institution pourrait très bien, sur le modèle promu par Framasoft, installer ces mêmes services, et ainsi offrir ces solutions pour un usage généralisé dans les écoles, collèges et lycées. C'est justement le but de la campagne de Framasoft que de proposer une vaste démonstration pour que des organisations retrouvent leur autonomie numérique. Les contacts que Framasoft a noué à ce propos avec différentes instances de l'Éducation Nationale se résumaient finalement soit à ce que Framasoft et ses bénévoles proposent un service à la carte dont l'ambition est bien loin d'une offre de service à l'échelle institutionnelle, soit participe à quelques comités d'expertise sur le numérique à l'école. L'idée sous-jacente est que l'Éducation Nationale ne peut faire autrement que de demander à des prestataires de mettre en place une offre numérique clé en main et onéreuse, alors même que Framasoft propose tous ses services au grand public avec des moyens financiers et humains ridiculement petits.

Dès lors, après la signature du partenariat entre le MEN et Microsoft, le message a été clairement formulé à Framasoft (et aux communautés du Libre en général), par un Tweet de la Ministre Najat Vallaud-Belkacem exprimant en substance la «&nbsp;neutralité technologique&nbsp;» du ministère (ce qui justifie donc le choix de Microsoft comme objectivement la meilleure offre du marché) et l'idée que les «&nbsp;éditeurs de logiciels libres&nbsp;» devraient proposer eux aussi leurs solutions, c'est-à-dire entrer sur le marché concurrentiel. Cette distorsion dans la compréhension de ce que sont les alternatives libres (non pas un produit mais un engagement) a été confirmée à plusieurs reprises par la suite&nbsp;: les solutions libres et leurs usages à l'Éducation Nationale peuvent être utilisées pour «&nbsp;mettre en tension&nbsp;» le marché et négocier des tarifs avec les firmes comme Microsoft, ou du moins servir d'épouvantail (dont on peut s'interroger sur l'efficacité réelle devant la puissance promotionnelle et lobbyiste des firmes en question).

On peut conclure de cette histoire que si la décision publique tient à ce point à discréditer les solutions alternatives qui échappent au marché des monopoles, c'est qu'une idéologie est à l’œuvre qui empêche toute forme d'initiative qui embarquerait le gouvernement dans une dynamique différente. Elle peut par exemple placer les décideurs devant une incapacité structurelle de choisir des alternatives proposant des logiciels libres, invoquant par exemple le droit des marchés publics voire la Constitution[@Beky2016], alors que l'exclusion du logiciel libre n'est pas réglementaire[@Rees2011].


![Ministre de l'Éducation Nationale et Microsoft](imgs/1650436815.png)



## L'idéologie de Silicon


En février 2017, quelques jours à peine après l'élection de Donald Trump à présidence des États-Unis, le PDG de Facebook, Mark Zuckerberg, publie sur son blog un manifeste[@Zuckerberg2017] remarquable à l'encontre de la politique isolationniste et réactionnaire du nouveau président. Il cite notamment tous les outils que Facebook déploie au service des utilisateurs et montre combien ils sont les vecteurs d'une grande communauté mondiale unie et solidaire. Tous les concepts de la cohésion sociale y passent, de la solidarité à la liberté de l'information, c'est-à-dire ce que le gouvernement est, aux yeux de Zuckerberg, incapable de garantir correctement à ses citoyens, et ce que les partisans de Trump en particulier menacent ouvertement.

Au moins, si les idées de Mark Zuckerberg semblent pertinentes aux yeux des détracteurs de Donald Trump, on peut néanmoins s'interroger sur l'idéologie à laquelle se rattache, de son côté, le PDG de Facebook. En réalité, pour lui, Donald Trump est la démonstration évidente que l'État ne devrait occuper ni l'espace social ni l'espace économique et que seul le marché et l'offre numérique sont en mesure d'intégrer les relations sociales.

Cette idéologie a déjà été illustrée par Fred Tuner, dans son ouvrage *Aux sources de l'utopie numérique*[@turner2012]. À propos de ce livre, j'écrivais (chapitre 1)&nbsp;:

> (...) Fred Turner montre comment les mouvements communautaires de contre-culture ont soit échoué par désillusion, soit se sont recentrés (surtout dans les années 1980) autour de techno-valeurs, en particulier portées par des leaders charismatiques géniaux à la manière de Steve Jobs un peu plus tard. L’idée dominante est que la revendication politique a échoué à bâtir un monde meilleur&nbsp;; c’est en apportant des solutions techniques que nous serons capables de résoudre nos problèmes.

Cette analyse un peu rapide passe sous silence la principale clé de lecture de Fred Tuner&nbsp;: l'émergence de nouveaux modes d'organisation économique du travail, en particulier le *freelance* et la collaboration en réseau. Comme je l'ai montré, le mouvement de la contre-culture californienne des années 1970 a permis la création de nouvelles pratiques d'échanges numériques utilisant les réseaux existants, comme le projet Community Memory, c'est-à-dire des utopies de solidarité, d'égalité et de liberté d'information dans une Amérique en proie au doute et à l'autoritarisme, notamment au sortir de la Guerre du Vietnam. Mais ce faisant, les années 1980, elles, ont développé à partir de ces idéaux la vision d'un monde où, en réaction à un État conservateur et disciplinaire, ce dernier se trouverait dépossédé de ses prérogatives de régulation, au profit de l'autonomie des citoyens dans leurs choix économiques et leurs coopérations. C'est l'avènement des principes du libertarisme grâce aux outils numériques. Et ce que montre Fred Turner, c'est que ce mouvement contre-culturel a ainsi *paradoxalement* préparé le terrain aux politiques libérales de dérégulation économique des années 1980-1990. C'est la volonté de réduire au strict minimum le rôle de l'État, garant des libertés individuelles, afin de permettre aux individus d'exercer leurs droits de propriété (sur leurs biens et sur eux-mêmes) dans un ordre social qui se définit uniquement comme un marché. À ce titre, pour ce qu'il est devenu, ce libertarisme est une résurgence radicale du libéralisme à la Hayek (la société démocratique libérale est un marché concurrentiel) doublé d'une conception utilitaire des individus et de leurs actions.

Néanmoins, tels ne sont pas exactement les principes du libertarisme, mais ceux-ci ayant cours dans une économie libérale, ils ne peuvent qu'aboutir à des modèles économiques basés sur une forme de collaboration dérégulée, anti-étatique, puisque la forme du marché, ici, consiste à dresser la liberté des échanges et de la propriété contre un État dont les principes du droit sont vécus comme arbitrairement interventionnistes. Les concepts tels la solidarité, l'égalité, la justice sont remplacés
par l'utilité, le choix, le droit.

Un exemple intéressant de ce renversement concernant le droit, est celui du droit de la concurrence appliqué à la question de la neutralité des plateformes, des réseaux, etc. Regardons les plateformes de service. Pourquoi assistons-nous à une forme de schizophrénie entre une Commission européenne pour qui la neutralité d'internet et des plateformes est une condition d'ouverture de l'économie numérique et la bataille contre cette même neutralité appliquée aux individus censés être libres de disposer de leurs données et les protéger, notamment grâce au chiffrement&nbsp;? Certes, les mesures de lutte contre le terrorisme justifient de s'interroger sur la pertinence d'une neutralité absolue (s'interroger seulement, car le chiffrement ne devrait jamais être remis en cause), mais la question est surtout de savoir quel est le rôle de l'État dans une économie numérique ouverte reposant sur la neutralité d'Internet et des plateformes. Dès lors, nous avons d'un côté la nécessité que l'État puisse intervenir sur la circulation de l'information dans un contexte de saisie juridique et de l'autre celle d'une volontaire absence du Droit dans le marché numérique.

Pour preuve, on peut citer le président de l'Autorité de la concurrence en France, Bruno Lassere, auditionné à l'Assemblée Nationale le 7 juillet 2015[@assnat2015]. Ce dernier cite le Droit de la Concurrence et ses applications comme un instrument de lutte contre les distorsions du marché, comme les monopoles à l'image de Google/Alphabet. Mais d'un autre côté, le Droit de la Concurrence est surtout vu comme une solution d'auto-régulation dans le contexte de la neutralité des plates-formes&nbsp;:

> (...) Les entreprises peuvent prendre des engagements par lesquels elles remédient elles-mêmes à certains dysfonctionnements. *Il me semble important que certains abus soient corrigés à l’intérieur du marché et non pas forcément sur intervention législative ou régulatrice*. C’est ainsi que Booking, Expedia et HRS se sont engagées à lever la plupart des clauses de parité tarifaire qui interdisent une véritable mise en compétition de ces plateformes de réservation hôtelières. Comment fonctionnent ces clauses&nbsp;? Si un hôtel propose à Booking douze nuitées au prix de 100 euros la chambre, il ne peut offrir de meilleures conditions —&nbsp;en disponibilité ou en tarif&nbsp;— aux autres plateformes. Il ne peut pas non plus pratiquer un prix différent à ses clients directs. Les engagements signés pour lever ces contraintes sont gagnants-gagnants&nbsp;: ils respectent le modèle économique des plateformes, et donc l’incitation à investir et à innover, tout en rétablissant plus de liberté de négociation. Les hôtels pourront désormais mettre les plateformes en concurrence.

Sur ce point, il ne faut pas s'interroger sur le mécanisme de concurrence qu'il s'agit de promouvoir mais sur l'implication d'une régulation systématique de l'économie numérique par le Droit de la Concurrence. Ainsi le rapport *Numérique et libertés* présenté Christian Paul et Christiane Féral-Schuhl, propose un long développement sur la question des données personnelles mais cite cette partie de l'audition de Bruno Lasserre à propos du Droit de la Concurrence sans revenir sur la conception selon laquelle l'alpha et l'omega du Droit consiste à aménager un environnement concurrentiel «&nbsp;sain&nbsp;» à l'intérieur duquel les mécanismes de concurrence suffisent à eux-seuls à appliquer des principes de loyauté, d'équité ou d'égalité.

Cette absence de questionnement politique sur le rôle du Droit dans un marché où la concentration des services abouti à des monopoles finit par produire immanquablement une forme d'autonomie absolue de ces monopoles dans les mécanismes concurrentiels, entre une concurrence acceptable et une concurrence non-souhaitable. Tel est par exemple l'objet de multiples pactes passés entre les grandes multinationales du numérique, ainsi entre Microsoft et AOL[@lesechos2003], entre AOL / Yahoo et Microsoft[@ZDNet2011], entre Intertrust et Microsoft[@Thorel2004], entre Apple et Google[@Ames2014] (pacte géant), entre Microsoft et Android[@Ensel2015], l'accord entre IBM et Apple[@Marti1991] en 1991 qui a lancé une autre vague d'accords du côté de Microsoft tout en définissant finalement l'informatique des années 1990, etc.

La liste de tels accords peut donner le tournis à n'importe quel juriste au vu de leurs implications en termes de Droit, surtout lorsqu'ils sont déclinés à de multiples niveaux nationaux. L'essentiel est de retenir que ce sont ces accords entre monopoles qui définissent non seulement le marché mais aussi toutes nos relations avec le numérique, à tel point que c'est sur le même modèle qu'agit le politique aujourd'hui.

Ainsi, face à la puissance des GAFAM et consorts, les gouvernements se placent en situation de demandeurs. Pour prendre un exemple récent, à propos de la lutte anti-terroriste en France, le gouvernement ne fait pas que déléguer une partie de ses prérogatives (qui pourraient consister à mettre en place lui-même un système anti-propagande efficace), mais se repose sur la bonne volonté des Géants, comme c'est le cas de l'accord avec Google, Facebook, Microsoft et Twitter, conclu par le Ministre Bernard Cazeneuve, se rendant lui-même en Californie en février 2015[@LeParisien2015]. On peut citer, dans un autre registre, celui de la maîtrise des coûts, l'accord-cadre CAIH-Microsoft[@Berne2015] cité plus haut, qui finalement ne fait qu'entériner la mainmise de Microsoft sur l'organisation hospitalière, et par extension à de multiples secteurs de la santé.

Certes, on peut arguer que ce type d'accord entre un gouvernement et des firmes est nécessaire dans la mesure où ce sont les opérateurs les mieux placés pour contribuer à une surveillance efficace des réseaux ou modéliser les échanges d'information. Cependant, on note aussi que de tels accords relèvent du principe de transfert du pouvoir du politique aux acteurs numériques. Tel est la thèse que synthétise Mark Zuckerberg dans son plaidoyer de février 2017. Elle est acceptée à de multiples niveaux de la décision et de l'action publique.

C'est par une analyse du rôle et de l'emploi du Droit aujourd'hui, en particulier dans ce contexte où ce sont les firmes qui définissent le droit (par exemple à travers leurs accords de loyauté) que Alain Supiot démontre comment le gouvernement par les nombres, c'est-à-dire ce mode de gouvernement par le marché (celui des instruments, de l'expertise, de la mesure et du contrôle) et non plus par le Droit, est en fait l'avènement du *Big Other* de Shoshanna Zuboff, c'est-à-dire un monde où ce n'est plus le Droit qui règle l'organisation sociale, mais c'est le contrat entre les individus et les différentes offres du marché. Alain Supiot l'exprime en deux phrases[@Supiot2015]&nbsp;:


> Référée à un nouvel objet fétiche —&nbsp;non plus l’horloge, mais l’ordinateur&nbsp;—, la gouvernance par les nombres vise à établir un ordre qui serait capable de s’autoréguler, rendant superflue toute référence à des lois qui le surplomberaient. Un ordre peuplé de particules contractantes et régi par le calcul d’utilité, tel est l’avenir radieux promis par l’ultralibéralisme, tout entier fondé sur ce que Karl Polanyi a appelé le solipsisme économique.

Le rêve de Mark Zuckerberg et, avec lui, les grands monopoles du numérique, c'est de pouvoir considérer l'État lui-même comme un opérateur économique. C'est aussi ce que les tenants *new public management* défendent&nbsp;: appliquer à la gestion de l'État les mêmes règles que l'économie privée. De cette manière, ce sont les acteurs privés qui peuvent alors prendre en charge ce qui était du domaine de l'incalculable, c'est-à-dire ce que le débat politique est normalement censé orienter mais qui finit par être approprié par des mécanismes privés&nbsp;: la protection de l'environnement, la gestion de l'état-civil, l'organisation de la santé, la lutte contre le terrorisme, la régulation du travail, etc.



## Conclusion&nbsp;: l'État est-il soluble dans les GAFAM&nbsp;?


Nous ne perdons pas seulement notre souveraineté numérique mais nous changeons de souveraineté. Pour appréhender ce changement, on ne peut pas se limiter à pointer les monopoles, les effets de la concentration des services numériques et l'exploitation des *big data*. Il faut aussi se questionner sur la réception de l'idéologie issue à la fois de l'ultra-libéralisme et du renversement social qu'impliquent les techniques numériques à l'épreuve du politique. Le terrain favorable à ce renversement est depuis longtemps prêt, c'est l'avènement de la gouvernance par les instruments (par les nombres, pour reprendre Alain Supiot). Dès lors que la décision publique est remplacée par la technique, cette dernière est soumise à une certaine idéologie du progrès, celle construite par les firmes et structurée par leur marché.

Qu'on ne s'y méprenne pas&nbsp;: la transformation progressive de la gouvernance et cette idéologie-silicone sont l'objet d'une convergence plus que d'un enchaînement logique et intentionnel. La convergence a des causes multiples, de la crise financière en passant par la formation des décideurs, les conjonctures politiques… autant de potentielles opportunités par lesquelles des besoins nouveaux structurels et sociaux sont nés sans pour autant trouver dans la décision publique de quoi les combler, si bien que l'ingéniosité des GAFAM a su configurer un marché où les solutions s'imposent d'elles-mêmes, par nécessité.

Le constat est particulièrement sombre. Reste-t-il malgré tout une possibilité à la fois politique et technologique capable de contrer ce renversement&nbsp;? Elle réside évidemment dans le modèle du logiciel libre. Premièrement parce qu'il renoue technique et Droit (par le droit des licences, avant tout), établit des chaînes de confiance là où seules des procédures régulent les contrats, ne construit pas une communauté mondiale uniforme mais des groupes sociaux en interaction impliqués dans des processus de décision, induit une diversité numérique et de nouveaux équilibres juridiques. Deuxièmement parce qu'il suppose des apprentissages à la fois techniques et politiques et qu'il est possible par l'éducation populaire de diffuser les pratiques et les connaissances pour qu'elles s'imposent à leur tour non pas sur le marché mais sur l'économie, non pas sur la gouvernance mais dans le débat public.


# La surveillance qui vient

Dans ce quatrième chapitre, nous allons voir dans quelle mesure le modèle économique a développé son besoin vital de la captation des données relatives à la vie privée. De fait, nous vivons dans le même scénario dystopique depuis une cinquantaine d'années. Nous verrons comment les critiques de l'économie de la surveillance sont redondantes depuis tout ce temps et que, au-delà des craintes, le temps est à l'action d'urgence.

---

Avons-nous vraiment besoin des utopies et des dystopies pour anticiper les rêves et les cauchemars des technologies appliquées aux comportements humains&nbsp;? Sempiternellement rabâchés, le *Meilleur de mondes* et *1984* sont sans doute les romans les plus vendus parmi les best-sellers des dernières années. Il existe un effet pervers des utopies et des dystopies, lorsqu'on les emploie pour justifier des arguments sur ce que devrait être ou non la société&nbsp;: tout argument qui les emploie afin de prescrire ce qui devrait être, se trouve à un moment ou à un autre face au mur du réel sans possibilité de justifier un mécanisme crédible qui causerait le basculement social vers la fiction libératrice ou la fiction contraignante. C'est la raison pour laquelle l'île de Thomas More se trouve partout et nulle part, elle est utopique, en aucun lieu. Utopie et dystopie sont des propositions d'expérience et n'ont en soi aucune vocation à prouver ou prédire quoi que ce soit bien qu'elles partent presque toujours de l'expérience commune et dont tout l'intérêt, en particulier en littérature, figure dans le troublant cheminement des faits, plus ou moins perceptible, du réel vers l'imaginaire.

Pourtant, lorsqu'on se penche sur le phénomène de l'exploitation des données personnelles à grande échelle par des firmes à la puissance financière inégalable, c'est la dystopie qui vient à l'esprit. Bien souvent, au gré des articles journalistiques pointant du doigt les dernières frasques des GAFAM dans le domaine de la protection des données personnelles, les discussions vont bon train&nbsp;: «&nbsp;ils savent tout de nous&nbsp;», «&nbsp;nous ne sommes plus libres&nbsp;», «&nbsp;c'est Georges Orwell&nbsp;», «&nbsp;on nous prépare le meilleur des mondes&nbsp;». En somme, c'est l'angoisse&nbsp; pendant quelques minutes, juste le temps de vérifier une nouvelle fois si l'application Google de notre smartphone a bien enregistré l'adresse du rendez-vous noté la veille dans l'agenda. 

Un petit coup d'angoisse&nbsp;? allez&hellip; que diriez-vous si vos activités sur les réseaux sociaux, les sites d'information et les sites commerciaux étaient surveillées et quantifiées de telle manière qu'un système de notation et de récompense pouvait vous permettre d'accéder à certains droits, à des prêts bancaires, à des autorisations officielles, au logement, à des libertés de circulation, etc. Pas besoin de science-fiction. Ainsi que le rapportait *Wired* en octobre 2017[@botsman2017], la Chine a déjà tout prévu d'ici 2020, c'est-à-dire demain. Il s'agit, dans le contexte d'un Internet déjà ultra-surveillé et non-neutre, d'établir un système de crédit social[@hamet2017] en utilisant les *big data* sur des millions de citoyens et effectuer un traitement qui permettra de catégoriser les individus, quels que soient les risques&nbsp;: risques d'erreurs, risques de piratage, crédibilité des indicateurs, atteinte à la liberté d'expression, etc. 

Évidemment les géants chinois du numérique comme Alibaba et sa filiale de crédit sont déjà sur le coup. Mais il y a deux choses troublantes dans cette histoire. La première c'est que le crédit social existe déjà et partout&nbsp;: depuis des années on évalue en ligne les restaurants et les hôtels sans se priver de critiquer les tenanciers et il existe toute une économie de la notation dans l’hôtellerie et la restauration, des applications terrifiantes comme Peeple[@gendron2015] existent depuis 2015, les banques tiennent depuis longtemps des listes de créanciers, les fournisseurs d'énergie tiennent à jour les historiques des mauvais payeurs, etc.&nbsp; Ce que va faire la Chine, c'est le rêve des firmes, c'est la possibilité à une gigantesque échelle et dans un cadre maîtrisé (un Internet non-neutre) de centraliser des millions de gigabits de données personnelles et la possibilité de recouper ces informations auparavant éparses pour en tirer des profils sur lesquels baser des décisions.

Le second élément troublant, c'est que le gouvernement chinois n'aurait jamais eu cette idée si la technologie n'était pas déjà à l’œuvre et éprouvée par des grandes firmes. Le fait est que pour traiter autant d'informations par des algorithmes complexes, il faut&nbsp;: de grandes banques de données, beaucoup d'argent pour investir dans des serveurs et dans des compétences, et espérer un retour sur investissement de telle sorte que plus vos secteurs d'activités sont variés plus vous pouvez inférer des profils et plus votre marketing est efficace. Il est important aujourd'hui pour des monopoles mondialisés de savoir combien vous avez de chance d'acheter à trois jours d'intervalle une tondeuse à gazon et un canard en mousse. Le profilage de la clientèle (et des utilisateurs en général) est devenu l'élément central du marché à tel point que notre économie est devenue une économie de la surveillance, repoussant toujours plus loin les limites de l'analyse de nos vies privées.

La dystopie est en marche, et si nous pensons bien souvent au cauchemar orwellien lorsque nous apprenons l'existence de projets comme celui du gouvernement chinois, c'est parce que nous n'avons pas tous les éléments en main pour en comprendre le cheminement. Nous anticipons la dystopie mais trop souvent, nous n'avons pas les moyens de déconstruire ses mécanismes. Pourtant, il devient de plus en plus facile de montrer ces mécanismes sans faire appel à l'imaginaire&nbsp;: toutes les conditions sont remplies pour n'avoir besoin de tracer que quelques scénarios alternatifs et peu différents les uns des autres. Le traitement et l'analyse de nos vies privées provient d'un besoin, celui de maximiser les profits dans une économie qui favorise l'émergence des monopoles et la centralisation de l'information. Cela se retrouve à tous les niveaux de l'économie, à commencer par l'activité principale des géants du Net&nbsp;: le démarchage publicitaire. Comprendre ces modèles économiques revient aussi à comprendre les enjeux de l'économie de la surveillance.

## Données personnelles&nbsp;: le commerce en a besoin

Dans le petit monde des études en commerce et marketing, Frederick Reichheld fait figure de référence. Son nom et ses publications dont au moins deux *best sellers*, ne sont pas vraiment connus du grand public, en revanche la plupart des stratégies marketing des vingt dernières années sont fondées, inspirées et même modélisées à partir de son approche théorique de la relation entre la firme et le client. Sa principale clé de lecture est une notion, celle de la *fidélité* du client. D'un point de vue opérationnel cette notion est déclinée en un concept, celui de la valeur vie client (*customer lifetime value*) qui se mesure à l'aune de profits réalisés durant le temps de cette relation entre le client et la firme. Pour Reichheld, la principale activité du marketing consiste à optimiser cette valeur vie client. Cette optimisation s'oppose à une conception rétrograde (et qui n'a jamais vraiment existé, en fait[^lev43]) de la «&nbsp;simple&nbsp;» relation marchande. 

[^lev43]: Elle n'a jamais existé dans le capitalisme du<span style="font-variant:small-caps;">xx</span>^e^ siècle, et depuis l'invention des relations publiques (qu'on appellera marketing). Il faut lire sur ce point Edward Bernays, *Propaganda* (1928).

En effet, pour bien mener les affaires, la relation avec le client ne doit pas seulement être une série de transactions marchandes, avec plus ou moins de satisfaction à la clé. Cette manière de concevoir les modèles économiques, qui repose uniquement sur l'idée qu'un client satisfait est un client fidèle, a son propre biais&nbsp;: on se contente de donner de la satisfaction. Dès lors, on se place d'un point de vue concurrentiel sur une conception du capitalisme marchand déjà ancienne. Le modèle de la concurrence «&nbsp;non faussée&nbsp;» est une conception nostalgique (fantasmée) d'une relation entre firme et client qui repose sur la rationalité de ce dernier et la capacité des firmes à produire des biens en réponse à des besoins plus ou moins satisfaits. Dès lors la décision du client, son libre arbitre, serait la variable juste d'une économie auto-régulée (la main invisible) et la croissance économique reposerait sur une dynamique de concurrence et d'innovation, en somme, la promesse du «&nbsp;progrès&nbsp;».

Évidemment, cela ne fonctionne pas ainsi. Il est bien plus rentable pour une entreprise de fidéliser ses clients que d'en chercher de nouveaux. Prospecter coûte cher alors qu'il est possible de jouer sur des variables à partir de l'existant, tout particulièrement lorsqu'on exerce un monopole (et on comprend ainsi pourquoi les monopoles s'accommodent très bien entre eux en se partageant des secteurs)&nbsp;: 

1. on peut résumer la conception de Reichheld à partir de son premier *best seller*, *The Loyalty Effect*[@reichheldloyalty1996]&nbsp;: avoir des clients fidèles, des employés fidèles et des propriétaires loyaux. Il n'y a pas de *main invisible*&nbsp;: tout repose sur *a)* le rapport entre hausse de la rétention des clients / hausse des dépenses, *b)* l'insensibilité aux prix rendue possible par la fidélisation (un client fidèle, ayant dépassé le stade du risque de défection, est capable de dépenser davantage pour des raisons qui n'ont rien à voir avec la valeur marchande), *c)* la diminution des coûts de maintenance (fidélisation des employés et adhésion au *story telling*), *d)* la hausse des rendements et des bénéfices. En la matière la firme Apple rassemble tous ces éléments à la limite de la caricature.
2. Reichheld est aussi le créateur d'un instrument d'évaluation de la fidélisation&nbsp;: le [NPS](https://en.wikipedia.org/wiki/Net_Promoter) (*Net Promoter Score*). Il consiste essentiellement à catégoriser les clients, entre promoteurs, détracteurs ou passifs. Son autre best-seller *The Ultimate Question 2.0: How Net Promoter Companies Thrive in a Customer-Driven World*[@reichheldultimate2011] déploie les applications possibles du contrôle de la qualité des relations client qui devient dès lors la principale stratégie de la firme d'où découlent toutes les autres stratégies (en particulier les choix d'innovation). Ainsi il cite dans son ouvrage les plus gros scores NPS détenus par des firmes comme Apple, Amazon et Costco.

Il ne faut pas sous-estimer la valeur opérationnelle du NPS. Notamment parce qu'il permet de justifier les choix stratégiques. Dans *The Ultimate Question 2.0* Reichheld fait référence à une étude de Bain & Co. qui montre que pour une banque, la valeur vie client d'un promoteur (au sens NPS) est estimée en moyenne à 9&nbsp;500 dollars. Ce modèle aujourd'hui est une illustration de l'importance de la surveillance et du rôle prépondérant de l'analyse de données. En effet, plus la catégorisation des clients est fine, plus il est possible de déterminer les leviers de fidélisation. Cela passe évidemment par un système de surveillance à de multiples niveaux, à la fois internes et externes&nbsp;:

- surveiller des opérations de l'organisation pour les rendre plus agiles et surveiller des employés pour augmenter la qualité des relations client, 
- rassembler le plus de données possibles sur les comportements des clients et les possibilités de déterminer leurs choix à l'avance.

Savoir si cette approche du marketing est née d'un nouveau contexte économique ou si au contraire ce sont les approches de la valeur vie client qui ont configuré l'économie d'aujourd'hui, c'est se heurter à l'éternel problème de l’œuf et de la poule. Toujours est-il que les stratégies de croissance et de rentabilité de l'économie reposent sur l'acquisition et l'exploitation des données personnelles de manière à manipuler les processus de décision des individus (ou plutôt des groupes d'individus) de manière à orienter les comportements et fixer des prix non en rapport avec la valeur des biens mais en rapport avec ce que les consommateurs (ou même les acheteurs en général car tout ne se réduit pas à la question des seuls biens de consommation et des services) sont à même de pouvoir supporter selon la catégorie à laquelle ils appartiennent.

Le fait de catégoriser ainsi les comportements et les influencer, comme nous l'avons vu dans les épisodes précédents de la série Leviathans, est une marque stratégique de ce que Shoshana Zuboff a appelé le *capitalisme de surveillance*[@zuboff_secrets_2106]. Les entreprises ont aujourd'hui un besoin vital de rassembler les données personnelles dans des silos de données toujours plus immenses et d'exploiter ces *big data* de manière à optimiser leurs modèles économiques. Ainsi, du point de vue des individus, c'est le quotidien qui est scruté et analysé de telle manière que, il y a à peine une dizaine d'années, nous étions à mille lieues de penser l'extrême granularité des données qui cartographient et catégorisent nos comportements. Tel est l'objet du récent rapport publié par Cracked Lab Corporate surveillance in everyday life[@Christl2017] qui montre à quel point tous les aspects du quotidien font l'objet d'une surveillance à une échelle quasi-industrielle (on peut citer les activités de l'entreprise Acxiom), faisant des données personnelles un marché dont la matière première est traitée sans aucun consentement des individus. En effet, tout le savoir-faire repose essentiellement sur le recoupement statistique et la possibilité de catégoriser des milliards d'utilisateurs de manière à produire des représentations sociales dont les caractéristiques ne reflètent pas la réalité mais les comportements futurs. Ainsi par exemple les secteurs bancaires et des assurances sont particulièrement friands des possibilités offertes par le pistage numérique et l'analyse de solvabilité. 

Cette surveillance a été caractérisée déjà [en 1988](http://www.rogerclarke.com/DV/CACM88.html) par le chercheur en systèmes d'information Roger Clarke[@Clarke1988]&nbsp;:

- dans la mesure où il s'agit d'automatiser, par des algorithmes, le traitement des informations personnelles dans un réseau regroupant plusieurs sources, et d'en inférer du sens, on peut la qualifier de «&nbsp;dataveillance&nbsp;», c'est à dire «&nbsp;l'utilisation systématique de systèmes de traitement de données à caractère personnel dans l'enquête ou le suivi des actions ou des communications d'une ou de plusieurs personnes&nbsp;»&nbsp;;
- l'un des attributs fondamentaux de cette *dataveillance* est que les intentions et les mécanismes sont cachés aux sujets qui font l'objet de la surveillance. 

En effet, l'accès des sujets à leurs données personnelles et leurs traitements doit rester quasiment impossible car après un temps très court de captation et de rétention, l'effet de recoupement fait croître de manière exponentielle la somme d'information sur les sujets et les résultats, qu'ils soient erronés ou non, sont imbriqués dans le profilage et la catégorisation de groupes d'individus. Plus le monopole a des secteurs d'activité différents, plus les comportements des mêmes sujets vont pouvoir être quantifiés et analysés à des fins prédictives. C'est pourquoi la dépendance des firmes à ces informations est capitale[@Clarke2017]&nbsp;:

> «&nbsp;L'économie de la surveillance numérique est cette combinaison d'institutions, de relations institutionnelles et de processus qui permet aux entreprises d'exploiter les données issues de la surveillance du comportement électronique des personnes et dont les sociétés de marketing deviennent rapidement dépendantes.&nbsp;»


Le principal biais que produit cette économie de la surveillance (pour S. Zuboff, c'est de capitalisme de surveillance qu'il s'agit puisqu'elle intègre une relation d'interdépendance entre centralisation des données et centralisation des capitaux) est qu'elle n'a plus rien d'une démarche descriptive mais devient prédictive par effet de prescription.

Elle n'est plus descriptive (mais l'a-t-elle jamais été&nbsp;?) parce qu'elle ne cherche pas à comprendre les comportements économiques en fonction d'un contexte, mais elle cherche à anticiper les comportements en maximisant les indices comportementaux. On ne part plus d'un environnement économique pour comprendre comment le consommateur évolue dedans, on part de l'individu pour l'assigner à un environnement économique sur mesure dans l'intérêt de la firme. Ainsi, comme l'a montré une étude de *Propublica* en 2016[@Angwin2016], Facebook dispose d'un panel de pas moins de 52&nbsp;000 indicateurs de profilage individuels pour en établir une classification générale. Cette quantification ne permet plus seulement, comme dans une approche statistique classique, de déterminer par exemple si telle catégorie d'individus est susceptible d'acheter une voiture. Elle permet de déterminer, de la manière la plus intime possible, quelle valeur économique une firme peut accorder à un panel d'individus au détriment des autres, leur valeur vie client.

Tout l'enjeu consiste à savoir comment influencer ces facteurs et c'est en cela que l'exploitation des données passe d'une dimension prédictive à une dimension prescriptive. Pour prendre encore l'exemple de Facebook, cette firme a breveté un système capable de déterminer la solvabilité bancaire[@Sullivan2015] des individus en fonction de la solvabilité moyenne de leur réseau de contacts[^lev49]. L'important ici, n'est pas vraiment d'aider les banques à diminuer les risques d'insolvabilité de leurs clients, car elles savent très bien le faire toutes seules et avec les mêmes procédés d'analyse en *big data*. En fait, il s'agit d'influencer les stratégies personnelles des individus par le seul effet [panoptique](https://fr.wikipedia.org/wiki/Panoptique)[^lev410]&nbsp;: si les individus savent qu'ils sont surveillés, toute la stratégie individuelle consistera à choisir ses amis Facebook en fonction de leur capacité à maximiser les chances d'accéder à un prêt bancaire (et cela peut fonctionner pour bien d'autres objectifs). L'intérêt de Facebook n'est pas d'aider les banques, ni de vendre une expertise en statistique (ce n'est pas le métier de Facebook) mais de normaliser les comportements dans l'intérêt économique et augmenter la valeur vie client potentielle de ses utilisateurs&nbsp;: si vous avez des problèmes d'argent, Facebook n'est pas fait pour vous. Dès lors il suffit ensuite de revendre des profils sur-mesure à des banques. On se retrouve typiquement dans un épisode d'anticipation de la série Black Mirror (*Chute libre*)[^lev411].

[^lev411]: Charlie <span style="font-variant:small-caps;">Brooker</span> (réal.), *Black Mirror*, série télévisée. Saison 3, épisode 1 «&nbsp;Chute Libre&nbsp;». 2011. [Lien](https://fr.wikipedia.org/wiki/Épisodes_de_Black_Mirror#Épisode_1_:_Chute_libre).

La fiction, l'anticipation, la dystopie&hellip; finalement, c'est-ce pas un biais que de toujours analyser sous cet angle l'économie de la surveillance et le rôle des algorithmes dans notre quotidien&nbsp;? Tout se passe en quelque sorte comme si nous découvrions un nouveau modèle économique, celui dont nous venons de montrer que les préceptes sont déjà anciens, et comme si nous appréhendions seulement aujourd'hui les enjeux de la captation et l'exploitation des données personnelles. Au risque de décevoir tous ceux qui pensent que questionner la confiance envers les GAFAM est une activité d'avant-garde, la démarche a été initiée dès les prémices de la révolution informatique.



## La vie privée à l'époque des pattes d'eph.

Face au constat selon lequel nous vivons dans un environnement où la surveillance fait loi, de nombreux ouvrages, articles de presse et autres témoignages ont sonné l'alarme. En décembre 2017, ce fut le soi-disant repentir de Chamath Palihapitya, ancien vice-président de Facebook, qui affirmait avoir contribué à créer «&nbsp;des outils qui déchirent le tissu social&nbsp;»[@Schneidermann2017]. Il ressort de cette lecture qu'après plusieurs décennies de centralisation et d'exploitation des données personnelles par des acteurs économiques ou institutionnels, nous n'avons pas fini d'être surpris par les transformations sociales qu'impliquent les *big data*. Là où, effectivement, nous pouvons accorder un tant soit peu de de crédit à C. Palihapitya, c'est dans le fait que l'extraction et l'exploitation des données personnelles implique une économie de la surveillance qui modèle la société sur son modèle économique. Et dans ce modèle, l'exercice de certains droits (comme le droit à la vie privée) passe d'un état absolu (un droit de l'homme) à un état relatif (au contexte économique).

Comme cela devient une habitude dans cette série des Léviathans, nous pouvons effectuer un rapide retour dans le temps et dans l'espace. Situons-nous à la veille des années 1970, aux États-Unis, plus exactement dans la période charnière qui vit la production en masse des ordinateurs *mainframe* (du type IBM 360),  à destination non plus des grands laboratoires de recherche et de l'aéronautique, mais vers les entreprises des secteurs de la banque, des assurances et aussi vers les institutions gouvernementales. L'objectif premier de tels investissements (encore bien coûteux à cette époque) était le traitement des données personnelles des citoyens ou des clients.

Comme bien des fois en histoire, il existe des périodes assez courtes où l'on peut comprendre les événements non pas parce qu'ils se produisent suivant un enchaînement logique et linéaire, mais parce qu'ils surviennent de manière quasi-simultanée comme des fruits de l'esprit du temps. Ainsi nous avons d'un côté l'émergence d'une industrie de la donnée personnelle, et, de l'autre l'apparition de nombreuses publications portant sur les enjeux de la vie privée. D'aucuns pourraient penser que, après la publication en 1949 du grand roman de G. Orwell, *1984*, la dystopie orwellienne pouvait devenir la clé de lecture privilégiée de l'*informationnalisation* (pour reprendre le terme de S. Zuboff) de la société américaine dans les années 1960-1970. Ce fut effectivement le cas&hellip; plus exactement, si les références à Orwell sont assez courantes dans la littérature de l'époque[^lev413], il y avait deux lectures possibles de la vie privée dans une société aussi bouleversée que celle de l'Amérique des années 1960. La première questionnait la hiérarchie entre vie privée et vie publique. La seconde focalisait sur le traitement des données informatiques. Pour mieux comprendre l'état d'esprit de cette période, il faut brosser quelques références.

[^lev413]: Les exemples ne manquent pas. La rhétorique habituellement rencontrée alors consiste à rappeler la dystopie sur un mode alarmiste puis à développer un argumentaire qui tend à démontrer les dangers de la surveillance sans pour autant revenir à Orwell dont la pertinence pour une analyse factuelle est limitée. Par exemple, une référence à Orwell ouvre le premier chapitre du livre de James B. Rule, *Social control and modern social structure, Private Lives and Public Surveillance*[@Rule1974]. Nous donnons un autre exemple plus bas dans une citation de Arthur R. Miller.


### Vie privée vs vie publique

Deux best-sellers parus en été 1964 effectuent un travail introspectif sur la société américaine et son rapport à la vie privée. Le premier, écrit par Myron Brenton, s'intitule *The privacy invaders*[@Brenton1964]. Brenton est un ancien détective privé qui dresse un inventaire des techniques de surveillance à l'encontre des citoyens et du droit. Le second livre, écrit par Vance Packard, connut un succès international. Il s'intitule *The naked Society*[@Packard1964], traduit en français un an plus tard sous le titre *Une société sans défense*[@Packard1965]. V. Packard est alors universitaire, chercheur en sociologie et économie. Il est connu pour avoir surtout travaillé sur la société de consommation et le marketing et dénoncé, dans un autre ouvrage (*La persuasion clandestine*[@Packard1958]), les abus des publicitaires en matière de manipulation mentale. Dans *The naked Society* comme dans *The privacy invaders* les mêmes thèmes sont déployés à propos des dispositifs de surveillance, entre les techniques d'enquêtes des banques sur leurs clients débiteurs, les écoutes téléphoniques, la surveillance audio et vidéo des employés sur les chaînes de montage, en somme toutes les stratégies privées ou publiques d'espionnage des individus et d'abus en tout genre qui sont autant d'atteintes à la vie privée. Il faut dire que la société américaine des années 1960 a vu aussi bien arriver sur le marché des biens de consommation le téléphone et la voiture à crédit mais aussi l'électronique et la miniaturisation croissante des dispositifs utiles dans ce genre d'activité. Or, les questions que soulignent Brenton et Packard, à travers de nombreux exemples, ne sont pas tant celles, plus ou moins spectaculaires, de la mise en œuvre, mais celles liées au droit des individus face à des puissances en recherche de données sur la vie privée extorquées aux sujets mêmes. En somme, ce que découvrent les lecteurs de ces ouvrages, c'est que la vie privée est une notion malléable, dans la réalité comme en droit, et qu'une bonne part de cette malléabilité est relative aux technologies et au médias. Packard ira légèrement plus loin sur l'aspect tragique de la société américaine en focalisant plus explicitement sur le respect de la vie privée dans le contexte des médias et de la presse à sensation et dans les contradictions apparente entre le droit à l'information, le droit à la vie privée et le Sixième Amendement. De là, il tire une sonnette d'alarme en se référant à Georges Orwell, et dénonçant l'effet panoptique  obtenu par l'accessibilité des instruments de surveillance, la généralisation de leur emploi dans le quotidien, y compris pour les besoins du marketing, et leur impact culturel.

En réalité, si ces ouvrages connurent un grand succès, c'est parce que leur approche de la vie privée reposait sur un questionnement des pratiques à partir de la morale et du droit, c'est-à-dire sur ce que, dans une société, on est prêt à admettre ou non au sujet de l'intimité vue comme une part structurelle des relations sociales. Qu'est-ce qui relève de ma vie privée et qu'est-ce qui relève de la vie publique&nbsp;? Que puis-je exposer sans crainte selon mes convictions, ma position sociale, la classe à laquelle j'appartiens, etc. Quelle est la légitimité de la surveillance des employés dans une usine, d'un couple dans une chambre d'hôtel, d'une star du show-biz dans sa villa&nbsp;?

Il reste que cette approche manqua la grande révolution informatique naissante et son rapport à la vie privée non plus conçue comme l'image et l'estime de soi, mais comme un ensemble d'informations quantifiables à grande échelle et dont l'analyse peut devenir le mobile de décisions qui impactent la société en entier[^lev417]. La révolution informatique relègue finalement la légitimité de la surveillance au second plan car la surveillance est alors conçue de manière non plus intentionnelle mais comme une série de faits&nbsp;: les données fournies par les sujets, auparavant dans un contexte fermé comme celui de la banque locale, finirent par se retrouver centralisées et croisées au gré des consortiums utilisant l'informatique pour traiter les données des clients. Le même schéma se retrouva pour ce qui concerne les institutions publiques dans le contexte fédéral américain.

[^lev417]: Non pas que Packard ait fait une totale impasse sur le traitement informatique des données, mais parce qu'il n'en voit pas le danger systémique.


### Vie privée vs ordinateurs

Une autre approche commença alors à faire son apparition dans la sphère universitaire. Elle intervient dans la seconde moitié des années 1960. Il s'agissait de se pencher sur la gouvernance des rapports entre la vie privée et l'administration des données personnelles. Suivant au plus près les nouvelles pratiques des grands acteurs économiques et gouvernementaux, les universitaires étudièrent les enjeux de la numérisation des données personnelles avec en arrière-plan les préoccupations juridiques, comme celle de V. Packard, qui faisaient l'objet des réflexions de la décennie qui se terminait. Si, avec la société de consommation venait tout un lot de dangers sur la vie privée,  cette dernière devrait être protégée, mais il fallait encore savoir sur quels plans agir. Le début des années 1970, en guise de résultat de ce *brainstorming* général, marquèrent alors une nouvelle ère de la *privacy* à l'Américaine à l'âge de l'informatisation et du réseautage des données personnelles. Il s'agissait de comprendre qu'un changement majeur était en train de s'effectuer avec les grands ordinateurs en réseau et qu'il fallait formaliser dans le droit les garde-fou les plus pertinents&nbsp;: on passait d'un monde où la vie privée pouvait faire l'objet d'une intrusion par des acteurs séparés, recueillant des informations pour leur propre compte en fonction d'objectifs différents, à un monde où les données éparses étaient désormais centralisées, avec des machines capables de traiter les informations de manière rapide et automatisée, capables d'inférer des informations sans le consentement des sujets à partir d'informations que ces derniers avaient données volontairement dans des contextes très différents.

La liste des publications de ce domaine serait bien longue. Par exemple, la Rand Corporation publia une longue liste bibliographique[@Harrison1967] annotée au sujet des données personnelles informatisées. Cette liste regroupe près de 300 publications entre 1965 et 1967 sur le sujet. 

Des auteurs universitaires firent école. On peut citer&nbsp;:

- [Alan F. Westin](https://en.wikipedia.org/wiki/Alan_Westin)&nbsp;: *Privacy and freedom*[@Westin1967], *Civil Liberties and Computerized Data Systems*[@Westin1971], *Databanks in a Free Society: Computers, Record Keeping and Privacy*[@Westin1972]&nbsp;;
- James B. Rule&nbsp;: *Private lives and public surveillance: social control in the computer age*[@Rule1974]&nbsp;;
- Arthur R. Miller&nbsp;: *The assault on privacy. Computers, Data Banks and Dossier*[@Miller1971]&nbsp;;
- Malcolm Warner et Mike Stone, *The Data Bank Society&nbsp;: Organizations, Computers and Social Freedom*[@WarnerStone1970].


Toutes ces publications ont ceci en commun qu'elles procédèrent en deux étapes. La première consistait à dresser un tableau synthétique de la société américaine face à la captation des informations personnelles. Quatre termes peuvent résumer les enjeux du traitement des informations&nbsp;: *1)* la légitimité de la captation des informations, *2)* la permanence des données et leurs modes de rétention, *3)* la transférabilité (entre différentes organisations), *4)* la combinaison ou le recoupement de ces données et les informations ainsi inférées[@Wheeler1970]. 

La seconde étape consistait à choisir ce qui, dans cette «&nbsp;société du dossier (*dossier society*)&nbsp;» comme l'appelait Arthur R. Miller, devait faire l'objet de réformes. Deux fronts venaient en effet de s'ouvrir&nbsp;: l'État et les firmes.

Le premier, évident, était celui que la dystopie orwellienne pointait avec empressement&nbsp;: l'État de surveillance. Pour beaucoup de ces analystes, en effet, le fédéralisme américain et la multiplicité des agences gouvernementales pompaient allègrement la *privacy* des honnêtes citoyens et s'équipaient d'ordinateurs à temps partagé justement pour rendre interopérables les systèmes de traitement d'information à (trop) grande échelle. Un rapide coup d’œil sur les références citées, montre que, effectivement, la plupart des conclusions focalisaient sur le besoin d'adapter le droit aux impératifs constitutionnels américains. Tels sont par exemple les arguments de A. F. Westin pour lequel l'informatisation des données privées dans les différentes autorités administratives devait faire l'objet non d'un recul, mais de nouvelles règles portant sur la sécurité, l'accès des citoyens à leurs propres données et la pertinence des recoupements (comme par exemple l'utilisation restreinte du numéro de sécurité sociale). En guise de synthèse, le rapport de l'U.S. Department of health, Education and Welfare livré en 1973[@usdhew1973] (et où l'on retrouve Arthur R. Miller parmi les auteurs) repris ces éléments au titre de ses recommandations. Il prépara ainsi le [Privacy Act](https://en.wikipedia.org/wiki/Privacy_Act_of_1974) de 1974, qui vise notamment à prévenir l'utilisation abusive de documents fédéraux et garantir l'accès des individus aux données enregistrées les concernant.

Le second front, tout aussi évident mais moins accessible car protégé par le droit de propriété, était celui de la récolte de données par les firmes, et en particulier les banques. L'un des auteurs les plus connus, Arthur R. Miller dans *The assault on privacy*, fit la synthèse des deux fronts en focalisant sur le fait que l'informatisation des données personnelles, par les agences gouvernementales comme par les firmes, est une forme de surveillance et donc un exercice du pouvoir. Se poser la question de leur légitimité renvoie effectivement à des secteurs différents du droit, mais c'est pour lui le traitement informatique (il utilise le terme «&nbsp;cybernétique&nbsp;») qui est un instrument de surveillance par essence. Et cet instrument est orwellien&nbsp;:

> «&nbsp;Il y a à peine dix ans, on aurait pu considérer avec suffisance *Le meilleur des mondes* de Huxley ou *1984* de Orwell comme des ouvrages de science-fiction excessifs qui ne nous concerneraient pas et encore moins ce pays. Mais les révélations publiques répandues au cours des dernières années au sujet des nouvelles formes de pratiques d'information ont fait s'envoler ce manteau réconfortant mais illusoire.&nbsp;»

Pourtant, un an avant la publication de Miller fut voté le [Fair Credit Reporting Act](https://en.wikipedia.org/wiki/Fair_Credit_Reporting_Act), portant sur les obligations déclaratives des banques. Elle fut aussi l'une des premières lois sur la protection des données personnelles, permettant de protéger les individus, en particulier dans le secteur bancaire, contre la tenue de bases de données secrètes, la possibilité pour les individus d'accéder aux données et de les contester, et la limitation dans le temps de la rétention des informations.

Cependant, pour Miller, le Fair Credit Reporting Act est bien la preuve que la bureaucratie informatisée et le réseautage des données personnelles impliquent deux pertes de contrôle de la part de l'individu et pour lesquelles la régulation par le droit n'est qu'un pis-aller (pp. 25-38). On peut de même, en s'autorisant quelque anachronisme, s'apercevoir à quel point les deux types de perte de contrôles qu'il pointe nous sont éminemment contemporains.

- *The individual loss of control over personal information* &nbsp;: dans un contexte où les données sont mises en réseau et recoupées, dès lors qu'une information est traitée par informatique, le sujet et l'opérateur n'ont plus le contrôle sur les usages qui pourront en être faits. Sont en jeu la sécurité et l'intégrité des données (que faire en cas d'espionnage&nbsp;? que faire en cas de fuite non maîtrisée des données vers d'autres opérateurs&nbsp;: doit-on exiger que les opérateurs en informent les individus&nbsp;?).
- *The individual loss of control over the accuracy of his informational profil*&nbsp;: la centralisation des données permet de regrouper de multiples aspects de la vie administrative et sociale de la personne, et de recouper toutes ces données pour en inférer des profils. Dans la mesure où nous assistons à une concentration des firmes par rachats successifs et l'émergence de monopoles (Miller prend toujours l'exemple des banques), qu'arrive-t-il si certaines données sont erronées ou si certains recoupements mettent en danger le droit à la vie privée&nbsp;: par exemple le rapport entre les données de santé, l'identité des individus et les crédits bancaires.

Et Miller de conclure (p. 79)&nbsp;: 

> «&nbsp;Ainsi, l'informatisation, le réseautage et la réduction de la concurrence ne manqueront pas de pousser l'industrie de l'information sur le crédit encore plus profondément dans le marasme du problème de la protection de la vie privée.&nbsp;»



## Les échos du passé

La lutte pour la préservation de la vie privée dans une société numérisée passe par une identification des stratégies intentionnelles de la surveillance et par l'analyse des procédés d'extraction, rétention et traitement des données. La loi est-elle une réponse&nbsp;? Oui, mais elle est loin de suffire. La littérature nord-américaine dont nous venons de discuter montre que l'économie de la surveillance dans le contexte du traitement informatisé des données personnelles est née il y a plus de 50 ans. Et dès le début il fut démontré, dans un pays où les droits individuels sont culturellement associés à l'organisation de l'État fédéral (la Déclaration des Droits), non seulement que la *privacy* changeait de nature (elle s'étend au traitement informatique des informations fournies et aux données inférées) mais aussi qu'un équilibre s'établissait entre le degré de sanctuarisation de la vie privée et les impératifs régaliens et économiques qui réclament une industrialisation de la surveillance. 

Puisque l'intimité numérique n'est pas absolue mais le résultat d'un juste équilibre entre le droit et les pratiques, tout les jeux post-révolution informatique après les années 1960 consistèrent en une lutte perpétuelle entre défense et atteinte à la vie privée. C'est ce que montre Daniel J. Solove dans «&nbsp;A Brief History of Information Privacy Law&nbsp;»[@Solove2006] en dressant un inventaire chronologique des différentes réponses de la loi américaine face aux changements technologiques et leurs répercussions sur la vie privée. 

Il reste néanmoins que la dimension industrielle de l'économie de la surveillance a atteint en 2001 un point de basculement à l'échelle mondiale avec le [Patriot Act](https://fr.wikipedia.org/wiki/USA_PATRIOT_Act) dans le contexte de la lutte contre le terrorisme. À partir de là, les principaux acteurs de cette économie ont vu une demande croissante de la part des États pour récolter des données au-delà des limites strictes de la loi sous couvert des dispositions propres à la sûreté nationale et au secret défense. Pour rester sur l'exemple américain, Thomas Rabino écrit à ce sujet[@Rabino2013]&nbsp;:

> «&nbsp;Alors que le Privacy Act de 1974 interdit aux agences fédérales de constituer des banques de données sur les citoyens américains, ces mêmes agences fédérales en font désormais l'acquisition auprès de sociétés qui, à l'instar de ChoicePoint, se sont spécialisées dans le stockage d'informations diverses. Depuis 2001, le FBI et d'autres agences fédérales ont conclu, dans la plus totale discrétion, de fructueux contrats avec ChoicePoint pour l'achat des renseignements amassés par cette entreprise d'un nouveau genre. En 2005, le budget des États-Unis consacrait plus de 30 millions de dollars à ce type d'activité.&nbsp;» 

Dans un contexte plus récent, on peut affirmer que même si le risque terroriste est toujours agité comme un épouvantail pour justifier des atteintes toujours plus fortes à l'encontre de la vie privée, les intérêts économiques et la pression des lobbies ne peuvent plus se cacher derrière la Raison d'État. Si bien que plusieurs pays se mettent maintenant au diapason de l'avancement technologique et des impératifs de croissance économique justifiant par eux-mêmes des pratiques iniques. Ce fut le cas par exemple du gouvernement de Donald Trump qui, en mars 2017 et à la plus grande joie du lobby des fournisseurs d'accès, abroge une loi héritée du gouvernement précédent et qui exigeait que les FAI obtiennent sous conditions la permission de partager des renseignements personnels &ndash;&nbsp;y compris les données de localisation[@Fung2017].

Encore en mars 2017, c'est la secrétaire d'État à l'Intérieur Britannique Amber Rudd qui juge publiquement «&nbsp;inacceptable&nbsp;» le chiffrement des communications de bout en bout et demande aux fournisseurs de messagerie de créer discrètement des *backdoors*, c'est à dire renoncer au chiffrement de bout en bout sans le dire aux utilisateurs[@Balkan2017]. Indépendamment du caractère moralement discutable de cette injonction, on peut mesurer l'impact du message sur les entreprises comme Google, Facebook et consors&nbsp;: il existe des décideurs politiques capables de demander à ce qu'un fournisseur de services propose à ses utilisateurs un faux chiffrement, c'est-à-dire que le droit à la vie privée soit non seulement bafoué mais, de surcroît, que le mensonge exercé par les acteurs privés soit couvert par les acteurs publics, et donc par la loi.

Comme le montre Shoshana Zuboff, le capitalisme de surveillance est aussi une idéologie, celle qui instaure une hiérarchie entre les intérêts économiques et le droit. Le droit peut donc être une arme de lutte pour la sauvegarde de la vie privée dans l'économie de la surveillance, mais il ne saurait suffire dans la mesure où il n'y pas de loyauté entre les acteurs économiques et les sujets et parfois même encore moins entre les décideurs publics et les citoyens. 

Dans ce contexte où la confiance n'est pas de mise, les portes sont restées ouvertes depuis les années 1970 pour créer l'industrie des *big data* dont le carburant principal est notre intimité, notre quotidienneté. C'est parce qu'il est désormais possible de repousser toujours un peu plus loin les limites de la captation des données personnelles que des théories économique prônent la fidélisation des clients et la prédiction de leurs comportements comme seuls points d'appui des investissements et de l'innovation. C'est vrai dans le marketing, c'est vrai dans les services et l'innovation numériques. Et tout naturellement c'est vrai dans la vie politique, comme le montre par exemple l'affaire des *dark posts*[@Lapowsy2017] durant la campagne présidentielle de D. Trump&nbsp;: la possibilité de contrôler l'audience et d'influencer une campagne présidentielle via les réseaux sociaux comme Facebook est désormais démontrée.

Tant que ce modèle économique existera, aucune confiance ne sera possible. La confiance est même absente des pratiques elles-mêmes, en particulier dans le domaine du traitement algorithmique des informations. En septembre 2017, la chercheuse Zeynep Tufekci, lors d'une conférence TED[@Tufekci2017], reprenait exactement les questions d'Arthur R. Miller dans *The assault on privacy*, soit 46 ans après. Miller prenait comme étude de cas le stockage d'information bancaire sur les clients débiteurs, et Tufekci prend les cas des réservation de vols aériens en ligne et du streaming vidéo. Dans les deux réflexions, le constat est le même&nbsp;: le traitement informatique de nos données personnelles implique que nous (les sujets et les opérateurs eux-mêmes) perdions le contrôle sur ces données&nbsp;: 

> «&nbsp;Le problème, c'est que nous ne comprenons plus vraiment comment fonctionnent ces algorithmes complexes. Nous ne comprenons pas comment ils font cette catégorisation. Ce sont d'énormes matrices, des milliers de lignes et colonnes, peut-être même des millions, et ni les programmeurs, ni quiconque les regardant, même avec toutes les données, ne comprend plus comment ça opère exactement, pas plus que vous ne sauriez ce que je pense en ce moment si l'on vous montrait une coupe transversale de mon cerveau. C'est comme si nous ne programmions plus, nous élevons une intelligence que nous ne comprenons pas vraiment.&nbsp;»

Z. Tufekci montre même que les algorithmes de traitement sont en mesure de fournir des conclusions (qui permettent par exemple d'inciter les utilisateurs à visualiser des vidéos sélectionnées d'après leur profil et leur historique) mais que ces conclusions ont ceci de particulier qu'elle modélisent le comportement humain de manière à l'influencer dans l'intérêt du fournisseur. D'après Z. Tufekci&nbsp;: «&nbsp; L'algorithme a déterminé que si vous pouvez pousser les gens à penser que vous pouvez leur montrer quelque chose de plus extrême (nda&nbsp;: des vidéos racistes dans l'exemple cité), ils ont plus de chances de rester sur le site à regarder vidéo sur vidéo, descendant dans le terrier du lapin pendant que Google leur sert des pubs.&nbsp;»

Ajoutons de même que les technologies de *[deep learning](https://fr.wikipedia.org/wiki/Apprentissage_profond)*, financées par millions par les GAFAM, se prêtent particulièrement bien au jeu du traitement automatisé en cela qu'elle permettent, grâce à l'extrême croissance du nombre de données, de procéder par apprentissage. Cela permet à Facebook de structurer la grande majorité des données des utilisateurs qui, auparavant, n'était pas complètement exploitable[@Marr2016]. Par exemple, sur les milliers de photos de chatons partagées par les utilisateurs, on peut soit se contenter de constater une redondance et ne pas les analyser davantage, soit apprendre à y reconnaître d'autres informations, comme par exemple l'apparition, en arrière-plan, d'un texte, d'une marque de produit, etc. Il en est de même pour la reconnaissance faciale, qui a surtout pour objectif de faire concorder les identités des personnes avec toutes les informations que l'on peut inférer à partir de l'image et du texte. 

Si les techniques statistiques ont le plus souvent comme objectif de contrôler les comportements à l'échelle du groupe, c'est parce que le seul fait de catégoriser automatiquement les individus consiste à considérer que leurs données personnelles en constituent l'essence. L'économie de la surveillance démontre ainsi qu'il n'y a nul besoin de connaître une personne pour en prédire le comportement, et qu'il n'y a pas besoin de connaître chaque individu d'un groupe en particulier pour le catégoriser et prédire le comportement du groupe, il suffit de laisser faire les algorithmes&nbsp;: le tout est d'être en mesure de classer les sujets dans les bonnes catégories et même faire en sorte qu'ils y entrent tout à fait. Cela a pour effet de coincer littéralement les utilisateurs des services «&nbsp;capteurs de données&nbsp;» dans des *bulles de filtres* où les informations auxquelles ils ont accès leur sont personnalisées selon des profils calculés[@Bosker2011]. Si vous partagez une photo de votre chat devant votre cafetière, et que dernièrement vous avez visité des sites marchands, vous aurez de grandes chance pour vos futures annonces vous proposent la marque que vous possédez déjà et exercent, par effet de répétition, une pression si forte que c'est cette marque que vous finirez par acheter. Ce qui fonctionne pour le marketing peut très bien fonctionner pour d'autres objectifs, même politiques.

En somme tous les déterminants d'une société soumise au capitalisme de surveillance, apparus dès les années 1970, structurent le monde numérique d'aujourd'hui sans que les lois ne puissent jouer de rôle pleinement régulateur. L'usage caché et déloyal des *big data*, le trafic de données entre organisations, la dégradation des droits individuels (à commencer par la liberté d'expression et le droit à la vie privée), tous ces éléments ont permis à des monopoles d'imposer un modèle d'affaire et affaiblir l'État-nation. Combien de temps continuerons-nous à l'accepter&nbsp;? 




## Sortir du marasme

En 2016, à la fin de son article synthétique sur le capitalisme de surveillance[@zuboff_secrets_2106], Shoshana Zuboff exprime personnellement un point de vue selon lequel la réponse ne peut pas être uniquement technologique&nbsp;:

> «&nbsp;(...) les faits bruts du capitalisme de surveillance suscitent nécessairement mon indignation parce qu’ils rabaissent la dignité humaine. L’avenir de cette question dépendra des savants et journalistes indignés attirés par ce projet de frontière, des élus et des décideurs indignés qui comprennent que leur autorité provient des valeurs fondamentales des communautés démocratiques, et des citoyens indignés qui agissent en sachant que l’efficacité sans l’autonomie n’est pas efficace, la conformité induite par la dépendance n’est pas un contrat social et être libéré de l’incertitude n’est pas la liberté.&nbsp;»

L'incertitude au sujet des dérives du capitalisme de surveillance n'existe pas. Personne ne peut affirmer aujourd'hui qu'avec l'avènement des *big data* dans les stratégies économiques, on pouvait ignorer que leur usage déloyal était non seulement possible mais aussi que c'est bien cette direction qui fut choisie d'emblée dans l'intérêt des monopoles et en vertu de la centralisation des informations et des capitaux. Depuis les années 1970, plusieurs concepts ont cherché à exprimer la même chose. Pour n'en citer que quelques-uns&nbsp;: computocracie (M. Warner et M. Stone, 1970), société du dossier (Arthur R. Miller, 1971), surveillance de masse (J. Rule, 1973), dataveillance (R. Clarke, 1988), capitalisme de surveillance (Zuboff, 2015)&hellip; tous cherchent à démontrer que la surveillance des comportements par l'usage des données personnelles implique en retour la recherche collective de points de rupture avec le modèle économique et de gouvernance qui s'impose de manière déloyale. Cette recherche peut s'exprimer par le besoin d'une régulation démocratiquement décidée et avec des outils juridiques. Elle peut s'exprimer aussi autrement, de manière violente ou pacifiste, militante et/ou contre-culturelle. 

Plusieurs individus, groupes et organisation se sont déjà manifestés dans l'histoire à ce propos. Les formes d'expression et d'action ont été diverses&nbsp;:

- institutionnelles&nbsp;: les premières formes d'action pour garantir le droit à la vie privée ont consisté à établir des rapports collectifs préparatoires à des grandes lois, comme le rapport *Records, computers and the rights of citizens*, de 1973, cité plus haut&nbsp;;
- individualistes, antisociales et violentes&nbsp;: bien que s'inscrivant dans un contexte plus large de refus technologique, l'affaire [Theodore Kaczynski](https://fr.wikipedia.org/wiki/Theodore_Kaczynski) (alias Unabomber) de 1978 à 1995 est un bon exemple d'orientation malheureuse que pourraient prendre quelques individus isolés trouvant des justifications dans un contexte paranoïaque&nbsp;;
- collectives - activistes - légitimistes&nbsp;: c'est le temps des manifestes [cypherpunk](https://fr.wikipedia.org/wiki/Cypherpunk) des années 1990[@hughes1993 ; @May1992 ; @Kirtchev1997], ou plus récemment le mouvement [Anonymous](https://fr.wikipedia.org/wiki/Anonymous_(collectif)), auxquels on peut ajouter des collectifs «&nbsp;événementiels&nbsp;», comme le [Jam Echelon Day](https://fr.wikipedia.org/wiki/Echelon#Jam_Echelon_Day)&nbsp;;
- Associatives, organisées&nbsp;: on peut citer le [mouvement pour le logiciel libre](https://www.gnu.org/gnu/manifesto.fr.html) et la [Free Software Foundation](http://www.fsf.org/), l'[Electronic Frontier Foundation](https://www.eff.org/fr/fight), [La Quadrature du Net](https://www.laquadrature.net/), ou bien encore certaines branches d'activité d'organisation plus générales comme la [Ligue des Droits de l'Homme](https://www.ldh-france.org/big-data-algorithmes-risques-discriminations-lexemple-lassurance/), [Reporter Sans Frontière](https://rsf.org/fr/actions/libertes-sur-internet), etc.

Les limites de l'attente démocratique sont néanmoins déjà connues. La société ne peut réagir de manière légale, par revendication interposée, qu'à partir du moment où l'exigence de transparence est remplie. Lorsqu'elle ne l'est pas, au pire les citoyens les plus actifs sont taxés de complotistes, au mieux apparaissent de manière épisodique des alertes, à l'image des révélations d'Edward Snowden, mais dont la fréquence est si rare malgré un impact psychologique certain, que la situation a tendance à s'enraciner dans un *statu quo* d'où sortent généralement vainqueurs ceux dont la capacité de lobbying est la plus forte. 

À cela s'ajoute une difficulté technique due à l'extrême complexité des systèmes de surveillance à l’œuvre aujourd'hui, avec des algorithmes dont nous maîtrisons de moins en moins les processus de calcul (cf. Z. Tufekci). À ce propos on peut citer Roger Clarke[@Clarke2017]&nbsp;: 

> «&nbsp;Dans une large mesure, la transparence a déjà été perdue, en partie du fait de la numérisation, et en partie à cause de l'application non pas des approches procédurales et d'outils de développement des logiciels algorithmiques du XX<sup>e</sup> siècle, mais à cause de logiciels de dernière génération dont la raison d'être est obscure ou à laquelle la notion de raison d'être n'est même pas applicable.&nbsp;»

Une autre option pourrait consister à mettre en œuvre un modèle alternatif qui permette de sortir du marasme économique dans lequel nous sommes visiblement coincés. Sans en faire l'article, le projet [Contributopia](https://contributopia.org/fr/home/) de Framasoft cherche à participer, à sa mesure, à un processus collectif de réappropriation d'Internet et, partant: 

- montrer que le code est un bien public et que la transparence, grâce aux principes du logiciel libre (l'ouverture du code), permet de proposer aux individus un choix éclairé, à l'encontre de l'obscurantisme de la dataveillance&nbsp;;
- promouvoir des apprentissages à contre-courant des pratiques de captation des vies privées et vers des usages basés sur le partage (du code, de la connaissance) entre les utilisateurs&nbsp;;
- rendre les utilisateurs autonomes et en même temps contributeurs à un réseau collectif qui les amènera naturellement, par l'attention croissante portée aux pratiques des monopoles, à refuser ces pratiques, y compris de manière active en utilisant des solutions de chiffrement, par exemple.

Mais Contributopia de Framasoft ne concerne que quelques aspects des stratégies de sortie du capitalisme de surveillance. Par exemple, pour pouvoir œuvrer dans cet esprit, une politique rigide en faveur de la neutralité du réseau Internet doit être menée. Les entreprises et les institutions publiques doivent être aussi parmi les premières concernées, car il en va de leur autonomie numérique, que cela soit pour de simples questions économiques (ne pas dépendre de la bonne volonté d'un monopole et de la logique des brevets) mais aussi pour des questions de sécurité. Enfin, le passage du risque pour la vie privée au risque de manipulation sociale étant avéré, toutes les structures militantes en faveur de la démocratie et les droits de l'homme doivent urgemment porter leur attention sur le traitement des données personnelles. Le cas de la britannique Amber Rudd est loin d'être isolé&nbsp;: la plupart des gouvernements collaborent aujourd'hui avec les principaux monopoles de l'économie numérique et, donc, contribuent activement à l'émergence d'une société de la surveillance. Aujourd'hui, le droit à la vie privée, au chiffrement des communications, au secret des correspondances sont des droits à protéger coûte que coûte sans avoir à choisir entre la liberté et les épouvantails (ou les sirènes) agités par les communicants.



# Le contrat social fait 128 bits… ou plus

**(Les anciens Léviathans)**

Chiffrer nos données est un acte censé protéger nos vies privées. Dans le contexte de la surveillance massive de nos communications, il devient une nécessité. Mais peut-on mettre en balance la notion de vie privée et la paix entre tous que le contrat social est censé nous garantir&nbsp;? Le prétendu choix entre liberté et sécurité tendrait à montrer que le pouvoir de l'État ne souffre aucune option. Et pourtant, les anciennes conceptions ont la vie dure.


## Quand Manuel Valls s'exprime

Dans un article de *Rue 89*, le journaliste Andréa Fradin revenait sur une allocution du premier ministre M. Valls, tenue le 16 octobre 2015 à l'occasion de la présentation de la *Stratégie nationale pour la sécurité numérique*[@fradin2015]. Durant son discours, M. Valls tenait ces propos&nbsp;:

> Mais &ndash;&nbsp;s'il était nécessaire de donner à nos services de renseignement les outils indispensables pour assumer leurs missions dans la société numérique&nbsp;&ndash; mon gouvernement reste favorable à ce que les acteurs privés continuent de bénéficier pleinement, pour se protéger, de toutes les ressources qu'offre la cryptologie légale.

Et le journaliste de s'interroger sur la signification de ce que pourrait bien être la «&nbsp;cryptologie légale&nbsp;», dans la mesure où le fait de pouvoir chiffrer des communications ne se pose pas en ces termes. Sur son site, l'[ANSSI est très claire](http://www.ssi.gouv.fr/administration/reglementation/controle-reglementaire-sur-la-cryptographie/) :

> L'utilisation d'un moyen de cryptologie est libre. Il n'y a aucune démarche à accomplir.
> 
> En revanche, la fourniture, l'importation, le transfert intracommunautaire et l'exportation d'un moyen de cryptologie sont soumis, sauf exception, à déclaration ou à demande d'autorisation.

Si M. Valls s'adressait essentiellement aux professionnels des communications, une telle déclaration mérite que l'on s'y arrête un peu. Elle résonne particulièrement fort dans le contexte juridique, social et émotionnel très particulier qui a vu se multiplier l'adoption de lois et de procédures qui mettent fortement en danger les libertés de communication et d'expression, sous couvert de lutte contre le terrorisme, ainsi que l'illustrait le [Projet de loi renseignement](https://wiki.laquadrature.net/Portail:Loi_Renseignement) au printemps 2015.

On note que M. Valls précise que les moyens de «&nbsp;cryptologie légale&nbsp;» sont laissés au libre choix des acteurs privés «&nbsp;pour se protéger&nbsp;». En effet, comme le rappelle l'ANSSI, le fait de fournir un moyen de chiffrer des communications doit faire l'objet d'une déclaration ou d'une autorisation. C'est uniquement dans le choix des systèmes préalablement autorisés, que M. Valls concède aux acteurs privés qui en ressentent le besoin d'aller piocher le meilleur moyen d'assurer la confidentialité et l'authenticité de leurs échanges ou des échanges de leurs utilisateurs.

C'est sans doute cela qu'il fallait comprendre dans cette phrase. À ceci près que rappeler ce genre d'éléments aussi basiques à des acteurs déjà bien établis dans le secteur des communications numériques, ressemble bien plutôt à une mise en garde&nbsp;: il y a du chiffrement autorisé et il y a du chiffrement qui ne l'est pas. En d'autres termes, du point de vue des fournisseurs comme du point de vue des utilisateurs, tout n'est pas permis, y compris au nom de la protection de la vie privée.


La question du choix entre respect de la vie privée (ou d'autres libertés comme les libertés d'expression et de communication) et l'intérêt suprême de l'État dans la protection de ses citoyens, est une question qui est à l'heure actuelle bien loin d'être tranchée (si elle peut l'être un jour). Habituellement caricaturée sur le mode binaire du choix entre sécurité et liberté, beaucoup ont essayé ces derniers temps de calmer les ardeurs des partisans des deux camps, en oubliant comme nous le verrons dans les prochaines sections, que le choix datait d'au moins des premiers théoriciens du Contrat Social, il y a trois siècles. L'histoire de PGP ([Pretty Good Privacy](https://fr.wikipedia.org/wiki/Pretty_Good_Privacy)) et du standard [OpenPGP](https://fr.wikipedia.org/wiki/OpenPGP) est jalonnée de cette dualité (sécurité et liberté) dans notre conception du contrat social. 


## Autorité et PGP

La première diffusion de PGP était déjà illégale au regard du droit à l'exportation des produits de chiffrement, ce qui a valu à son créateur, Philip Zimmermann quelques ennuis juridiques au début des années 1990. La France a finalement suivi la politique nord-américaine concernant PGP en autorisant l'usage mais en restreignant son étendue. C'est l'esprit du décret 99-200 du 17 mars 1999, qui autorise, sans formalité préalable, l'utilisation d'une clé de chiffrement à condition qu'elle soit inférieure ou égale à 128 bits pour chiffrer des données. Au-delà, il fallait une autorisation jusqu'au vote de la *Loi sur l'économie numérique* en 2004, qui fait sauter le verrou des 128 bits ([art. 30-1](http://www.legifrance.gouv.fr/eli/loi/2004/6/21/ECOX0200175L/jo#JORFSCTA000000911489)) pour l'usage du chiffrement (les moyens, les logiciels, eux, sont soumis à déclaration[^declalog]).

Si l'on peut aisément mettre le doigt sur les lacunes du *système* PGP[@Misc2014], il reste qu'une clé de chiffrement à 128 bits, si l'implémentation est correcte, permet déjà de chiffrer très efficacement des données, quelles qu'elles soient. Lorsque les activités de surveillance de masse de la NSA furent en partie révélées par E. Snowden, on apprit que l'une des pratiques consiste à capter et stocker les contenus des communications de manière exhaustive, qu'elles soient chiffrées ou non. En cas de chiffrement, la NSA compte sur les progrès techniques futurs pour pouvoir les déchiffrer un jour où l'autre, selon les besoins. Ce gigantesque travail d'archivage réserve en principe pour l'avenir des questions de droit plutôt inextricables (par exemple l'évaluation du degré de préméditation d'un crime, ou le fait d'être suspect parce qu'on peut établir que 10 ans plus tôt Untel était en relation avec Untel). Mais le principal sujet, face à ce gigantesque travail d'espionnage de tout l'Internet, et d'archivage de données privées lisibles et illisibles, c'est de savoir dans quelle mesure il est possible de réclamer un peu plus que le seul *respect* de la vie privée. Pour qu'une agence d'État s'octroie le droit de récupérer dans mon intimité des données qu'elle n'est peut-être même pas capable de lire, en particulier grâce à des dispositifs comme PGP, il faut se questionner non seulement sur sa légitimité mais aussi sur la conception du pouvoir que cela suppose.

Si PGP a finalement été autorisé, il faut bien comprendre quelles en sont les limitations légales. Pour rappel, PGP fonctionne sur la base du binôme clé publique / clé privée. Je chiffre mon message avec ma clé de session, générée aléatoirement à 128 bits (ou plus), et cette clé de session est elle-même chiffrée avec la clé publique du destinataire (qui peut largement excéder les 128 bits). J'envoie alors un paquet contenant a) le message chiffré avec ma clé de session, et b) ma clé de session chiffrée par la clé publique de mon destinataire. Puis, comme ce dernier possède la clé privée qui va de pair avec sa clé publique, lui seul va pouvoir déchiffrer le message. On comprend donc que la clé privée et la clé publique ont des rôles bien différents. Alors que la clé privée sert à chiffrer les données, la clé publique sert contrôler l'accès au contenu chiffré. Dans l'esprit du décret de 1999, c'est la clé de session qui était concernée par la limitation à 128 bits.

PGP a donc été autorisé pour au moins trois raisons, que je propose ici à titre de conjectures&nbsp;:

   - parce que PGP devenait de plus en plus populaire et qu'il aurait été difficile d'en interdire officiellement l'usage, ce qui aurait supposé une surveillance de masse des échanges privés (!),
   - parce que PGP est une source d'innovation en termes de services et donc porteur d'intérêts économiques,
   - parce que PGP, limité en chiffrement des contenus à 128 bits, permettait d'avoir un étalon de mesure pour justifier la nécessité de délivrer des autorisations pour des systèmes de chiffrement supérieurs à 128 bits, c'est-à-dire des chiffrements hautement sécurisés, même si la version autorisée de PGP est déjà très efficace. Après 2004, la question ne se pose plus en termes de limitation de puissance mais en termes de surveillance des moyens (ce qui compte, c'est l'intention de chiffrer et à quel niveau).

En somme c'est une manière pour l'État de retourner à son avantage une situation dans laquelle il se trouvait pris en défaut. Je parle en premier lieu des États-Unis, car j'imagine plutôt l'État français (et les États européens en général) en tant que suiveur, dans la mesure où si PGP est autorisé d'un côté de l'Atlantique, il aurait été de toute façon contre-productif de l'interdire de l'autre. En effet, Philip Zimmermann rappelle bien les enjeux dans son texte «&nbsp;Pourquoi j'ai écrit PGP&nbsp;»[@Zimmermann1999]. La principale raison qui justifie selon lui l'existence de PGP, est qu'une série de dispositions légales entre 1991 et 1994 imposaient aux compagnies de télécommunication américaines de mettre en place des dispositions permettant aux autorités d'intercepter en clair des communications. En d'autres termes, il s'agissait d'optimiser les dispositifs de communication pour faciliter leur accès par les services d'investigation et de surveillance aujourd'hui tristement célèbres. Ces dispositions légales ont été la cause de scandales et furent en partie retirés, mais ces intentions cachaient en vérité un programme bien plus vaste et ambitieux. Les [révélations](https://fr.wikipedia.org/wiki/R%C3%A9v%C3%A9lations_d%27Edward_Snowden) d'E. Snowden nous en ont donné un aperçu concret.


## Inconstitutionnalité de la surveillance de masse

Là où l'argumentaire de Philip Zimmermann devient intéressant, c'est dans la justification de l'intention de créer PGP, au-delà de la seule réaction à un contexte politique dangereux. Pour le citer&nbsp;:

> [...] Il n'y a rien de mal dans la défense de votre intimité. L'intimité est aussi importante que la Constitution. Le droit à la vie privée est disséminé implicitement tout au long de la Déclaration des Droits. Mais quand la Constitution des États-Unis a été bâtie, les Pères Fondateurs ne virent aucun besoin d'expliciter le droit à une conversation privée. Cela aurait été ridicule. Il y a deux siècles, toutes les conversations étaient privées. Si quelqu'un d'autre était en train d'écouter, vous pouviez aller tout simplement derrière l'écurie et avoir une conversation là. Personne ne pouvait vous écouter sans que vous le sachiez. Le droit à une conversation privée était un droit naturel, non pas seulement au sens philosophique, mais au sens des lois de la physique, étant donnée la technologie de l'époque. Mais avec l'arrivée de l'âge de l'information, débutant avec l'invention du téléphone, tout cela a changé. Maintenant, la plupart de nos conversations sont acheminées électroniquement. Cela permet à nos conversations les plus intimes d'être exposées sans que nous le sachions.

L'évocation de la Constitution des États-Unis est tout à fait explicite dans l'argumentaire de Philip Zimmermann, car la référence à laquelle nous pensons immédiatement est le Quatrième amendement (de la *Déclaration des Droits*)&nbsp;:

> Le droit des citoyens d'être garantis dans leurs personne, domicile, papiers et effets, contre les perquisitions et saisies non motivées ne sera pas violé, et aucun mandat ne sera délivré, si ce n'est sur présomption sérieuse, corroborée par serment ou affirmation, ni sans qu'il décrive particulièrement le lieu à fouiller et les personnes ou les choses à saisir.

En d'autres termes, la surveillance de masse est anticonstitutionnelle. Et cela va beaucoup plus loin qu'une simple affaire de loi. Le Quatrième amendement repose essentiellement sur l'adage très britannique *my home is my castle*, c'est à dire le point de vue de la *castle doctrine*, une rémanence du droit d'asile romain (puis chrétien). C'est-à-dire qu'il existe un lieu en lequel toute personne peut trouver refuge face à l'adversité, quelle que soit sa condition et ce qu'il a fait, criminel ou non. Ce lieu pouvant être un temple (c'était le cas chez les Grecs), un lieu sacré chez les romains, une église chez les chrétiens, et pour les peuples qui conféraient une importance viscérale à la notion de propriété privée, comme dans l'Angleterre du XVI<sup>e</sup> siècle, c'est la demeure. La naissance de l'État moderne (et déjà un peu au Moyen âge) encadra fondamentalement ce droit en y ajoutant des conditions d'exercice, ainsi, par exemple, dans le Quatrième Amendement, l'existence ou non de «&nbsp;présomptions sérieuses&nbsp;».

## État absolu, soif d'absolu

Le besoin de limiter drastiquement ce qui ressort de la vie privée, est éminemment lié à la conception de l'État moderne et du contrat social. En effet, ce qui se joue à ce moment de l'histoire, qui sera aussi celui des Lumières, c'est une conception rationnelle de la vie commune contre l'irrationnel des temps anciens. C'est Thomas Hobbes qui, parmi les plus acharnés du pouvoir absolu de l'État, traumatisé qu'il était par la guerre civile, pensait que rien ne devait entraver la survie et l'omnipotence de l'État au risque de retomber dans les âges noirs de l'obscurantisme et du déchaînement des passions. Pour lui, le pacte social ne tient que dans la mesure où, pour le faire respecter, l'État peut exercer une violence incommensurable sur les individus qui composent le tissu social (et ont conféré à l'État l'exercice de cette violence). Le pouvoir de l'État s'exerce par la centralisation et la soumission à l'autorité, ainsi que le résume très bien Pierre Dockès dans son article «&nbsp;Hobbes et le pouvoir&nbsp;»[@dockes2006].

Mais qu'est-ce qui était irrationnel dans ces temps anciens, par exemple dans la République romaine&nbsp;? Beaucoup de choses à vrai dire, à commencer par le polythéisme. Et justement, l'*asylum* latin fait partie de ces conceptions absolues contre lesquelles les théoriciens du contrat social se débattront pour trouver des solutions. L'État peut-il ou non supporter l'existence même d'un lieu où son pouvoir ne pourrait s'exercer, en aucun cas, même s'il existe des moyens techniques pour le faire&nbsp;? C'est le *tabou*, dans la littérature ethnologique, dont la transgression oblige le transgresseur à se soumettre à une forme d'intervention au-delà de la justice des hommes, et par là oblige les autres hommes à l'impuissance face à cette transgression innommable et surnaturelle.

À cet absolu générique s'opposent donc les limitations de l'État de droit. Dans le Code Civil français, l'article 9 stipule&nbsp;: «&nbsp;Chacun a droit au respect de sa vie privée &nbsp;. Tout est dans la notion de respect, que l'on oublie bien vite dans les discussions, ici et là, autour des conditions de la vie privée dans un monde numérique. La définition du respect est une variable d'ajustement, alors qu'un absolu ne se discute pas. Et c'est cette soif d'absolu que l'on entend bien souvent réclamée, car il est tellement insupportable de savoir qu'un ou plusieurs États organisent une surveillance de masse que la seule réaction proportionnellement inverse que peuvent opposer les individus au non-respect de la vie privée relève de l'irrationnel&nbsp;: l'absolu de la vie privée, l'idée qu'une vie privée est non seulement inviolable mais qu'elle constitue aussi l'*asylum* de nos données numériques.

Qu'il s'agisse de la vie privée, de la propriété privée ou de la liberté d'expression, à lire la Déclaration des droits de l'homme et du citoyen de 1789, elles sont toujours soumises à deux impératifs. Le premier est un dérivé de l'impératif catégorique kantien&nbsp;: «&nbsp;ne fais pas à autrui ce que tu n'aimerais pas qu'on te fasse&nbsp;» (article 4 de la Déclaration), qui impose le pouvoir d'arbitrage de l'État («&nbsp;Ces bornes ne peuvent être déterminées que par la Loi&nbsp;») dans les affaires privées comme dans les affaires publiques. L'autre impératif est le principe de souveraineté (article 3 de la Déclaration) selon lequel «&nbsp;Le principe de toute Souveraineté réside essentiellement dans la Nation. Nul corps, nul individu ne peut exercer d'autorité qui n'en émane expressément&nbsp;». En d'autres termes, il faut choisir&nbsp;: soit les règles de l'État pour la paix entre les individus, soit le retour à l'âge du surnaturel et de l'immoralité.

À l'occasion du vote concernant la [Loi Renseignement](https://fr.wikipedia.org/wiki/Loi_relative_au_renseignement), c'est en ces termes que furent posés nombre de débats autour de la vie privée sous l'apparent antagonisme entre sécurité et liberté. D'un côté, on opposait la loi comme le moyen sans lequel il ne pouvait y avoir d'autre salut qu'en limitant toujours plus les libertés des individus. De l'autre côté, on voyait la loi comme un moyen d'exercer un pouvoir à d'autres fins (ou profits) que la paix sociale&nbsp;: maintenir le pouvoir de quelques-uns ou encore succomber aux demandes insistantes de quelques lobbies.

Mais très peu se sont penché sur la réaction du public qui voyait dans les révélations de Snowden comme dans les lois «&nbsp;scélérates&nbsp;» la transgression du tabou de la vie privée, de l'*asylum*. Comment&nbsp;? Une telle conception archaïque n'est-elle pas depuis longtemps dépassée&nbsp;? Il y aurait encore des gens soumis au *diktat* de la Révélation divine&nbsp;? et après tout, qu'est-ce qui fait que j'accorde un caractère absolu à un concept si ce n'est parce qu'il me provient d'un monde d'idées (formelles ou non) sans être le produit de la déduction rationnelle et de l'utilité&nbsp;? Cette soif d'absolu, si elle ne provient pas des dieux, elle provient du monde des idées. Or, si on en est encore à l'opposition Platon *vs.* Aristote, comment faire la démonstration de ce qui n'est pas démontrable, savoir&nbsp;: on peut justifier, au nom de la sécurité, que l'État puisse intervenir dans nos vies privées, mais au nom de quoi justifier le caractère absolu de la vie privée&nbsp;? Saint Augustin, au secours&nbsp;!

À ceci près, mon vieil Augustin, que deux éléments manquent encore à l'analyse et montrent qu'en réalité le caractère absolu du droit à la vie privée, d'où l'État serait exclu quelle que soit sa légitimité, a muté au fil des âges et des pratiques démocratiques.


## Dialogue entre droit de savoir et droit au secret

C'est l'autorité judiciaire qui exerce le droit de savoir au nom de la manifestation de la vérité. Et à l'instar de la vie privée, la notion de vérité possède un caractère tout aussi absolu. La vie privée manifeste, au fond, notre soif d'exercer notre droit au secret. Ses limites&nbsp;? elles sont instituées par la justice (et particulièrement la jurisprudence) et non par le pouvoir de l'État. Ainsi le [Rapport annuel 2010](https://www.courdecassation.fr/publications_26/rapport_annuel_36/rapport_2010_3866/etude_droit_3872/e_droit_3876/reconnu_interet_3877/face_droit_19405.html) de la Cour de Cassation exprime parfaitement le cadre dans lequel peut s'exercer le droit de savoir en rapport avec le respect de la vie privée&nbsp;:

> Dans certains cas, il peut être légitime de prendre connaissance d'une information ayant trait à la vie privée d'une personne indépendamment de son consentement. C'est dire qu'il y a lieu de procéder à la balance des intérêts contraires. Un équilibre doit être trouvé, dans l'édification duquel la jurisprudence de la Cour de cassation joue un rôle souvent important, entre le droit au respect de la vie privée et des aspirations, nombreuses, à la connaissance d'informations se rapportant à la vie privée d'autrui. Lorsqu'elle est reconnue, la primauté du droit de savoir sur le droit au respect de la vie privée se traduit par le droit de prendre connaissance de la vie privée d'autrui soit dans un intérêt privé, soit dans l'intérêt général.

En d'autres termes, il n'y a aucun archaïsme dans la défense de la vie privée face à la décision publique&nbsp;: c'est simplement que le débat n'oppose pas vie privée et sécurité, et en situant le débat dans cette fausse dialectique, on oublie que le premier principe de cohésion sociale, c'est la justice. On retrouve ici aussi tous les contre-arguments avancés devant la tendance néfaste des gouvernements à vouloir automatiser les sanctions sans passer par l'administration de la justice. Ainsi, par exemple, le fait de se passer d'un juge d'instruction pour surveiller et sanctionner le téléchargement «&nbsp;illégal&nbsp;» d'œuvres cinématographiques, ou de vouloir justifier la surveillance de toutes les communications au nom de la sécurité nationale au risque de suspecter tout le monde. C'est le manque (subit ou consenti) de justice qui conditionne toutes les dictatures.

Le paradoxe est le suivant&nbsp;: en situant le débat sur le registre sécurité *vs.* liberté, au nom de l'exercice légitime du pouvoir de l'État dans la protection des citoyens, on place le secret privé au même niveau que le secret militaire et stratégique, et nous serions alors tous des ennemis potentiels, exactement comme s'il n'y avait pas d'État ou comme si son rôle ne se réduisait qu'à être un instrument de répression à disposition de quelques-uns contre d'autres, ou du souverain contre la Nation. Dans ce débat, il ne faudrait pas tant craindre le «&nbsp;retour à la nature&nbsp;» mais le retour à la servitude.

Le second point caractéristique du droit de savoir, est qu'on ne peut que lui opposer des arguments rationnels. S'il s'exerce au nom d'un autre absolu, la vérité, tout l'exercice consiste à démontrer non pas le *pourquoi* mais le *comment* il peut aider à atteindre la vérité (toute relative qu'elle soit). On l'autorise alors, ou pas, à l'aune d'un consentement éclairé et socialement acceptable. On entre alors dans le règne de la déduction logique et de la jurisprudence. Pour illustrer cela, il suffit de se pencher sur les cas où les secrets professionnels ont été cassés au nom de la manifestation de la vérité, à commencer par le secret médical. La Cour de cassation explique à ce sujet, dans son Rapport 2010 :

> [...] La chambre criminelle a rendu le 16 février 2010 (Bull. crim. 2010, no 27, pourvoi no 09-86.363) une décision qui, entre les droits fondamentaux que sont la protection des données personnelles médicales d'une part, et l'exercice des droits de la défense d'autre part, a implicitement confirmé l'inopposabilité du secret au juge d'instruction, mais aussi la primauté du droit de la défense qui peut justifier, pour respecter le principe du contradictoire, que ce secret ne soit pas opposable aux différentes parties.

Au risque de rappeler quelques principes évidents, puisque nous sommes censés vivre dans une société rationnelle, toute tentative de casser un secret et s'immiscer dans la vie privée, ne peut se faire *a priori* que par décision de justice à qui l'on reconnaît une légitimité «&nbsp;prudentielle&nbsp;». Confier ce rôle de manière unilatérale à l'organe d'exercice du pouvoir de l'État, revient à nier ce partage entre l'absolu et le rationnel, c'est à dire révoquer le contrat social.

## La sûreté des échanges est un droit naturel et universel

Comme le remarquait Philip Zimmermann, avant l'invention des télécommunications, le droit à avoir une conversation privée était aussi à comprendre comme une loi physique&nbsp;: il suffisait de s'isoler de manière assez efficace pour pouvoir tenir des échanges d'information de manière complètement privée. Ce n'est pas tout à fait exact. Les communications ont depuis toujours été soumises au risque de la divulgation, à partir du moment où un opérateur et/ou un dispositif entrent en jeu. Un rouleau de parchemin ou une lettre cachetée peuvent toujours être habilement ouverts et leur contenu divulgué. Et d'ailleurs la principale fonction du cachet n'était pas tant de fermer le pli que de l'authentifier.

C'est pour des raisons de stratégie militaire, que les premiers chiffrements firent leur apparition. Créés par l'homme pour l'homme, leur degré d'inviolabilité reposait sur l'habileté intellectuelle de l'un ou l'autre camp. C'est ainsi que le chiffrement ultime, une propriété de la nature (du moins, de la logique algorithmique) a été découvert&nbsp;: le [chiffre de Vernam](https://fr.wikipedia.org/wiki/Masque_jetable) ou système de chiffrement à masque jetable. L'idée est de créer un chiffrement dont la clé (ou masque) est aussi longue que le message à chiffrer, composée de manière aléatoire et utilisable une seule fois. Théoriquement impossible à casser, et bien que présentant des lacunes dans la mise en œuvre pratique, cette méthode de chiffrement était accessible à la puissance de calcul du cerveau humain. C'est avec l'apparition des machines que les dés ont commencés à être pipés, sur trois plans&nbsp;:

   - en dépassant les seules capacités humaines de calcul,
   - en rendant extrêmement rapides les procédures de chiffrement et de déchiffrement,
   - en rendant accessibles des outils puissants de chiffrement à un maximum d'individus dans une société «&nbsp;numérique&nbsp;».

Dans la mesure où l'essentiel de nos communications, chargées de données complexes et à grande distance, utilisent des machines pour être produites (ou au moins formalisées) et des services de télécommunications pour être véhiculées, le «&nbsp;droit naturel&nbsp;» à un échange privé auquel faisait allusion Philip Zimmermann, passe nécessairement par un système de chiffrement pratique, rapide et hautement efficace. PGP est une solution (il y en a d'autres).

PGP est-il efficace&nbsp;? Si le contrôle de l'accès à nos données peut toujours nous échapper (comme le montrent les procédures de surveillance), le chiffrement lui-même, ne serait-ce qu'à 128 bits «&nbsp;seulement&nbsp;», reste à ce jour assez crédible. Cette citation de [Wikipédia](https://fr.wikipedia.org/wiki/Cryptographie_sym%C3%A9trique) en donne la mesure :

> À titre indicatif, l'algorithme AES, dernier standard d'algorithme symétrique choisi par l'institut de standardisation américain NIST en décembre 2001, utilise des clés dont la taille est au moins de 128 bits soit 16 octets, autrement dit il y en a 2<sup>128</sup>. Pour donner un ordre de grandeur sur ce nombre, cela fait environ 3,4×10<sup>38</sup> clés possibles ; l'âge de l'univers étant de 10<sup>10</sup> années, si on suppose qu'il est possible de tester 1 000 milliards de clés par seconde (soit 3,2×10<sup>19</sup> clés par an), il faudra encore plus d'un milliard de fois l'âge de l'univers. Dans un tel cas, on pourrait raisonnablement penser que notre algorithme est sûr. Toutefois, l'utilisation en parallèle de très nombreux ordinateurs, synchronisés par internet, fragilise la sécurité calculatoire.

Les limites du chiffrement sont donc celles de la physique et des grands nombres, et à ce jour, ce sont des limites déjà largement acceptables. Tout l'enjeu, désormais, parce que les États ont montré leur propension à retourner l'argument démocratique contre le droit à la vie privée, est de disséminer suffisamment les pratiques de chiffrement dans le corps social. Ceci de manière à imposer en pratique la communication privée-chiffrée comme un acte naturel, un libre choix qui borne, en matière de surveillance numérique, les limites du pouvoir de l'État à ce que les individus choisissent de rendre privé et ce qu'ils choisissent de ne pas protéger par le chiffrement.

## Conclusion

Aujourd'hui, la définition du contrat social semble passer par un concept supplémentaire, le chiffrement de nos données. L'usage libre des pratiques de chiffrement est borné officiellement à un contrôle des moyens, ce qui semble suffisant, au moins pour nécessiter des procédures judiciaires bien identifiées dans la plupart des cas où le droit de savoir s'impose. Idéalement, cette limite ne devrait pas exister et il devrait être possible de pouvoir se servir de systèmes de chiffrement réputés inviolables, quel que soit l'avis des gouvernements.

L'inviolabilité est une utopie&nbsp;? pas tant que cela. En 2001, le chercheur Michael Rabin avait montré lors d'un [colloque](http://cs.nyu.edu/web/Calendar/colloquium/sp01/apr2.html) qu'un système réputé inviolable était concevable. En 2005, il a publié un article éclairant sur la technique de l'hyper-chiffrement ([hyper encryption](https://en.wikipedia.org/wiki/Hyper-encryption)) intitulé «&nbsp;Provably unbreakable hyper-encryption in the limited access model&nbsp;»[@Rabin2005], et une thèse (sous la direction de M. Rabin) a été soutenue en 2009 par Jason K. Juang[@Juang2009]. Si les moyens pour implémenter de tels modèles sont limités à ce jour par les capacités techniques, la sécurité de nos données semble dépendre de notre volonté de diminuer davantage ce qui nous sépare d'un système 100% efficace d'un point de vue théorique.

Le message de M. Valls, à propos de la «&nbsp;cryptologie légale&nbsp;» ne devrait pas susciter de commentaires particuliers puisque, effectivement, en l'état des possibilités techniques et grâce à l'ouverture de PGP, il est possible d'avoir des échanges réputés privés à défaut d'être complètement inviolables. Néanmoins, il faut rester vigilant quant à la tendance à vouloir définir légalement les conditions d'usage du chiffrement des données personnelles. Autant la surveillance de masse est (devrait être) inconstitutionnelle, autant le droit à chiffrer nos données doit être inconditionnel. 

Doit-on craindre les pratiques d'un gouvernement plus ou moins bien intentionné&nbsp;? Le Léviathan semble toutefois vaciller&nbsp;: non pas parce que nous faisons valoir en droit notre intimité, mais parce que d'autres Léviathans se sont réveillés, en dehors du droit et dans une nouvelle économie, sur un marché dont ils maîtrisent les règles. Ces nouveaux Léviathans, il nous faut les étudier avec d'autres concepts que ceux qui définissent l'État moderne.

[^declalog]: On peut se reporter au site de B. <span style="font-variant:small-caps;">Depail</span> (Univ. Marne-La-Vallée) qui expose les aspects juridiques de la signature numérique, en particulier la section «&nbsp;Aspects juridiques relatifs à la cryptographie&nbsp;».


# Internet, pour un contre-ordre social

**(Les anciens Léviathans)**

Les bouleversements des modèles socio-économiques induits par Internet nécessitent aujourd'hui une mobilisation des compétences et des expertises libristes pour maintenir les libertés des utilisateurs. Il est urgent de se positionner sur une offre de solutions libres, démocratiques, éthiques et solidaires. Voici un article paru initialement dans [Linux Pratique](http://www.unixgarden.com/) num. 85 Septembre/Octobre 2014, et repris sur le [Framablog](http://www.framablog.org/index.php/post/2014/09/05/internet-pour-un-contre-ordre-social-christophe-masutti) le 5 septembre 2014.


## De la légitimité

Michel Foucault, disparu il y a trente ans, proposait d’approcher les grandes questions du monde à travers le rapport entre savoir et pouvoir. Cette méthode a l’avantage de contextualiser le discours que l’on est en train d’analyser&nbsp;: quels discours permettent d’exercer quels pouvoirs ? Et quels pouvoirs sont censés induire quelles contraintes et en vertu de quels discours&nbsp;? Dans un de ses plus célèbres ouvrages, *Surveiller et punir*[@Foucault1975], Foucault démontre les mécanismes qui permettent de passer de la démonstration publique du pouvoir d’un seul, le monarque qui commande l’exécution publique des peines, à la normativité morale et physique imposée par le contrôle, jusqu’à l’auto-censure. Ce n’est plus le pouvoir qui est isolé dans la forteresse de l’autorité absolue, mais c’est l’individu qui exerce lui-même sa propre coercition. Ainsi, *Surveiller et punir* n’est pas un livre sur la prison mais sur la conformation de nos rapports sociaux à la fin du XX<sup>e</sup> siècle.

Les modèles économiques ont suivi cet ordre des choses&nbsp;: puisque la société est individualiste, c’est à l’individu que les discours doivent s’adresser. La plupart des modèles économiques qui préexistent à l’apparition de services sur Internet furent considérés, au début du XXI<sup>e</sup> siècle, comme les seuls capables de générer des bénéfices, de l’innovation et du bien-être social. L’exercice de la contrainte consistait à susciter le consentement des individus-utilisateurs dans un rapport qui, du moins le croyait-on, proposait une hiérarchie entre d’un côté les producteurs de contenus et services et, de l’autre côté, les utilisateurs. Il n’en était rien&nbsp;: les utilisateurs eux-mêmes étaient supposés produire des contenus œuvrant ainsi à la normalisation des rapports numériques où les créateurs exerçaient leur propre contrainte, c’est-à-dire accepter le dévoilement de leur vie privée (leur identité) en guise de tribut à l’expression de leurs idées, de leurs envies, de leurs besoins, de leurs rêves. Que n’avait-on pensé plus tôt au spectaculaire déploiement de la surveillance de masse focalisant non plus sur les actes, mais sur les éléments qui peuvent les déclencher ? Le commerce autant que l’État cherche à renseigner tout comportement prédictible dans la mesure où, pour l’un il permet de spéculer et pour l’autre il permet de planifier l’exercice du pouvoir. La société prédictible est ainsi devenue la force normalisatrice en fonction de laquelle tout discours et tout pouvoir s’exerce désormais (mais pas exclusivement) à travers l’organe de communication le plus puissant qui soit&nbsp;: Internet. L’affaire Snowden n’a fait que focaliser sur l’un de ses aspects relatif aux questions des défenses nationales. Mais l’aspect le plus important est que, comme le dit si bien Eben Moglen dans une conférence donnée à Berlin en 2012[@Moglen2012], «&nbsp;nous n’avons pas créé l’anonymat lorsque nous avons inventé Internet.&nbsp;»

Depuis le milieu des années 1980, les méthodes de collaboration dans la création de logiciels libres montraient que l’innovation devait être collective pour être assimilée et partagée par le plus grand nombre. La philosophie du Libre s’opposait à la nucléarisation sociale et proposait un modèle où, par la mise en réseau, le bien-être social pouvait émerger de la contribution volontaire de tous adhérant à des objectifs communs d’améliorations logicielles, techniques, sociales. Les créations non-logicielles de tout type ont fini par suivre le même chemin à travers l’extension des licences à des œuvres non logicielles. Les campagnes de financement collaboratif, en particulier lorsqu’elles visent à financer des projets sous licence libre, démontrent que dans un seul et même mouvement, il est possible à la fois de valider l’impact social du projet (par l’adhésion du nombre de donateurs) et assurer son développement. Pour reprendre Eben Moglen, ce n’est pas l’anonymat qui manque à Internet, c’est la possibilité de structurer une société de la collaboration qui échappe aux modèles anciens et à la coercition de droit privé qu’ils impliquent. C’est un changement de pouvoir qui est à l’œuvre et contre lequel toute réaction sera nécessairement celle de la punition&nbsp;: on comprend mieux l’arrivée plus ou moins subtile d’organes gouvernementaux et inter-gouvernementaux visant à sanctionner toute incartade qui soit effectivement condamnable en vertu du droit mais aussi à rigidifier les conditions d’arrivée des nouveaux modèles économiques et structurels qui contrecarrent les intérêts (individuels eux aussi, par définition) de quelques-uns. Nous ne sommes pas non plus à l’abri des resquilleurs et du libre-washing cherchant, sous couvert de sympathie, à rétablir une hiérarchie de contrôle.

Dans sa *Lettre aux barbus*[@Chemla2014], le 5 juin 2014, Laurent Chemla vise juste&nbsp;: le principe selon lequel «&nbsp;la sécurité globale (serait) la somme des sécurités individuelles&nbsp;» implique que la surveillance de masse (rendue possible, par exemple, grâce à notre consentement envers les services gratuits dont nous disposons sur Internet) provoque un déséquilibre entre d’une part ceux qui exercent le pouvoir et en ont les moyens et les connaissances, et d’autre part ceux sur qui s’exerce le pouvoir et qui demeurent les utilisateurs de l’organe même de l’exercice de ce pouvoir. Cette double contrainte n’est soluble qu’à la condition de cesser d’utiliser des outils centralisés et surtout s’en donner les moyens en «&nbsp;(imaginant) des outils qui créent le besoin plutôt que des outils qui répondent à des usages existants&nbsp;». C’est-à-dire qu’il relève de la responsabilité de ceux qui détiennent des portions de savoir (les barbus, caricature des libristes) de proposer au plus grand nombre de nouveaux outils capables de rétablir l’équilibre et donc de contrecarrer l’exercice illégitime du pouvoir. 

## Une affaire de compétences

Par bien des aspects, le logiciel libre a transformé la vie politique. En premier lieu parce que les licences libres ont bouleversé les modèles[@Jean2011] économiques et culturels hérités d’un régime de monopole. En second lieu, parce que les développements de logiciels libres n’impliquent pas de hiérarchie entre l’utilisateur et le concepteur et, dans ce contexte, et puisque le logiciel libre est aussi le support de la production de créations et d’informations, il implique des pratiques démocratiques de décision et de liberté d’expression. C’est en ce sens que la culture libre a souvent été qualifiée de «&nbsp;culture alternative&nbsp;» ou «&nbsp;contre-culture&nbsp;» parce qu’elle s’oppose assez frontalement avec les contraintes et les usages qui imposent à l’utilisateur une fenêtre minuscule pour échanger sa liberté contre des droits d’utilisation.

Contrairement à ce que l’on pouvait croire il y a seulement une dizaine d’années, tout le monde est en mesure de comprendre le paradoxe qu’il y a lorsque, pour pouvoir avoir le droit de communiquer avec la terre entière et 2 amis, vous devez auparavant céder vos droits et votre image à une entreprise comme Facebook. Il en est de même avec les formats de fichiers dont les limites ont vite été admises par le grand public qui ne comprenait et ne comprend toujours pas en vertu de quelle loi universelle le document écrit il y a 20 ans n’est aujourd’hui plus lisible avec le logiciel qui porte le même nom depuis cette époque. Les organisations libristes telles la Free Software Foundation[^fn5], L’Electronic Frontier Foundation[^fn6], l’April[^fn7], l’Aful[^fn8], Framasoft[^fn9] et bien d’autres à travers le monde ont œuvré pour la promotion des formats ouverts et de l’interopérabilité à tel point que la décision publique a dû agir en devenant, la plupart du temps assez mollement, un organe de promotion de ces formats. Bien sûr, l’enjeu pour le secteur public est celui de la manipulation de données sensibles dont il faut assurer une certaine pérennité, mais il est aussi politique puisque le rapport entre les administrés et les organes de l’État doit se faire sans donner à une entreprise privée l’exclusivité des conditions de diffusion de l’information.

Les acteurs associatifs du Libre, sans se positionner en lobbies (alors même que les lobbies privés sont financièrement bien plus équipés) et en œuvrant auprès du public en donnant la possibilité à celui-ci d’agir concrètement, ont montré que la société civile est capable d’expertise dans ce domaine. Néanmoins, un obstacle de taille est encore à franchir&nbsp;: celui de donner les moyens techniques de rendre utilisables les solutions alternatives permettant une émancipation durable de la société. Peine perdue ? On pourrait le croire, alors que des instances comme le CNNum (Conseil National du Numérique) ont tendance à se résigner[^fn10] et penser que les GAFA (Google, Apple, Facebook et Amazon) seraient des autorités incontournables, tout comme la soumission des internautes à cette nouvelle forme de féodalité serait irrémédiable.

Pour ce qui concerne la visibilité, on ne peut pas nier les efforts souvent exceptionnels engagés par les associations et fondations de tout poil visant à promouvoir le Libre et ses usages auprès du large public. Tout récemment, la Free Software Foundation a publié un site web multilingue exclusivement consacré à la question de la sécurité des données dans l’usage des courriels. Intitulé *Email Self Defense*[^fn11], ce guide explique, étape par étape, la méthode pour chiffrer efficacement ses courriels avec des logiciels libres. Ce type de démarche est en réalité un symptôme, mais il n’est pas seulement celui d’une réaction face aux récentes affaires d’espionnage planétaire via Internet.

Pour reprendre l’idée de Foucault énoncée ci-dessus, le contexte de l’espionnage de masse est aujourd’hui tel qu’il laisse la place à un autre discours&nbsp;: celui de la nécessité de déployer de manière autonome des infrastructures propres à l’apprentissage et à l’usage des logiciels libres en fonction des besoins des populations. Auparavant, il était en effet aisé de susciter l’adhésion aux principes du logiciel libre sans pour autant déployer de nouveaux usages et sans un appui politique concret et courageux (comme les logiciels libres à l’école, dans les administrations, etc.). Aujourd’hui, non seulement les principes sont socialement intégrés mais de nouveaux usages ont fait leur apparition tout en restant prisonniers des systèmes en place. C’est ce que soulève très justement un article récent de Cory Doctorow[^fn12] en citant une étude à propos de l’usage d’Internet chez les jeunes gens. Par exemple, une part non négligeable d’entre eux suppriment puis réactivent au besoin leurs comptes Facebook de manière à protéger leurs données et leur identité. Pour Doctorow, être «&nbsp;natifs du numérique&nbsp;» ne signifie nullement avoir un sens inné des bons usages sur Internet, en revanche leur sens de la confidentialité (et la créativité dont il est fait preuve pour la sauvegarder) est contrecarré par le fait que «&nbsp;Facebook rend extrêmement difficile toute tentative de protection de notre vie privée&nbsp;» et que, de manière plus générale, «&nbsp;les outils propices à la vie privée tendent à être peu pratiques&nbsp;». Le sous-entendu est évident&nbsp;: même si l’installation logicielle est de plus en plus aisée, tout le monde n’est capable d’installer chez soi des solutions appropriées comme un serveur de courriel chiffré.

## Que faire ?

Le diagnostic posé, que pouvons-nous faire ? Le domaine associatif a besoin d’argent. C’est un fait. Il est d’ailleurs remarqué par le gouvernement français, qui a fait de l’engagement associatif la grande «&nbsp;cause nationale de l’année 2014&nbsp;». Cette action[^fn13] a au moins le mérite de valoriser l’économie sociale et solidaire, ainsi que le bénévolat. Les associations libristes sont déjà dans une dynamique similaire depuis un long moment, et parfois s’essoufflent… En revanche, il faut des investissements de taille pour avoir la possibilité de soutenir des infrastructures libres dédiées au public et répondant à ses usages numériques. Ces investissements sont difficiles pour au moins deux raisons&nbsp;:

*   la première réside dans le fait que les associations ont pour cela besoin de dons en argent. Les bonnes volontés ne suffisent pas et la monnaie disponible dans les seules communautés libristes est à ce jour en quantité insuffisante compte tenu des nombreuses sollicitations ;
*   la seconde découle de la première&nbsp;: il faut lancer un mouvement de financements participatifs ou des campagnes de dons ciblées de manière à susciter l’adhésion du grand public et proposer des solutions adaptées aux usages.

Pour cela, la première difficulté sera de lutter contre la gratuité. Aussi paradoxal que cela puisse paraître, la gratuité (relative) des services privateurs possède une dimension attractive si puissante qu’elle élude presque totalement l’existence des solutions libres ou non libres qui, elles, sont payantes. Pour rester dans le domaine de la correspondance, il est très difficile aujourd’hui de faire comprendre à Monsieur Dupont qu’il peut choisir un hébergeur de courriel payant, même au prix «&nbsp;participatif&nbsp;» d’1 euro par mois. En effet, Monsieur Dupont peut aujourd’hui utiliser, au choix&nbsp;: le serveur de courriel de son employeur, le serveur de courriel de son fournisseur d’accès à Internet, les serveurs de chez Google, Yahoo et autres fournisseurs disponibles très rapidement sur Internet. Dans l’ensemble, ces solutions sont relativement efficaces, simples d’utilisation, et ne nécessitent pas de dépenses supplémentaires. Autant d’arguments qui permettent d’ignorer la question de la confidentialité des courriels qui peuvent être lus et/ou analysés par son employeur, son fournisseur d’accès, des sociétés tierces…

Pourtant des solutions libres, payantes et respectueuses des libertés, existent depuis longtemps. C’est le cas de *Sud-Ouest.org*[^fn14], une plate-forme d’hébergement mail à prix libre. Ou encore l’association *Lautre.net*[^fn15], qui propose une solution d’hébergement de site web, mais aussi une adresse courriel, la possibilité de partager ses documents via FTP, la création de listes de discussion, etc. Pour vivre, elle propose une participation financière à la gestion de son infrastructure, quoi de plus normal ?

Aujourd’hui, il est de la responsabilité des associations libristes de multiplier ce genre de solutions. Cependant, pour dégager l’obstacle de la contrepartie financière systématique, il est possible d’ouvrir gratuitement des services au plus grand nombre en comptant exclusivement sur la participation de quelques-uns (mais les plus nombreux possible). En d’autres termes, il s’agit de mutualiser à la fois les plates-formes et les moyens financiers. Cela ne rend pas pour autant les offres gratuites, simplement le coût total est réparti socialement tant en unités de monnaie qu’en contributions de compétences. Pour cela, il faut savoir convaincre un public déjà largement refroidi par les pratiques des géants du web et qui perd confiance.



## Framasoft propose des solutions

Parmi les nombreux projets de Framasoft, il en est un, plus généraliste, qui porte exclusivement sur les moyens techniques (logiciels et matériels) de l’émancipation du web. Il vise à renouer avec les principes qui ont guidé (en des temps désormais très anciens) la création d’Internet, à savoir&nbsp;: un Internet libre, décentralisé (ou démocratique), éthique et solidaire (l.d.e.s.).

Framasoft n’a cependant pas le monopole de ces principes l.d.e.s., loin s’en faut, en particulier parce que les acteurs du Libre œuvrent tous à l’adoption de ces principes. Mais Framasoft compte désormais jouer un rôle d’interface. À l’instar d’un Google qui rachète des start-up pour installer leurs solutions à son compte et constituer son nuage, Framasoft se propose depuis 2010, d’héberger des solutions libres pour les ouvrir gratuitement à tout public. C’est ainsi que par exemple, des particuliers, des syndicats, des associations et des entreprises utilisent les instances Framapad et Framadate. Il s’agit du logiciel Etherpad, un système de traitement de texte collaboratif, et d’un système de sondage issu de l’Université de Strasbourg (Studs) permettant de convenir d’une date de réunion ou créer un questionnaire. Des milliers d’utilisateurs ont déjà bénéficié de ces applications en ligne ainsi que d’autres, qui sont listées sur *Framalab.org*[^fn16].

Depuis le début de l’année 2014, Framasoft a entamé une stratégie qui, jusqu’à présent est apparue comme un iceberg aux yeux du public. Pour la partie émergée, nous avons tout d’abord commencé par rompre radicalement les ponts avec les outils que nous avions tendance à utiliser par pure facilité. Comme nous l’avions annoncé lors de la campagne de don 2013, nous avons quitté les services de Google pour nos listes de discussion et nos analyses statistiques. Nous avons choisi d’installer une instance Bluemind, ouvert un serveur Sympa, mis en place Piwik ; quant à la publicité et les contenus embarqués, nous pouvons désormais nous enorgueillir d’assurer à tous nos visiteurs que nous ne nourrissons plus la base de données de Google. À l’échelle du réseau Framasoft, ces efforts ont été très importants et ont nécessité des compétences et une organisation technique dont jusque-là nous ne disposions pas.

Nous ne souhaitons pas nous arrêter là. La face immergée de l’iceberg est en réalité le déploiement sans précédent de plusieurs services ouverts. Ces services ne seront pas seulement proposés, ils seront accompagnés d’une pédagogie visant à montrer comment[^fn17] installer des instances similaires pour soi-même ou pour son organisation. Nous y attachons d’autant plus d’importance que l’objectif n’est pas de commettre l’erreur de proposer des alternatives centralisées mais d’essaimer au maximum les solutions proposées.

Au mois de juin, nous avons lancé une campagne de financement participatif afin d’améliorer Etherpad (sur lequel est basé notre service Framapad) en travaillant sur un plugin baptisé Mypads&nbsp;: il s’agit d’ouvrir des instances privées, collaboratives ou non, et les regrouper à l’envi, ce qui permettra *in fine* de proposer une alternative sérieuse à Google Docs. À l’heure où j’écris ces lignes, la campagne est une pleine réussite et le déploiement de Mypads (ainsi que sa mise à disposition pour toute instance Etherpad) est prévue pour le dernier trimestre 2014. Nous avons de même comblé les utilisateurs de Framindmap, notre créateur en ligne de carte heuristiques, en leur donnant une dimension collaborative avec Wisemapping, une solution plus complète.

Au mois de juillet, nous avons lancé Framasphère[^fn18], une instance Diaspora* dont l’objectif est de proposer (avec Diaspora-fr[^fn19]) une alternative à Facebook en l’ouvrant cette fois au maximum en direction des personnes extérieures au monde libriste. Nous espérons pouvoir attirer ainsi l’attention sur le fait qu’aujourd’hui, les réseaux sociaux doivent afficher clairement une éthique respectueuse des libertés et des droits, ce que nous pouvons garantir de notre côté.

Enfin, après l’été 2014, nous comptons de même offrir aux utilisateurs un moteur de recherche (Framasearx) et d’ici 2015, si tout va bien, un diaporama en ligne, un service de visioconférence, des services de partage de fichiers anonymes et chiffrés, et puis… et puis…

Aurons-nous les moyens techniques et financiers de supporter la charge ? J’aimerais me contenter de dire que nous avons la prétention de faire ainsi œuvre publique et que nous devons réussir parce qu’Internet a aujourd’hui besoin de davantage de zones libres et partagées. Mais cela ne suffit pas. D’après les derniers calculs, si l’on compare en termes de serveurs, de chiffre d’affaire et d’employés, Framasoft est environ 38.000 fois plus petit que Google[^fn20]. Or, nous n’avons pas peur, nous ne sommes pas résignés, et nous avons nous aussi une vision au long terme pour changer le monde[^fn21]. Nous savons qu’une population de plus en plus importante (presque majoritaire, en fait) adhère aux mêmes principes que ceux du modèle économique, technique et éthique que nous proposons. C’est à la société civile de se mobiliser et nous allons développer un espace d’expression de ces besoins avec les moyens financiers de 200 mètres d’autoroute en équivalent fonds publics. Dans les mois et les années qui viennent, nous exposerons ensemble des méthodes et des exemples concrets pour améliorer Internet. Nous aider et vous investir, c’est rendre possible le passage de la résistance à la réalisation.




[^fn5]: La FSF (``fsf.org``) se donne pour mission mondiale la promotion du logiciel libre et la défense des utilisateurs.

[^fn6]: L’objectif de l’EFF(``eff.org``) est de défendre la liberté d’expression sur Internet, ce qui implique l’utilisation des logiciels libres.

[^fn7]: April. Promouvoir et défendre le logiciel libre&nbsp;; ``april.org``.

[^fn8]: Association Francophone des Utilisateurs de Logiciels Libres&nbsp;; ``aful.org``.

[^fn9]: Framasoft, *La route est longue mais la voie est libre*&nbsp;; ``framasoft.org``.

[^fn10]: Voir à ce sujet l’analyse du dernier rapport du CNNum sur «&nbsp;La neutralité des plateformes&nbsp;», par Stéphane <span style="font-variant:small-caps;">Bortzmeyer</span>.

[^fn11]: Autodéfense courriel&nbsp;; ``emailselfdefense.fsf.org/fr/``.

[^fn12]: Cory <span style="font-variant:small-caps;">Doctorow</span>, «&nbsp;Vous êtes *natif du numérique*&nbsp;? &ndash;&nbsp;Ce n’est pas si grave, mais&hellip;&nbsp;», trad. fr. sur *Framablog*, le 6 juin 2014.

[^fn13]: Sera-t-elle efficace&nbsp;? c’est une autre question.

[^fn14]: Plateforme libre d’hébergement mail à prix libre&nbsp;; ``sud-ouest.org``.

[^fn15]: L’Autre Net, hébergeur associatif autogéré&nbsp;; ``lautre.net``.

[^fn16]: Le laboratoire des projets Framasoft; ``framalab.org``.

[^fn17]: On peut voir le tutoriel d’installation de Wisemapping sur ``framacloud.org`` comme exemple de promotion de la décentralisation.

[^fn18]: Un réseau social libre, respectueux et décentralisé&nbsp;; ``framasphere.org``.

[^fn19]: Noeud du réseau Diaspora*, hébergé en France chez OVH&nbsp;; ``diaspora-fr.org/``.

[^fn20]: Voir au sujet de la dégooglisation d’Internet la conférence de Pierre-Yves <span style="font-variant:small-caps;">Gosset</span>  lors des Rencontres Mondiales du Logiciel Libre, juillet 2014, Montpellier.

[^fn21]: Cette vision du monde vaut bien celle de Google, qui faisait l’objet de la Une de *Courrier International* du mois de mai 2014.






