# Compiler pour PDF (LaTeX)

        pandoc --filter pandoc-citeproc --bibliography=biblio.bib --csl=iso-french-bricole.csl -s --template=tanplette.latex --latex-engine=xelatex --toc --number-section -colorlinks info.md nl1.md nl2.md nl3.md nl4.md al1.md al2.md metapourlatex.yaml -o leviathans1234et12.pdf

# Pour du HTML

        pandoc --filter pandoc-citeproc --bibliography=biblio.bib --csl=iso-french-bricole.csl -s --toc --template template.html --css template.css XXXHTML.yaml ENTREE.md -o SORTIE.html

# Pour le Epub

        pandoc --filter pandoc-citeproc --bibliography=biblio.bib --csl=iso-french-bricole.csl --number-section -colorlinks --epub-cover-image=cover.png info.md nl1.md nl2.md nl3.md nl4.md al1.md al2.md metapourepub.yaml -o lev.epub
