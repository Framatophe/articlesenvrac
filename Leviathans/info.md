# Informations {.unnumbered #infos}

**Licence :** les écrits présents dans ce fichier sont placés sous [Licence Art Libre](http://artlibre.org/).

**Couverture :** *Behemoth and Leviathan*, par William Blake, 1826 (``commons.wikimedia.org``).

**Les sources** de ce fichier sont accessibles depuis [ce dépôt Framagit](https://framagit.org/Framatophe/articlesenvrac/)</a>.

Les articles qui composent ce fichier ont été publiés sur [Framablog](https://framablog.org).

- Les nouveaux Léviathans [Ia](https://framablog.org/?p=6617) et [Ib](https://framablog.org/?p=6626) (2016)
- Les nouveaux Léviathans  [IIa](https://framablog.org/?p=6629) et [IIb](https://framablog.org/?p=6631) (2016)
- Les nouveaux Léviathans  [III](https://framablog.org/?p=9897) (2017)
- Les nouveaux Léviathans  [IV](https://framablog.org/?p=11179/) (2018)
- Les anciens Léviathans [I](https://framablog.org/?p=6637) (2016) et [II](https://framablog.org/?p=6661) (2015)



[Christophe Masutti](http://christophe.masutti.name) est administrateur  de [Framasoft](https://framasoft.org/). À ses moments perdus, il note et partage les réflexions qui motivent son engagement dans le Libre.


